#version 330
layout(location = 0) in vec4 vertex;
uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection;

void main()
{
	gl_Position = projection * view_matrix * model_matrix * vertex;
}