#version 120
varying vec3 normal;
varying vec3 frag_pos;
varying vec4 frag_pos_lightspace;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection;
uniform mat3 normalMatrix;
uniform mat4 lightSpaceMatrix;

void main()
{
	normal = normalize(normalMatrix * gl_Normal);
	frag_pos = vec3(model_matrix * gl_Vertex);
	frag_pos_lightspace = lightSpaceMatrix * vec4(frag_pos, 1.0);
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = projection * view_matrix * model_matrix * gl_Vertex;
}