varying vec3 normal;
varying vec3 MCposition;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection;
uniform mat3 normalMatrix;
uniform float Scale;

void main()
{
	MCposition = vec3(gl_Vertex) * Scale;
	normal = normalize(normalMatrix * gl_Normal);
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = projection * view_matrix * model_matrix * gl_Vertex;
}