#version 120
varying vec3 normal;
varying vec3 frag_pos;
varying vec4 frag_pos_lightspace;

uniform sampler2D tex;
uniform sampler2D shadowMap;

uniform mat4 view_matrix;
uniform vec3 lightDir;
uniform vec2 shadowMapSize;

float ShadowCalculation(in vec4 fragPosLightSpace, in float bias)
{
	vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
	projCoords = projCoords * 0.5 + 0.5;
	//float closestDepth = texture2D(shadowMap, projCoords.xy).r;
	float currentDepth = projCoords.z;
	float shadow = 0.0;
	if(projCoords.z > 1.0)
	{
		shadow = 0.0;
	}
	else
	{
		vec2 texelsize = 1.0 / shadowMapSize;
		for(int x=-1; x<=1; ++x)
		{
			for(int y =-1; y<=1; ++y)
			{
				float pcfDepth = texture2D(shadowMap, projCoords.xy + vec2(x, y) * texelsize).r;
				shadow += (currentDepth-bias) > pcfDepth  ? 1.0 : 0.0;
			}
		}
		shadow /= 9.0;
	}
	return shadow;
}

void main()
{
	float NdotL;
	vec3 temp = texture2D(tex,gl_TexCoord[0].st).xyz;
	vec4 color = vec4(temp*0.1, 1.0);
	vec4 lightDir_viewspace = view_matrix * vec4(-lightDir, 0.0);
	float bias = max(0.05 * (1.0 - dot(normalize(normal), vec3(lightDir_viewspace))), 0.005);
	float shadow = ShadowCalculation(frag_pos_lightspace, bias);
	NdotL = max(dot(normalize(normal), vec3(lightDir_viewspace)), 0.0);
	if(NdotL>0.0)
	{
		color += texture2D(tex,gl_TexCoord[0].st) * NdotL * (1.0 - shadow);
	}
	gl_FragColor = color;
}