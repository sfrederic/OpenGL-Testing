#pragma optimize(off)

varying vec3 normal;
varying vec3 MCposition;

uniform sampler2D tex;
uniform sampler3D noise_tex;
uniform mat4 view_matrix;
uniform vec3 lightDir;

#define TEXTURE_MATERIAL 0
#define PROCEDURAL_MATERIAL 1
uniform ivec2 materialDefinition;

#define CLOUD_EFFECT 0
#define LAVA_EFFECT 1
#define MARBLE_EFFECT 2
#define GRANITE_EFFECT 3
#define WOOD_EFFECT 4
uniform int effect_ID;

vec4 simpleDiffuseTexture()
{
	float NdotL;
	vec4 temp = texture2D(tex, gl_TexCoord[0].st);
	vec3 color = temp.xyz * 0.1;
	vec4 lightDir_viewspace = view_matrix * vec4(-lightDir, 0.0);
	NdotL = max(dot(normalize(normal), vec3(lightDir_viewspace)), 0.0);
	if( NdotL > 0.0 )
	{
		color += texture2D(tex, gl_TexCoord[0].st).xyz * NdotL;
	}
	return vec4(color, 1.0);
}

vec4 CloudEffect()
{
	vec3 SkyColor=vec3(0.0, 0.0, 0.8);
	vec3 CloudColor=vec3(0.8, 0.8, 0.8);
	float NdotL;
	float LightIntensity;

	vec4 lightDir_viewspace = view_matrix * vec4(-lightDir, 0.0);
	NdotL = max(dot(normalize(normal), vec3(lightDir_viewspace)), 0.0);
	LightIntensity = 1.5 * NdotL;

	vec4 noisevec = texture3D(noise_tex, MCposition);
	
	float intensity = (noisevec[0] + noisevec[1] + noisevec[2] + noisevec[3] + 0.03125) * 1.5;
	
	vec3 color = mix( SkyColor, CloudColor, intensity ) * NdotL;

	return vec4(color, 1.0);
}

vec4 LavaEffect()
{
	float NoiseScale = 1.2;
	float NdotL;
	vec3 Color1 = vec3(0.8, 0.7, 0.0);
	vec3 Color2 = vec3(0.6, 0.1, 0.0);

	vec4 lightDir_viewspace = view_matrix * vec4(-lightDir, 0.0);
	NdotL = max(dot(normalize(normal), vec3(lightDir_viewspace)), 0.0);

	vec4 noisevec = texture3D(noise_tex, MCposition * NoiseScale);

	float intensity =	abs(noisevec[0] - 0.25) +
						abs(noisevec[1] - 0.125) +
						abs(noisevec[2] - 0.0625)+
						abs(noisevec[3] - 0.03125);

	intensity = clamp(intensity * 6.0, 0.0, 1.0);
	vec3 color = mix(Color1, Color2, intensity) * NdotL;
	return vec4(color, 1.0);
}

vec4 MarbleEffect()
{
	float NdotL;
	vec3 MarbleColor = vec3(0.6, 0.65, 0.8);
	vec3 VeinColor	= vec3(0.5, 0.5, 0.1);

	vec4 lightDir_viewspace = view_matrix * vec4(-lightDir, 0.0);
	NdotL = max(dot(normalize(normal), vec3(lightDir_viewspace)), 0.0);

	vec4 noisevec = texture3D(noise_tex, MCposition);

	float intensity =	abs(noisevec[0] - 0.25) +
						abs(noisevec[1] - 0.125) +
						abs(noisevec[2] - 0.0625)+
						abs(noisevec[3] - 0.03125);

	float sine_val = sin(MCposition.y * 6.0 + intensity * 12.0) * 0.5 + 0.5;
	vec3 color = mix(VeinColor, MarbleColor, sine_val) * NdotL;
	 
	return vec4(color, 1.0);
}

vec4 GraniteEffect()
{
	float NoiseScale = 1.2;
	float NdotL;
	vec4 lightDir_viewspace = view_matrix * vec4(-lightDir, 0.0);
	NdotL = max(dot(normalize(normal), vec3(lightDir_viewspace)), 0.0);

	vec4 noisevec = texture3D(noise_tex, MCposition * NoiseScale);
	float intensity = min(1.0, noisevec[3] * 18.0);
	vec3 color = vec3(intensity * NdotL);

	return vec4(color, 1.0);
}

vec4 WoodEffect()
{
	vec3 LightWood		= vec3(0.6, 0.3, 0.1);
	vec3 DarkWood		= vec3(0.4, 0.2, 0.07);
	float RingFreq		= 4.0;
	float LightGrains	= 1.0;
	float DarkGrains	= 0.0;
	float GrainThreshold= 0.5;
	vec3 NoiseScale		= vec3(0.5, 0.1, 0.1);
	float Noisiness		= 3.0;
	float GrainScale	= 27.0;
	float NdotL;
	vec4 lightDir_viewspace = view_matrix * vec4(-lightDir, 0.0);
	NdotL = max(dot(normalize(normal), vec3(lightDir_viewspace)), 0.0);

	vec3 noisevec = vec3(texture3D(noise_tex, MCposition * NoiseScale)) * Noisiness;

	vec3 location = MCposition + noisevec;

	float dist = sqrt(location.x * location.x + location.z * location.z);
	dist *= RingFreq;

	float r = fract(dist + noisevec[0] + noisevec[1] + noisevec[2]) * 2.0;

	if( r > 1.0)
		r = 2.0 - r;

	vec3 color = mix(LightWood, DarkWood, r);

	r = fract((MCposition.x + MCposition.z) * GrainScale + 0.5);
	noisevec[2] *= r;
	if( r < GrainThreshold )
		color += LightWood * LightGrains * noisevec[2];
	else
		color -= LightWood * DarkGrains * noisevec[2];

	color *= NdotL;
	return vec4(color, 1.0);
}

void main()
{
	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
	if ( materialDefinition[TEXTURE_MATERIAL] == 1 )
	{
		color = simpleDiffuseTexture();
	}
	else if ( materialDefinition[PROCEDURAL_MATERIAL] == 1 )
	{
		if( effect_ID == CLOUD_EFFECT )
		{
			color = CloudEffect();
		}
		else if( effect_ID == LAVA_EFFECT )
		{
			color = LavaEffect();
		}
		else if( effect_ID == MARBLE_EFFECT )
		{
			color = MarbleEffect();
		}
		else if( effect_ID == GRANITE_EFFECT )
		{
			color= GraniteEffect();
		}
		else if( effect_ID == WOOD_EFFECT )
		{
			color = WoodEffect();
		}
		else
		{
			color = vec4(1.0, 0.0, 0.0, 1.0);
		}
	}
	else
	{
		color = vec4(1.0, 0.0, 0.0, 1.0);
	}
	gl_FragColor = color;
}