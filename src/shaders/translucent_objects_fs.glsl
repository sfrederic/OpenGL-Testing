#version 330
layout(location = 0) out vec4 out_color;

uniform vec4 light_direction;
uniform vec3 light_intensity;
uniform float ior_material;
uniform float theta_crit;

in vec3 normal;
in vec3 pos_to_eye;
in vec3 radiantFlux;

#define M_2PI 6.283185307179586476925286766559

void main()
{
	vec3 n = normalize(normal);
	vec3 l = normalize(light_direction.xyz);
	float ndotl = max(0.0, dot(n, -l));
	vec3 incoming_light = light_intensity.xyz * ndotl;
	
	float ior_air = 1.000293;
	float eta = ior_air/ior_material;
	float F_dr = -1.44f / (ior_material*ior_material) + 0.71f / ior_material + 0.668f + 0.0636f*ior_material;
	
	vec3 pte = normalize(pos_to_eye);
	float cos_theta_2 = dot( n, pte);
	float cos_theta_1 = dot(-n, normalize(refract(-pte, n, eta)));
	float theta_1 = acos(cos_theta_1);
	float f_t = 0.0;
	
	if ( theta_1 <= theta_crit )
	{
		float ior1 = ior_material;
		float ior2 = ior_air;
		float t_parallel 		= (2.0 * ior1 * cos_theta_1) / (ior2 * cos_theta_1 + ior1 * cos_theta_2);
		float t_perpendicular 	= (2.0 * ior1 * cos_theta_1) / (ior1 * cos_theta_1 + ior2 * cos_theta_2);
		f_t = 0.5 * (t_parallel + t_perpendicular);
	}
	
	out_color = vec4( f_t * (radiantFlux - 2.0 * incoming_light) / ( M_2PI * (1.0 + F_dr) ), 1.0) ;
}