#version 330

layout(location = 0) out vec4 transmitted_light;

in vec3 normal;
in vec3 Refract;
in vec3 Reflect;
flat in float ior1;
flat in float ior2;
uniform vec4 light_direction;
uniform vec3 light_intensity;

void main()
{
	//compute Lambertian reflection and Fresnel transmission term
	vec3 n = normalize(normal);
	vec3 l = normalize(light_direction.xyz);
	float ndotl = max(0.0, dot(n, -l));
	float f_t = 0.0;
	
	float cos_theta_1 = dot(n, normalize(Reflect));
	float cos_theta_2 = dot(-n, normalize(Refract));
	
	float t_parallel 		= (2.0 * ior1 * cos_theta_1) / (ior2 * cos_theta_1 + ior1 * cos_theta_2);
	float t_perpendicular 	= (2.0 * ior1 * cos_theta_1) / (ior1 * cos_theta_1 + ior2 * cos_theta_2);
	f_t = 0.5 * (t_parallel + t_perpendicular);
	
	transmitted_light = f_t * vec4(light_intensity * ndotl, 1.0 );
}