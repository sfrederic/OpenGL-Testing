#version 330
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texcoord;
layout(location = 2) in vec3 normal_vec;

uniform mat4 lightSpaceMatrix;
uniform mat4 modelTransform;
uniform mat3 lightNormalMatrix;
uniform vec4 light_direction;
uniform float ior_material;

out vec3 normal;
out vec3 Refract;
out vec3 Reflect;
flat out float ior1;
flat out float ior2;

void main()
{
	float ior_air = 1.000293;
	float eta = ior_air / ior_material;
	
	normal = lightNormalMatrix * normal_vec;
	vec4 tf_pos = lightSpaceMatrix * modelTransform * vec4(position, 1.0);
	
	vec3 I = light_direction.w > 0.0 ? -(light_direction.xyz - tf_pos.xyz) : light_direction.xyz;
	Refract = refract(I, normal, eta);
	Reflect = reflect(I, normal);
	
	ior1 = ior_air;
	ior2 = ior_material;
	
	gl_Position = tf_pos;
}