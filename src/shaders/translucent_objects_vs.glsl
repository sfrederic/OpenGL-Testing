#version 330
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texcoords;
layout(location = 2) in vec3 normals;
layout(location = 4) in vec2 fluxTexcoordRange;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;
uniform vec4 light_direction;
uniform vec3 camera_position;

uniform sampler2DRect radiantFluxTexture;
uniform samplerBuffer radiantFluxTexCoords;

out vec3 normal;
out vec3 pos_to_eye;
out vec3 radiantFlux;

void main()
{
	vec3 n = normalize( normalMatrix * normals );
	vec4 mc_position = modelMatrix * vec4(position, 1.0);

	int s = int(fluxTexcoordRange.x);
	int e = int(fluxTexcoordRange.y);
	//compute average flux over neighboring surface nodes of this vertex
	vec3 flux = vec3(0.0); float count = 0.0;
	for( ; s < e; s++)
	{
		vec2 fluxTexCoord = texelFetch(radiantFluxTexCoords, s).xy;
		vec3 fluxval = texture(radiantFluxTexture, fluxTexCoord).xyz;
		flux += fluxval;
		count += 1.0;
	}
	radiantFlux = flux / count;
	normal = n;
	pos_to_eye = camera_position.xyz - mc_position.xyz;
	gl_Position = projectionMatrix * viewMatrix * mc_position;
}