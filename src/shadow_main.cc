#include "CGEMesh_OGL2.h"
#include "CGEFullScreenQuad.h"
#include "shaders.h"
#include "CGETransformation.h"
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include "app_setup.h"
#include "functionality.h"
#include "shadow_mapping.h"

extern GLuint depthMapFBO;
extern GLuint depthMap;
extern GLuint color_tex;
extern GLuint color_tex2;
extern GLuint color_tex3;

int main(void)
{
	//////////////////////////////////////////////////////////////////////////
	GLFWwindow* window;

	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	window = glfwCreateWindow(640, 480, "OpenGL Testing", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwSetKeyCallback(window, key_callback);
	glfwSetFramebufferSizeCallback(window, frambuffer_resize_callback);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	int width(0), height(0);
	glfwGetFramebufferSize(window, &width, &height);

	glewExperimental = GL_TRUE;
	GLenum initStatus = glewInit();
	if (initStatus != GLEW_OK) 
	{
		std::cerr << "Unable to initialize GLEW!\n";
		exit(EXIT_FAILURE);
	}

	std::cout << glGetString(GL_VERSION) << std::endl;
	std::cout << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	glViewport(0, 0, width, height);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	//////////////////////////////////////////////////////////////////////////
	CGEngine::ShaderProgram shader	;
	CGEngine::ShaderProgram fsq_shader;
	CGEngine::ShaderProgram simpleDepthShader;
	
	try
	{
		shader.LoadVertexShader( (shadercode_path + std::string("shadow_render_pass.vert")).c_str () );
		shader.LoadFragmentShader((shadercode_path + std::string("shadow_render_pass.frag")).c_str ());
		shader.LinkProgram();

		fsq_shader.VertexShaderFromSource( fsq_vertex_shader );
		fsq_shader.FragmentShaderFromSource( fsq_fragment_shader );
		fsq_shader.LinkProgram();

		simpleDepthShader.LoadVertexShader( (shadercode_path + std::string("light_pass.vert")).c_str () );
		simpleDepthShader.LoadFragmentShader((shadercode_path + std::string("light_pass.frag")).c_str ());
		simpleDepthShader.LinkProgram();
	}
	catch (CGEngine::ShaderError& _e)
	{
		std::cerr << _e.what() << "\n";
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	GLint fsq_tex_loc = fsq_shader.GetUniformVarLocation("depthMap");
	GLint tex_loc = shader.GetUniformVarLocation("tex");
	GLint depthmap_loc = shader.GetUniformVarLocation("shadowMap");
	GLint light_loc = shader.GetUniformVarLocation("lightDir");
	GLint texsize_loc = shader.GetUniformVarLocation("shadowMapSize");

	GLint proj_loc = shader.GetUniformVarLocation("projection");
	GLint nm_loc = shader.GetUniformVarLocation("normalMatrix");
	GLint mm_loc = shader.GetUniformVarLocation("model_matrix");
	GLint view_loc = shader.GetUniformVarLocation("view_matrix");
	GLint lspmat_loc = shader.GetUniformVarLocation("lightSpaceMatrix");

	GLint lsm_loc = simpleDepthShader.GetUniformVarLocation("lightSpaceMatrix");
	GLint mt_loc = simpleDepthShader.GetUniformVarLocation("model_transform");

	bool b = true;

	CGEngine::CGLTexture2D my_texture;
	my_texture.VLoadTexture( data_path + std::string("textures/nase.bmp"));

	CGEngine::CMeshOGL2 suzanne;
	if( !suzanne.LoadMesh( data_path + std::string("suzanne.obj")) )
		exit(EXIT_FAILURE);
	CGEngine::CMeshOGL2 dice;
	if( !dice.LoadMesh( data_path + std::string("dice.obj")) )
		exit(EXIT_FAILURE);
	CGEngine::CMeshOGL2 sphere;
	if( !sphere.LoadMesh( data_path + std::string("sphere.obj")) )
		exit(EXIT_FAILURE);
	CGEngine::CMeshOGL2 jeep;
	if( !jeep.LoadMesh( data_path + std::string("jeep1.obj")) )
		exit(EXIT_FAILURE);
	CGEngine::CFullScreenQuad fsq;
	fsq.LoadMesh(std::string(""));

	CGEngine::Vec3 lightDir(-1.0, 0.0, 0.0); //in world space
	CGEngine::CMatrixStack ModelTransformStack;
	CGEngine::Matrix4x4 projection = glm::perspective(45.0f, (GLfloat)width/(GLfloat)height, 1.0f, 100.0f);
	CGEngine::Matrix4x4 view(1.0);
	CGEngine::Matrix4x4 light_view(1.0);
	light_view = glm::lookAt(	glm::vec3(6.0, 0.0, -9.0), 
						glm::vec3(6.0, 0.0, -9.0)+lightDir, 
						glm::vec3(0.0, 1.0, 0.0));
	CGEngine::Matrix4x4 lightSpaceMatrix = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane) * light_view;

	FBO_Info fbo;
	CGEngine::Vec2 shadowmapsize(0.0,0.0);
	try
	{
		shadows_generateBuffer(fbo);
		shadowmapsize.x = static_cast<GLfloat>(SHADOW_WIDTH);
		shadowmapsize.y = static_cast<GLfloat>(SHADOW_HEIGHT);
	}
	catch (std::string& _s)
	{
		std::cout << _s;
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	//////////////////////////////////////////////////////////////////////////

	double lastTime = glfwGetTime();
	int nbFrames = 0;
	int count_=0;
	
	while (!glfwWindowShouldClose(window))
	{
		glfwGetFramebufferSize(window, &width, &height);
		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if ( currentTime - lastTime >= 1.0 )
		{ 
			// If last prinf() was more than 1 sec ago printf and reset timer
			double spf = 1000.0/double(nbFrames);
			int fps = static_cast<int>(1.0/spf*1000.0);
			std::string t("OpenGL Testing, ");
			std::string title = std::string("SPF: ") + std::to_string((long double)spf) + std::string(" ms/frame. FPS: ") + std::to_string((long double)fps);
			glfwSetWindowTitle(window, (t+title).c_str());
			//printf("SPF: %f ms/frame. FPS: %d \n", spf, fps);
			nbFrames = 0;
			lastTime += 1.0;
		}		
		//////////////////////////////////////////////////////////////////////////
		// DEPTH PASS
		//////////////////////////////////////////////////////////////////////////

		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo.depthMapFBO);
		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClearDepth(1.0f);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glEnable(GL_DEPTH_TEST);

		ModelTransformStack.LoadIdentity();
		ModelTransformStack.Translate(CGEngine::Vec3(0.0,0.0,-5.0));
		simpleDepthShader.StartProgram();

		//Draw Sphere
		{
			ModelTransformStack.PushMatrix();
			ModelTransformStack.UniformScale(0.5f);
			ModelTransformStack.Translate(CGEngine::Vec3(2.0,0.0,0.0));
			ModelTransformStack.Rotate(static_cast<GLfloat>(glfwGetTime())*15.0f, CGEngine::Vec3(0.0f, 1.0f, 0.0f));
			glUniformMatrix4fv(lsm_loc, 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));
			glUniformMatrix4fv(mt_loc, 1, GL_FALSE, ModelTransformStack.getCurrentTransformPointer());
			sphere.VOnDraw();
			ModelTransformStack.PopMatrix();
		}
		//Draw Dice
		{
			ModelTransformStack.PushMatrix();
			ModelTransformStack.Translate(CGEngine::Vec3(-1.5, -1.0, 0.0));
			ModelTransformStack.Rotate(static_cast<GLfloat>(glfwGetTime())*-15.0f, CGEngine::Vec3(0.0f, 1.0f, 0.0f));
			glUniformMatrix4fv(lsm_loc, 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));
			glUniformMatrix4fv(mt_loc, 1, GL_FALSE, ModelTransformStack.getCurrentTransformPointer());
			dice.VOnDraw();
			ModelTransformStack.PopMatrix();
		}
		//Draw Jeep
		{
			ModelTransformStack.PushMatrix();
			ModelTransformStack.UniformScale(0.5);
			ModelTransformStack.Translate(CGEngine::Vec3(0.0, 0.0, -15.0));
			ModelTransformStack.Rotate(static_cast<GLfloat>(glfwGetTime())*-15.0f, CGEngine::Vec3(0.0f, 1.0f, 0.0f));
			glUniformMatrix4fv(lsm_loc, 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));
			glUniformMatrix4fv(mt_loc, 1, GL_FALSE, ModelTransformStack.getCurrentTransformPointer());
			jeep.VOnDraw();
			ModelTransformStack.PopMatrix();
		}
		simpleDepthShader.StopProgram();
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
		glDrawBuffer(GL_BACK);

		//////////////////////////////////////////////////////////////////////////
		// RENDER PASS
		//////////////////////////////////////////////////////////////////////////
		/*if (count_%10==0)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, fbo.color_tex2);
			ReadTextureFromGPUToFile(data_path+std::string("result_")+std::to_string((_ULonglong)count_)+std::string(".ppm"));
			//ReadBufferFromGPUToFile(fbo.depthMapFBO, SHADOW_WIDTH, SHADOW_HEIGHT, data_path+std::string("result_")+std::to_string((_ULonglong)count_)+std::string(".ppm"));
		}
		count_++;

		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, fbo.depthMap);

		fsq_shader.StartProgram();
		glUniform1i(fsq_tex_loc, 0);
		fsq.VOnDraw();

		fsq_shader.StopProgram();*/

		glViewport(0, 0, width, height);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClearDepth(1.0f);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glEnable(GL_DEPTH_TEST);
		glCullFace(GL_BACK); 

		ModelTransformStack.LoadIdentity();
		ModelTransformStack.Translate(CGEngine::Vec3(0.0,0.0,-5.0));
		CGEngine::Matrix3x3 nm(1.0);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, fbo.depthMap);

		//Draw Sphere
		{
			shader.StartProgram();
			ModelTransformStack.PushMatrix();
			ModelTransformStack.UniformScale(0.5f);
			ModelTransformStack.Translate(CGEngine::Vec3(2.0,0.0,0.0));
			ModelTransformStack.Rotate(static_cast<GLfloat>(glfwGetTime())*15.0f, CGEngine::Vec3(0.0f, 1.0f, 0.0f));
			glUniform1i( tex_loc, 0 );
			glUniform1i( depthmap_loc, 1 );
			glUniform3fv( light_loc, 1, glm::value_ptr(lightDir) );
			glUniform2fv( texsize_loc, 1, glm::value_ptr(shadowmapsize));
			CGEngine::CMatrixStack::ComputeNormalMatrix( nm, ModelTransformStack.getCurrentTransform(), view);
			glUniformMatrix4fv(mm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
			glUniformMatrix3fv(nm_loc, 1, false, glm::value_ptr(nm));
			glUniformMatrix4fv(proj_loc, 1, false, glm::value_ptr(projection));
			glUniformMatrix4fv(view_loc, 1, false, glm::value_ptr(view));
			glUniformMatrix4fv(lspmat_loc, 1, false, glm::value_ptr(lightSpaceMatrix));
			sphere.VOnDraw();
			ModelTransformStack.PopMatrix();
			shader.StopProgram();
		}
		//Draw Dice
		{
			shader.StartProgram();
			ModelTransformStack.PushMatrix();
			ModelTransformStack.Translate(CGEngine::Vec3(-1.5, -1.0, 0.0));
			ModelTransformStack.Rotate(static_cast<GLfloat>(glfwGetTime())*-15.0f, CGEngine::Vec3(0.0f, 1.0f, 0.0f));
			glUniform1i( tex_loc, 0 );
			glUniform1i( depthmap_loc, 1 );
			glUniform3fv( light_loc, 1, glm::value_ptr(lightDir) );
			glUniform2fv( texsize_loc, 1, glm::value_ptr(shadowmapsize));
			CGEngine::CMatrixStack::ComputeNormalMatrix( nm, ModelTransformStack.getCurrentTransform(), view);
			glUniformMatrix4fv(mm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
			glUniformMatrix3fv(nm_loc, 1, false, glm::value_ptr(nm));
			glUniformMatrix4fv(proj_loc, 1, false, glm::value_ptr(projection));
			glUniformMatrix4fv(view_loc, 1, false, glm::value_ptr(view));
			glUniformMatrix4fv(lspmat_loc, 1, false, glm::value_ptr(lightSpaceMatrix));
			dice.VOnDraw();
			ModelTransformStack.PopMatrix();
			shader.StopProgram();
		}
		//Draw Jeep
		{
			shader.StartProgram();
			ModelTransformStack.PushMatrix();
			ModelTransformStack.UniformScale(0.5);
			ModelTransformStack.Translate(CGEngine::Vec3(0.0, 0.0, -15.0));
			ModelTransformStack.Rotate(static_cast<GLfloat>(glfwGetTime())*-15.0f, CGEngine::Vec3(0.0f, 1.0f, 0.0f));
			glUniform1i( tex_loc, 0 );
			glUniform1i( depthmap_loc, 1 );
			glUniform3fv( light_loc, 1, glm::value_ptr(lightDir) );
			glUniform2fv( texsize_loc, 1, glm::value_ptr(shadowmapsize));
			CGEngine::CMatrixStack::ComputeNormalMatrix( nm, ModelTransformStack.getCurrentTransform(), view);
			glUniformMatrix4fv(mm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
			glUniformMatrix3fv(nm_loc, 1, false, glm::value_ptr(nm));
			glUniformMatrix4fv(proj_loc, 1, false, glm::value_ptr(projection));
			glUniformMatrix4fv(view_loc, 1, false, glm::value_ptr(view));
			glUniformMatrix4fv(lspmat_loc, 1, false, glm::value_ptr(lightSpaceMatrix));
			jeep.VOnDraw();
			ModelTransformStack.PopMatrix();
			shader.StopProgram();
		}
		
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteTextures(1, &(fbo.depthMap));
	glDeleteTextures(1, &(fbo.color_tex));
	glDeleteTextures(1, &(fbo.color_tex2));
	glDeleteTextures(1, &(fbo.color_tex3));
	glDeleteTextures(1, &(fbo.depthMap));
	//Bind 0, which means render to back buffer, as a result, depthMapFBO is unbound
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	glDeleteFramebuffersEXT(1, &fbo.depthMapFBO);

	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}