#pragma once

#include "CGEGeometry.h"
#include "CGEAttributes.h"
#include <map>

#include <boost/noncopyable.hpp>

namespace CGEngine
{
	typedef CGEngine::Vec3		Vec3;
	typedef CGEngine::Vec4		Vec4;
	typedef CGEngine::Vec3DVec	Vec3DVec;
	typedef CGEngine::Vec2DVec	Vec2DVec;
	typedef CGEngine::Matrix4x4 Matrix4x4;
	typedef std::vector<GLuint> GLuintVec;

	/*
	 *Base class for generating data to later transfer to GPU for rendering
	 */
	class CRenderableDataGenerator : public boost::noncopyable
	{
	public:
		CRenderableDataGenerator(){}
		virtual ~CRenderableDataGenerator();
	
		virtual bool VMakeObject(bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace) = 0;

		const Vec3DVec& GetVertices() const;
		const Vec3DVec& GetNormalVectors() const;
		const Vec2DVec& GetTextureCoordinates() const;
		const Vec3DVec& GetBinormalVector() const;
		const Vec3DVec& GetTangentVectors() const;
		const GLuintVec& GetFaceIndices() const;

		void CopyVertices(Vec3DVec& _vertices) const;
		void CopyNormals(Vec3DVec& _normals) const;
		void CopyTextureCoordinates(Vec2DVec& _texcoords) const;
		void CopyTangentSpace(Vec3DVec& _binormals, Vec3DVec& _tangents) const;
		void CopyFaceIndices(GLuintVec& _indices) const;

	public:

		bool AddVertexAttributeStorage(const std::string& _attribName, const Attributes::AttribStoragePtr& _attribStorage);
		Attributes::AttribStoragePtr GetVertexAttributeStorage(const std::string& _attribName) const;
		bool RemoveVertexAttributeStorage(const std::string& _attribName);

		/* Add an attribute with _name and default value _t. If an attribute with _name already exists,
		it returns the existing attribute */
		template<class T>
		Attributes::CAttributeWrapper<T> AddVertexAttribute(const std::string& _attribName, const T& _t = T())
		{
			return Attributes::CAttributeWrapper<T>( m_VertexAttributeContainer.AddAttribute<T>(_attribName, _t) );
		}

		//Get existing attribute with name _attribName. If it does not exist, an invalid attribute wrapper is returned.
		template<class T>
		Attributes::CAttributeWrapper<T> GetVertexAttribute(const std::string& _attribName) const
		{
			return Attributes::CAttributeWrapper<T>( m_VertexAttributeContainer.Get(_attribName) );
		}

		//Remove existing attribute with name _attribName. Returns false, if attribute does not exist.
		bool RemoveVertexAttribute(const std::string& _attribName);

		void GetAttributeNames(std::vector<std::string>& _names) const;

		bool HasAttributes() const;

		std::size_t GetNumAttributes() const;

		const Attributes::CAttributeContainer& GetAttributeContainer() const;
	
	protected:
		Vec3DVec	m_vertices;
		Vec3DVec	m_normals;
		Vec3DVec	m_binormals;
		Vec3DVec	m_tangents;
		Vec2DVec	m_texcoords;
		GLuintVec	m_face_indices;

		//Map an attribute name to an attribute storage strategy
		std::map <std::string, Attributes::AttribStoragePtr> m_VertexAttributeMap;
		typedef Attributes::CAttributeContainer AttributeContainer;
		//Collection of vertex attributes
		AttributeContainer m_VertexAttributeContainer;


		virtual void make_normals() = 0;
		virtual void make_texcoords() = 0;
		virtual void make_tangentspace() = 0;
		
	private:
	};
}