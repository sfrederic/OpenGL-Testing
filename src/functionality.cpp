#include "functionality.h"

namespace CGEngine
{
	namespace Functionality
{
	void cout_gl_error(GLenum _error)
		{
			switch (_error)
			{
			case GL_INVALID_ENUM: std::cerr << "GL_INVALID_ENUM\n";
				break;
			case GL_INVALID_VALUE: std::cerr << "GL_INVALID_VALUE\n";
				break;
			case GL_INVALID_OPERATION: std::cerr << "GL_INVALID_OPERATION\n";
				break;
			case GL_NO_ERROR: std::cerr << "No ERROR.\n";
				break;
			default: std::cerr << "ERROR but no code!\n";
				break;
			}
		}
		
		void cout_gl_frambufferError( GLenum _error )
		{
			switch (_error)
			{
			case GL_FRAMEBUFFER_UNDEFINED: std::cerr << "GL_FRAMEBUFFER_UNDEFINED\n";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT: std::cerr << "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT: std::cerr << "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT: std::cerr << "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER\n";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT: std::cerr << "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER\n";
				break;
			case GL_FRAMEBUFFER_UNSUPPORTED_EXT: std::cerr << "GL_FRAMEBUFFER_UNSUPPORTED\n";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_EXT: std::cerr << "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE\n";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS_EXT: std::cerr << "GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS\n";
				break;
			default: std::cerr << "Unknown framebuffer error!\n";
				break;
			}
		}
		
		void ReadDepthTextureToFile(const std::string& _output_file, GLsizei _width, GLsizei _height, std::int8_t _DepthPrecision /*= 24*/)
		{
			std::cout << "Reading depth texture from GPU..\n";
		
			std::vector<GLfloat> depth_data(_width*_height, 0.0f);
			glReadPixels(0, 0, _width, _height, GL_DEPTH_COMPONENT, GL_FLOAT, (GLvoid*)&depth_data[0]);
			if (glGetError()!= GL_NO_ERROR)
				cout_gl_error(glGetError());
		
			GLfloat d_min = std::numeric_limits<GLfloat>::max();
			GLfloat d_max = std::numeric_limits<GLfloat>::min();
			BOOST_FOREACH(GLfloat& val, depth_data)
			{
				if (val > 0.0f && val < 1.0f)
				{
					if (val < d_min)
						d_min = val;
					if (val > d_max)
						d_max = val;
				}	
			}
			GLfloat diff = d_max - d_min;
		
			//normalize to the range [0, ..., 1]
			BOOST_FOREACH(GLfloat& val, depth_data)
			{
				val = (val - d_min) / diff;
			}
			
			std::ofstream outf(_output_file);
			if (!outf)
			{
				// Print an error and exit
				std::cerr << "Uh oh, " << _output_file << " could not be opened for writing!\n";
				return;
			}
			outf << "P3\n" << _width << " " << _height << "\n" << 255 << "\n";
			int W = _width;
			for (int j = 0; j < _height; ++j)
			{
				for (int i = 0; i < W; ++i)
				{
					if (i < W - 1)
						for (int c = 0; c < 3; ++c)
							outf << static_cast<int>(depth_data[j*W + i] * 255.0f) << " ";
					else
					{
						for (int c = 0; c < 2; ++c)
							outf << static_cast<int>(depth_data[j*W + i] * 255.0f) << " ";
						outf << static_cast<int>(depth_data[j*W + i] * 255.0f);
					}
				}
				outf << "\n";
			}
		}
		
		void ReadTextureFromGPUToFile( const std::string& _output_file )
		{
			{
				std::cout << "Reading texture from GPU..\n";
				GLint w(0), h(0), i_f_(0);
				glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h);
				if (glGetError() != GL_NO_ERROR)
					cout_gl_error(glGetError());
				glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &w);
				if (glGetError() != GL_NO_ERROR)
					cout_gl_error(glGetError());
				glGetTexLevelParameteriv(GL_TEXTURE_2D, 0,  GL_TEXTURE_INTERNAL_FORMAT , &i_f_);
				if (glGetError() != GL_NO_ERROR)
					cout_gl_error(glGetError());
				std::cout << "Width: " << w << ", height: " << h << "\n";
		
				int int_form(0); GLenum final_format(0);
				switch (i_f_)
				{
				case GL_RGB: int_form = 3;
					std::cout << "Internal format is GL_RGB\n";
					final_format = GL_RGB;
					break;
				case GL_RGBA: int_form = 4;
					std::cout << "Internal format is GL_RGBA\n";
					final_format = GL_RGBA;
					break;
				case GL_DEPTH_COMPONENT: int_form = 4;
					final_format = GL_DEPTH_COMPONENT;
					std::cout << "Internal format is GL_DEPTH_COMPONENT\n";
					break;
				case GL_STENCIL_INDEX: int_form = 4;
					std::cout << "Internal format is GL_STENCIL_INDEX\n";
					final_format = GL_STENCIL_INDEX;
					break;
				case  GL_DEPTH_STENCIL: int_form = 4;
					std::cout << "Internal format is  GL_DEPTH_STENCIL\n";
					final_format = GL_DEPTH_STENCIL;
					break;
				case GL_RED: int_form = 4;
					std::cout << "Internal format is GL_RED\n";
					final_format = GL_RED;
					break;
				case  GL_GREEN: int_form = 4;
					std::cout << "Internal format is GL_GREEN\n";
					final_format = GL_GREEN;
					break;
				case GL_BLUE: int_form = 4;
					std::cout << "Internal format is GL_BLUE\n";
					final_format = GL_BLUE;
					break;
				case GL_RG: int_form = 4;
					std::cout << "Internal format is GL_RG\n";
					final_format = GL_RG;
					break;
				case GL_BGR: int_form = 3;
					std::cout << "Internal format is GL_BGR\n";
					final_format = GL_BGR;
					break;
				case GL_BGRA: int_form = 4;
					std::cout << "Internal format is GL_BGRA\n";
					final_format = GL_BGRA;
					break;
				case GL_RED_INTEGER: int_form = 4;
					std::cout << "Internal format is GL_RED_INTEGER\n";
					final_format = GL_RED_INTEGER;
					break;
				case GL_GREEN_INTEGER: int_form = 4;
					std::cout << "Internal format is GL_GREEN_INTEGER\n";
					final_format = GL_GREEN_INTEGER;
					break;
				case GL_BLUE_INTEGER: int_form = 4;
					std::cout << "Internal format is GL_BLUE_INTEGER\n";
					final_format = GL_BLUE_INTEGER;
					break;
				case GL_RG_INTEGER: int_form = 4;
					std::cout << "Internal format is GL_RG_INTEGER\n";
					final_format = GL_RG_INTEGER;
					break;
				case GL_RGB_INTEGER: int_form = 3;
					std::cout << "Internal format is GL_RGB_INTEGER\n";
					final_format = GL_RGB_INTEGER;
					break;
				case GL_RGBA_INTEGER: int_form = 4;
					std::cout << "Internal format is GL_RGBA_INTEGER\n";
					final_format = GL_RGBA_INTEGER;
					break;
				case  GL_BGR_INTEGER: int_form = 3;
					std::cout << "Internal format is  GL_BGR_INTEGER\n";
					final_format = GL_BGR_INTEGER;
					break;
				case  GL_BGRA_INTEGER: int_form = 4;
					std::cout << "Internal format is  GL_BGRA_INTEGER\n";
					final_format = GL_BGRA_INTEGER;
					break;
				case 1: int_form = 4;
					final_format = GL_DEPTH_COMPONENT;
					break;
				default:
					std::cout << "Internal format problem: " << i_f_ << "\n";
					return;
					break;
				}
				GLint type_(0); int size_of_(0); GLenum final_type(0);
				glGetTexLevelParameteriv(GL_TEXTURE_2D, 0,  GL_TEXTURE_RED_TYPE, &type_);
				switch (type_)
				{
				case GL_SIGNED_NORMALIZED:
					std::cout << "Type is GL_SIGNED_NORMALIZED\n";
					size_of_ = sizeof(GLfloat);
					final_type = GL_FLOAT;
					break;
				case GL_UNSIGNED_NORMALIZED:
					std::cout << "Type is GL_UNSIGNED_NORMALIZED\n";
					size_of_ = sizeof(GLfloat);
					final_type = GL_FLOAT;
					break;
				case GL_FLOAT:
					std::cout << "Type is GL_FLOAT\n";
					size_of_ = sizeof(GLfloat);
					final_type = GL_FLOAT;
					break;
				case GL_INT:
					std::cout << "Type is GL_INT\n";
					size_of_ = sizeof(GLint);
					final_type = GL_INT;
					break;
				case  GL_UNSIGNED_INT:
					std::cout << "Type is GL_UNSIGNED_INT\n";
					size_of_ = sizeof(GLuint);
					final_type = GL_UNSIGNED_INT;
					break;
				case  GL_UNSIGNED_BYTE:
					std::cout << "Type is GL_UNSIGNED_BYTE\n";
					size_of_ = sizeof(GLubyte);
					final_type = GL_UNSIGNED_BYTE;
					break;
				default:
					std::cout << "Unknown type!\n";
					return;
					break;
				}
		
				GLfloat* data = (GLfloat*)malloc( int_form * size_of_ * w * h );
				glGetTexImage(GL_TEXTURE_2D, 0, final_format, final_type, data);
				if (glGetError() != GL_NO_ERROR)
					cout_gl_error(glGetError());
				std::ofstream outf(_output_file);
				if (!outf)
				{
					// Print an error and exit
					std::cerr << "Uh oh, " << _output_file << " could not be opened for writing!\n";
					free(data);
					return;
				}
				outf << "P3\n" << w << " " << h << "\n" << 255 << "\n";
				int W = int_form * w;
				for (int j = h-1; j >= 0; --j)
				{
					for (int i = 0; i < W; i+=int_form)
					{
						if (int_form == 1)
						{
							outf << static_cast<int>(data[j*W +i]*255.0) << " ";
						} 
						else if (int_form == 3)
						{
							for (int c = 0; c < 3; ++c)
								outf << static_cast<int>(data[j*W + i + c]*255.0) << " ";
						}
						else
						{
							for (int c = 0; c < 3; ++c)
								outf << static_cast<int>(data[j*W + i + c]*255.0) << " ";
						}
					}
					outf << "\n";
				}
				free(data);
			}
		}
		
		void ReadBufferFromGPUToFile( const std::string& _output_file, int width, int height, int rgb_format /*= 4*/, float _scale /*= 1.0f*/ )
		{
			std::cout << "Reading pixels...\n";
			int rgb = rgb_format;
			//glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
			if (glGetError() != GL_NO_ERROR)
				cout_gl_error(glGetError());
		
			if (glGetError() != GL_NO_ERROR)
				cout_gl_error(glGetError());
		
			glPixelStorei(GL_PACK_ALIGNMENT, 1);
			if (glGetError() != GL_NO_ERROR)
				cout_gl_error(glGetError());
			std::vector<GLfloat> data(rgb*width*height, -1.0);
			
			glReadPixels(0, 0, width, height, GL_RGBA, GL_FLOAT, &data[0]);
			if (glGetError() != GL_NO_ERROR)
				cout_gl_error(glGetError());
		
			std::ofstream outf(_output_file);
			if (!outf)
			{
				// Print an error and exit
				std::cerr << "Uh oh, " << _output_file << " could not be opened for writing!\n";
				//free(data);
				data.clear();
				return;
			}
			outf << "P3\n" << width << " " << height << "\n" << 255 << "\n";
			int W = rgb*width;
			for (int j = height-1; j >= 0; --j)
			{
				for (int i = 0; i < W; i+=rgb)
				{
					if (rgb == 1)
					{
							outf << static_cast<int>( std::min(255.0f, data[j*W + i + 0]* _scale * 255.0f) ) << " ";
					} 
					else if (rgb == 3)
					{
						for (int c = 0; c < 3; ++c)
							outf << static_cast<int>( std::min(255.0f, data[j*W + i + c]* _scale * 255.0f) ) << " ";
					}
					else
					{
						for (int c = 0; c < 3; ++c)
							outf << static_cast<int>( std::min(255.0f, data[j*W + i + c]* _scale * 255.0f) ) << " ";
					}
				}
				outf << "\n";
			}
			//free(data);
			data.clear();
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}
}
}
