#ifndef __GLEW_H__
#define GLEW_STATIC
#include <GL/glew.h>
#endif
#include "CGEGeometry.h"
#include "shaders.h"

#ifndef shadow_mapping_h__
#define shadow_mapping_h__

#include <string>
#include "functionality.h"

struct FBO_Info
{
public:
	GLuint depthMapFBO;
	GLuint depthMap;
	GLuint color_tex;
	GLuint color_tex2;
	GLuint color_tex3;

	FBO_Info() : depthMapFBO(0), depthMap(0), color_tex(0), color_tex2(0), color_tex3(0) {}
};

static GLfloat near_plane = 1.0f;
static GLfloat far_plane = 40.0f;

const GLuint SHADOW_WIDTH = 512;
const GLuint SHADOW_HEIGHT = 512;

static const char* shadow_vertex_shader =
	"uniform mat4 lightSpaceMatrix;\n"
	"uniform mat4 model_transform;\n"
	"void main()\n"
	"{\n"
	"    gl_Position = lightSpaceMatrix * model_transform * gl_Vertex;\n"
	"}\n";
static const char* shadow_fragment_shader =
	"void main()\n"
	"{\n" 
	"	gl_FragData[1] = vec4(1.0, 1.0, 0.0, 1.0 );\n"
	"}\n";

void shadows_generateBuffer(FBO_Info& _info);


#endif // shadow_mapping_h__