#pragma once

#include "CGECGALTetrahedralMeshGenerator.h"
#include "functionality.h"
#include "CGEFramebufferObject.h"
#include "CGERectangleTexture.h"
#include "CGEBufferTextures.h"
#include "CGEFullScreenQuad.h"
#include "shaders.h"
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/filesystem.hpp>
#include <type_traits>

#define _QUADG_DEBUG

namespace CGEngine
{
	/************************************************************************/
	/* Preprocessing step
	-----------------------
	/************************************************************************/
	template<class TetrahedralMeshGenerator>
	class TranslucentObjectsPreprocessing
	{
	protected:

		//Stores for each vertex in the QuadGraph, the position and info if it is a surface or inner node
		class GraphNodeInfo
		{
		public:
			CGEngine::Vec3 m_Position; //position of the node in object space
			CGEngine::iVec2 m_TexCoord; //texture coordinate, identifies a node when stored to texture on GPU
			bool m_IsSurfaceNode;
			//Constructors
			GraphNodeInfo() : m_Position(CGEngine::Vec3(0.0f)), m_TexCoord(CGEngine::iVec2(0)), m_IsSurfaceNode(false) {}
			GraphNodeInfo(const CGEngine::Vec3& _pos, const CGEngine::iVec2& _texcoord, bool _isSurfaceNode) :
				m_Position(_pos), m_TexCoord(_texcoord), m_IsSurfaceNode(_isSurfaceNode) {}
			GraphNodeInfo(const GraphNodeInfo& _rhs) :
				m_Position(_rhs.m_Position), m_TexCoord(_rhs.m_TexCoord), m_IsSurfaceNode(_rhs.m_IsSurfaceNode) {}
			~GraphNodeInfo() {}
			//Assignment
			GraphNodeInfo& operator=(const GraphNodeInfo& _rhs)
			{
				m_Position = _rhs.m_Position;
				m_TexCoord = _rhs.m_TexCoord;
				m_IsSurfaceNode = _rhs.m_IsSurfaceNode;
				return *this;
			}
		};

		//BoostGraph typedefs
		//tag class for custom property
		struct vertex_info_t {
			typedef boost::vertex_property_tag kind; //note: must be named like this, or else we get compiler errors
		};
		//graph with custom property
		typedef boost::property<vertex_info_t, GraphNodeInfo> NodeInfo;
		typedef boost::adjacency_list<boost::setS,	/*(Out-)Edge container*/
			boost::vecS,							/*Vertex container*/
			boost::undirectedS,
			NodeInfo>				BoostGraph;

		typedef boost::adjacency_list<>::vertex_descriptor	BoostNodeHandle;
		typedef boost::adjacency_list<>::edge_descriptor	BoostEdgeHandle;

	public:
		using TMGShPtr		= typename std::shared_ptr<TetrahedralMeshGenerator>;
		using Cell_handle	= typename TetrahedralMeshGenerator::Cell_handle;
		using Vertex_handle = typename TetrahedralMeshGenerator::Vertex_handle;	
		using Facet			= typename TetrahedralMeshGenerator::Facet;
		using Point_3		= typename TetrahedralMeshGenerator::Point;
		using Triangulation = typename TetrahedralMeshGenerator::Triangulation3D;

		TranslucentObjectsPreprocessing()=delete;
		TranslucentObjectsPreprocessing(const TMGShPtr& _pTetMeshGenerator);
		~TranslucentObjectsPreprocessing();

		//Construct the quad graph from given tetrahedral mesh
		bool MakeQuadGraph();

		//Computes for each node the texture coordinates for texture lookup for the diffusion equation solver
		void ComputeNodeTextureCoordinates(std::uint32_t _block_size = 32);

		//Get texture coordinate for i-th node
		const iVec2& GetNodeTexCoord(std::uint32_t _i) const
		{
			return boost::get(m_vertex_info, boost::vertex(_i, m_QuadGraph)).m_TexCoord;
		}

		//Get i-th node position
		const Vec3& GetNodePosition(std::uint32_t _i) const
		{
			return boost::get(m_vertex_info, boost::vertex(_i, m_QuadGraph)).m_Position;
		}

		//Get the maximal texture coordinates that resulted from the texture coordinate computation
		void GetMaxNodeTexCoords(iVec2& _maxTexCoordSurfaceNodes, iVec2& _maxTexCoordInnerNodes, iVec2& _tex_dimensions) const
		{
			_maxTexCoordSurfaceNodes = m_max_texcoord_ns;
			_maxTexCoordInnerNodes = m_max_texcoord_ni;
			_tex_dimensions = m_texture_dimensions;
		}

		//Get quad graph
		const BoostGraph& GetQuadGraph() const
		{
			return m_QuadGraph;
		}

		// Get the surface mesh from the tetrahedral mesh, returns a texture buffer TB containing the texture coordinates 
		// for radiant flux value look-up (RFTC). Assigns 2D vectors as vertex attributes to mesh, to designate offset and range in the TB
		// where to access the RFTCs.
		std::unique_ptr<CGEngine::CGEBufferTexture>
		ExtractSurfaceMesh(CGEngine::CCGALMeshGenerator& _surface_mesh_gen, bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace)
		{
			std::string attribName = std::string("v:radiantfluxTexCoordRange");
			std::unique_ptr<CCGALMeshGenerator::SurfaceMesh> surface_mesh = std::make_unique<CCGALMeshGenerator::SurfaceMesh>();

			typedef CCGALMeshGenerator::SurfaceMesh::Property_map<CCGALMeshGenerator::Vertex_handle, Vec2> prop_map_type;
			prop_map_type prop_map; bool created = false;
			boost::tie(prop_map, created) = surface_mesh->add_property_map<CCGALMeshGenerator::Vertex_handle, Vec2>(attribName, Vec2(0.0f));
			if (!created)
				throw std::string(std::string("Could not create necessary property for surface mesh: ") + attribName);

			std::map<Vertex_handle, CCGALMeshGenerator::SurfaceMesh::Vertex_index> vertex_indices;

			std::vector<Vec2> radiantfluxTexCoordVec;
			int last_start(0);
			int last_end(0);

			for (auto c_it = m_TetMeshGenerator->CellsBegin(); c_it != m_TetMeshGenerator->CellsEnd() ; ++c_it)
			{
				std::vector<int> surface_facets_indices;
				int num_surface_facets = count_surface_facets_of_cell(c_it, surface_facets_indices);
				if (num_surface_facets  > 0)
				{
					for (auto sf_it = surface_facets_indices.begin(); sf_it != surface_facets_indices.end(); ++sf_it)
					{
						std::vector<CCGALMeshGenerator::SurfaceMesh::Vertex_index> vi(3);
						std::vector<Vertex_handle> vh(3);
						for (int i = 0; i < 3; ++i)
						{
							vh[i] = c_it->vertex(m_triangulation3D.vertex_triple_index(*sf_it, i));

							if (vertex_indices.find(vh[i]) == vertex_indices.end())
							{
								vi[i] = surface_mesh->add_vertex(CCGALMeshGenerator::Point_3(vh[i]->point().x(), vh[i]->point().y(), vh[i]->point().z()));
								vertex_indices[vh[i]] = vi[i];

								//texture coordinates
								BOOST_FOREACH(BoostNodeHandle& nh, m_surfaceVertexToSurfaceNodesMap[vh[i]])
								{
									iVec2 node_tex_coord = boost::get(m_vertex_info, boost::vertex(nh, m_QuadGraph)).m_TexCoord;
									Vec2 tex_coord_fluxtex = Vec2(static_cast<float>(node_tex_coord.x) + 0.5, static_cast<float>(node_tex_coord.y) + 0.5f);
									radiantfluxTexCoordVec.push_back(tex_coord_fluxtex);
									last_end++;
								}
								Vec2 tex_coord_range(static_cast<float>(last_start), static_cast<float>(last_end));
								prop_map[vi[i]] = tex_coord_range;
								last_start = last_end;
							}
							else
							{
								vi[i] = vertex_indices[vh[i]];
							}
						}
						surface_mesh->add_face(vi[0], vi[1], vi[2]);
					}
				}
			}
			_surface_mesh_gen.SetFileName("");
			_surface_mesh_gen.SetSurfaceMesh(surface_mesh);
			_surface_mesh_gen.VMakeObject(_compute_normals, _compute_texcoords, _compute_tangentspace);

			//setup radiant flux coordinate range attribute
			Attributes::AttribStoragePtr texcoord_attrib_storage =
				std::make_shared<Attributes::CFloatingPointAttributeStorage>(static_cast<GLint>(2));
			_surface_mesh_gen.AddVertexAttributeStorage(attribName, texcoord_attrib_storage);
			_surface_mesh_gen.CollectAttributes<Vec2>();
			//setup buffer texture
			GLint max_size(0);
			glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE, &max_size);
			if (radiantfluxTexCoordVec.size() > static_cast<std::size_t>(max_size))
				throw std::string("Translucent Objects preprocessing - ExtractSurfaceMesh: Not enough space on server side to store necessary data.");
			std::unique_ptr<CGEngine::CGEBufferTexture> texcoord_buffer = std::make_unique<CGEngine::CGEBufferTexture>();
			texcoord_buffer->VLoadTexture(radiantfluxTexCoordVec.size(), 0, GL_RG16F, 0, GL_DYNAMIC_DRAW,
				static_cast<GLvoid*>(&radiantfluxTexCoordVec[0]));
			return texcoord_buffer;
		}

		//Extract surface mesh composed of triangulated surface nodes
		void ExtractSurfaceRenderMesh(CGEngine::CCGALMeshGenerator& _surface_mesh_gen, bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace)
		{
			//Set up property for CGAL mesh, to contain the texture coordinates of the resulting flux texture
			std::string attribName = std::string("v:radiantfluxTexCoord");
			std::unique_ptr<CCGALMeshGenerator::SurfaceMesh> surface_mesh = std::make_unique<CCGALMeshGenerator::SurfaceMesh>();
			typedef CCGALMeshGenerator::SurfaceMesh::Property_map<CCGALMeshGenerator::Vertex_handle, Vec2> prop_map_type;
			prop_map_type prop_map; bool created = false;
			boost::tie(prop_map, created) = surface_mesh->add_property_map<CCGALMeshGenerator::Vertex_handle, Vec2>(attribName, Vec2(0.0f));
			if (!created)
				throw std::string(std::string("Could not create necessary property for surface mesh: ") + attribName);
			
			using VertexToNodeMapIterator = typename std::map<Vertex_handle, std::vector<BoostNodeHandle>>::iterator;
			VertexToNodeMapIterator mvn_it = m_surfaceVertexToSurfaceNodesMap.begin();
			VertexToNodeMapIterator mvn_end = m_surfaceVertexToSurfaceNodesMap.end();
			std::map<BoostNodeHandle, CCGALMeshGenerator::SurfaceMesh::Vertex_index> inserted_vertex_indices;
			//make mesh from surface nodes //m_surfaceFacetSurfaceNodesMap
			for (; mvn_it != mvn_end; ++mvn_it)
			{
				Vertex_handle vh = mvn_it->first;
				std::vector<BoostNodeHandle> nodeHandles;
				std::vector<Facet> incidentFacets(50); std::vector<Facet>::iterator f_it;
				f_it = m_triangulation3D.incident_facets(vh, incidentFacets.begin());
				incidentFacets.erase( f_it, incidentFacets.end());
				for (f_it = incidentFacets.begin(); f_it != incidentFacets.end(); ++f_it)
				{
						if (m_surfaceFacetSurfaceNodesMap.find(*f_it) != m_surfaceFacetSurfaceNodesMap.end())
							nodeHandles.push_back(m_surfaceFacetSurfaceNodesMap[*f_it]);
				}
				std::vector<CCGALMeshGenerator::SurfaceMesh::Vertex_index> face_vertices;
				/*BOOST_FOREACH(BoostNodeHandle& nh, mvn_it->second)*/
				BOOST_FOREACH(BoostNodeHandle& nh, nodeHandles)
				{
					iVec2 node_tex_coord = boost::get(m_vertex_info, boost::vertex(nh, m_QuadGraph)).m_TexCoord;
					Vec2 tex_coord_attrib = Vec2( static_cast<float>(node_tex_coord.x)+0.5, static_cast<float>(node_tex_coord.y)+0.5f );

					Vec3 node_pos = boost::get(m_vertex_info, boost::vertex(nh, m_QuadGraph)).m_Position;
					CCGALMeshGenerator::Point_3 vertex_pos = CCGALMeshGenerator::Point_3(node_pos.x, node_pos.y, node_pos.z);

					//Check if vertex corresponding to surface node has already been inserted in mesh
					if (inserted_vertex_indices.find(nh) == inserted_vertex_indices.end()) /*new vertex*/
					{
						CCGALMeshGenerator::SurfaceMesh::Vertex_index vi = surface_mesh->add_vertex(vertex_pos);
						face_vertices.push_back(vi);
						inserted_vertex_indices[nh] = vi;
						prop_map[vi] = tex_coord_attrib;
					} 
					else
					{
						face_vertices.push_back(inserted_vertex_indices[nh]);
					}
				}
				CCGALMeshGenerator::SurfaceMesh::Face_index fi =
					surface_mesh->add_face<std::vector<CCGALMeshGenerator::SurfaceMesh::Vertex_index>>(face_vertices);
			}
			//triangulate
			int num_faces = surface_mesh->number_of_faces();
			num_faces = surface_mesh->num_faces();
			CGAL::Polygon_mesh_processing::triangulate_faces(*surface_mesh);
			num_faces = surface_mesh->number_of_faces();
			num_faces = surface_mesh->num_faces();

			_surface_mesh_gen.SetFileName("");
			_surface_mesh_gen.SetSurfaceMesh(surface_mesh);
			_surface_mesh_gen.VMakeObject(_compute_normals, _compute_texcoords, _compute_tangentspace);

			//setup radiant flux texcoord attribute
			Attributes::AttribStoragePtr texcoord_attrib_storage =
				std::make_shared<Attributes::CFloatingPointAttributeStorage>(static_cast<GLint>(2));
			_surface_mesh_gen.AddVertexAttributeStorage(attribName, texcoord_attrib_storage);
			_surface_mesh_gen.CollectAttributes<Vec2>();
		}

		inline std::uint32_t GetNumberOfSurfaceNodes() const
		{
			return m_NumSurfaceNodes;
		}

		inline std::uint32_t GetNumberOfInnerNodes() const
		{
			return m_NumInnerNodes;
		}

#ifdef _QUADG_DEBUG
		//For graph rendering
		void GetNodePositions(Vec3DVec& _positions, GLuintVec& _drawing_indices)
		{
			_positions.clear(); _drawing_indices.clear();
			if ((boost::num_vertices(m_QuadGraph)==0) && (boost::num_edges(m_QuadGraph)==0))
			{
				return;
			}
			else
			{
				auto vertex_it = boost::vertices(m_QuadGraph);
				_positions.reserve(boost::num_vertices(m_QuadGraph));
				for (auto v_it = vertex_it.first; v_it != vertex_it.second; ++v_it)
				{
					_positions.push_back(m_vertex_info[*v_it].m_Position);
				}

				auto edge_it = boost::edges(m_QuadGraph);
				_drawing_indices.reserve(boost::num_edges(m_QuadGraph));
				for (auto e_it = edge_it.first; e_it != edge_it.second; ++e_it)
				{
					_drawing_indices.push_back(static_cast<GLuint>(boost::source(*e_it, m_QuadGraph)));
					_drawing_indices.push_back(static_cast<GLuint>(boost::target(*e_it, m_QuadGraph)));
				}
			}
		}
#endif

	private:
		std::uint32_t m_NumSurfaceNodes;
		std::uint32_t m_NumInnerNodes;

		//check if cell(tetrahedron) is within the triangulation
		inline bool is_in_triangulation(const Cell_handle& _c)
		{
			return m_TetMeshGenerator->IsInTriangulation(_c);
		}

		//Get the number of surface facets and the facets of a cell that are on the surface of the tetrahedral mesh
		inline int count_surface_facets_of_cell(const Cell_handle& _c, std::vector<int>& _surface_facets_indices)
		{
			int count(0);
			for (int i=0; i < 4; ++i)
				if (!is_in_triangulation(_c->neighbor(i)))
				{
					count++;
					_surface_facets_indices.push_back(i);
				}
			return count;
		}

		//Constructing the quad-graph (helper functions);
		//Construct inner node
		BoostNodeHandle make_inner_node(const Cell_handle& _c);
		//Construct surface node (centroid of surface facet induced by_opposite_vertex_index). Adds an edge to _innerNode !!
		BoostNodeHandle make_surface_node(const Cell_handle& _c, const BoostNodeHandle& _innerNode, int _opposite_vertex_index);
		//virtually split a cell into four cells, and insert surface and inner nodes to cell-node-map
		void virtual_cell_split(const Cell_handle& _c, std::vector<BoostNodeHandle>& _new_nodes);
		//add edge to graph
		inline void make_edge(const Cell_handle& _current_cell, int _neighbor_index, int _curr_cell_node_index)
		{
			Cell_handle c_neigh = _current_cell->neighbor(_neighbor_index);
			if (!is_in_triangulation(c_neigh))
				return;
			//check if neighbor cell is split
			if (m_cellToNodeMap[c_neigh].size() > 1)
			{
				//use 1-to-1 correspondence of facets and inner nodes of split cell
				for (int j = 0; j < 4; ++j)
				{
					if (c_neigh->neighbor(j) == _current_cell)
					{
						boost::add_edge(m_cellToNodeMap[_current_cell].at(_curr_cell_node_index), m_cellToNodeMap[c_neigh].at(j), m_QuadGraph);
						break;
					}
				}
			}
			else
			{
				//if neighbor cell is not split, it has only one inner node
				boost::add_edge(m_cellToNodeMap[_current_cell].at(_curr_cell_node_index), m_cellToNodeMap[c_neigh].at(0), m_QuadGraph);
			}
		}
		
		//numerically stable mean computation to determine centroid of tetrahedral cell
		inline void cell_centroid(const Cell_handle& _c, CGEngine::Vec3& node_position)
		{
			node_position = CGEngine::Vec3( /* = mean of the four points of cell _c */
				static_cast<float>(_c->vertex(0)->point().x()),
				static_cast<float>(_c->vertex(0)->point().y()),
				static_cast<float>(_c->vertex(0)->point().z())
			);
			for (int i = 1; i < 4; ++i)
			{
				node_position[0] = node_position[0] + (1.0f / static_cast<float>(i + 1))*(static_cast<float>(_c->vertex(i)->point().x()) - node_position[0]);
				node_position[1] = node_position[1] + (1.0f / static_cast<float>(i + 1))*(static_cast<float>(_c->vertex(i)->point().y()) - node_position[1]);
				node_position[2] = node_position[2] + (1.0f / static_cast<float>(i + 1))*(static_cast<float>(_c->vertex(i)->point().z()) - node_position[2]);
			}
		}

		//Data generator that generated the tetrahedral mesh for a given surface mesh
		TMGShPtr m_TetMeshGenerator;
		//Quad graph: Dual graph of tetrahedral mesh, plus surface nodes (corresponding to surface facets)
		BoostGraph	m_QuadGraph;

	private:
		//helper structures
		using CellToNodeMap = typename std::map<Cell_handle, std::vector<BoostNodeHandle>>; //map tetrahedral cells to nodes
		using VertexToNodeMap = typename std::map<Vertex_handle, std::vector<BoostNodeHandle>>; //map cell vertices to incident graph nodes
		using FacetToNodeMap = typename std::map<Facet, BoostNodeHandle>;
		CellToNodeMap m_cellToNodeMap;
		FacetToNodeMap m_surfaceFacetSurfaceNodesMap;
		VertexToNodeMap m_surfaceVertexToSurfaceNodesMap; //for a surface vertex store surface nodes corresponding to incident surface facets
		const Triangulation& m_triangulation3D; //actual tetrahedral-mesh
		using NodePropertyMap = typename boost::property_map<BoostGraph, vertex_info_t>::type;
		using NodeInfoType = typename boost::property_traits<NodePropertyMap>::value_type;
		NodePropertyMap m_vertex_info; //vertex info for graph
		iVec2 m_max_texcoord_ns, m_max_texcoord_ni, m_texture_dimensions;
	protected:

		struct bfs_state
		{
			std::uint32_t m_texblockWidth_ni, m_texblockHeight_ns; //block width of inner-nodes-block, height of surface-node-block in the final textures
			std::uint32_t curr_u_ni, curr_v_ni, curr_u_ns, curr_v_ns;

			bfs_state() = delete;

			bfs_state(std::uint32_t _texblockWidht, std::uint32_t _texblockHeight) :
				m_texblockWidth_ni(_texblockWidht), m_texblockHeight_ns(_texblockHeight)
			{
				curr_u_ni = curr_u_ns = curr_v_ns = 0;
				curr_v_ni = _texblockHeight;
			}
		};

		//visitor class for breadth-first-search-algo. Assigns texture coordinate to each node
		class texcoord_bfs_visitor : public boost::default_bfs_visitor
		{
		public:
			NodePropertyMap* m_pvertex_info; //vertex info for graph
			bfs_state* m_pCurrentState;

			//Constructors
			texcoord_bfs_visitor(NodePropertyMap* _npm, bfs_state* _new_state) :
				m_pvertex_info(_npm),  m_pCurrentState(_new_state)
			{
				if ((_npm == nullptr) || (_new_state == nullptr))
					throw std::string("QuadGraph BFS visitor exception: Null pointer parameter received!");
			}

			texcoord_bfs_visitor(const texcoord_bfs_visitor& _vis) :
				m_pvertex_info(_vis.m_pvertex_info), m_pCurrentState(_vis.m_pCurrentState)
			{
				if ((m_pvertex_info == nullptr) || (m_pCurrentState == nullptr))
					throw std::string("QuadGraph BFS visitor exception: Null pointer parameter received!");
			}

			~texcoord_bfs_visitor() { /*std::cout << "texcoord_bfs_visitor destructor called.\n";*/ }

			//assign texture coordinate to visited node
			template <typename Vertex, typename Graph>
			void finish_vertex(Vertex u, Graph & g) const
			{
				NodeInfoType& nit = boost::get(*m_pvertex_info, u);
				if (nit.m_IsSurfaceNode)
				{
					nit.m_TexCoord[0] = m_pCurrentState->curr_u_ns;
					nit.m_TexCoord[1] = m_pCurrentState->curr_v_ns;
					m_pCurrentState->curr_u_ns++;
					if (m_pCurrentState->curr_u_ns >= m_pCurrentState->m_texblockWidth_ni)
					{
						m_pCurrentState->curr_u_ns = 0;
						m_pCurrentState->curr_v_ns++;
					}
				} 
				else
				{
					nit.m_TexCoord[0] = m_pCurrentState->curr_u_ni;
					nit.m_TexCoord[1] = m_pCurrentState->curr_v_ni;
					m_pCurrentState->curr_u_ni++;
					if (m_pCurrentState->curr_u_ni >= m_pCurrentState->m_texblockWidth_ni)
					{
						m_pCurrentState->curr_u_ni = 0;
						m_pCurrentState->curr_v_ni++;
					}
				}
				boost::put(*m_pvertex_info, u, GraphNodeInfo(nit));
			}
		};
	};

	//////////////////////////////////////////////////////////////////////////
	// TranslucentObjectsPreprocessing implementation
	//////////////////////////////////////////////////////////////////////////

	template<class TetrahedralMeshGenerator>
	TranslucentObjectsPreprocessing<TetrahedralMeshGenerator>::TranslucentObjectsPreprocessing(const TMGShPtr& _pTetMeshGenerator) :
		m_TetMeshGenerator(_pTetMeshGenerator), m_NumInnerNodes(0), m_NumSurfaceNodes(0),
		m_triangulation3D(_pTetMeshGenerator->GetTriangulation())
	{
	}

	template<class TetrahedralMeshGenerator>
	TranslucentObjectsPreprocessing<TetrahedralMeshGenerator>::~TranslucentObjectsPreprocessing()
	{
		m_QuadGraph.clear();
		m_cellToNodeMap.clear();
		m_surfaceVertexToSurfaceNodesMap.clear();
		m_surfaceFacetSurfaceNodesMap.clear();
	}

	template<class TetrahedralMeshGenerator>
	bool TranslucentObjectsPreprocessing<TetrahedralMeshGenerator>::MakeQuadGraph()
	{
		m_QuadGraph.clear();
		m_cellToNodeMap.clear();
		m_surfaceVertexToSurfaceNodesMap.clear();
		m_surfaceFacetSurfaceNodesMap.clear();
		m_vertex_info = boost::get(vertex_info_t(), m_QuadGraph);
		auto cells_end = m_TetMeshGenerator->CellsEnd();

		//create nodes
		for (auto c_it = m_TetMeshGenerator->CellsBegin(); c_it != cells_end; ++c_it)
		{
			std::vector<int> surface_facets_indices;
			surface_facets_indices.reserve(3);
			int num_surface_facets = count_surface_facets_of_cell(c_it, surface_facets_indices);

			switch (num_surface_facets)
			{
			case 0:
				m_cellToNodeMap[c_it].push_back(make_inner_node(c_it));
				break;
			case 1:
			{
				BoostNodeHandle nh = make_inner_node(c_it);
				make_surface_node(c_it, nh, surface_facets_indices[0]);
				m_cellToNodeMap[c_it].push_back(nh);
				break;
			}
			default: /*more than one surface facet for this cell*/
				virtual_cell_split(c_it, m_cellToNodeMap[c_it]);
				break;
			}
		}
		//create edges
		auto m_end = m_cellToNodeMap.end();
		for (auto m_it = m_cellToNodeMap.begin(); m_it != m_end; ++m_it)
		{
			int num_inner_nodes = static_cast<int>(m_it->second.size());
			for (int current_inner_node = 0; current_inner_node < num_inner_nodes; ++current_inner_node)
				for (int neighbor_index = 0; neighbor_index < 4; ++neighbor_index) //for each neighbor cell
				{
					switch (num_inner_nodes)
					{
					case 1:
						make_edge(m_it->first, neighbor_index, current_inner_node);
						break;
					case 4:
						make_edge(m_it->first, current_inner_node, current_inner_node);
						break;
					default: /*shouldn't get here*/
						throw std::string("Quad-Graph-Node has neither 1 nor 4 neighbors!");
						break;
					}
				}
		}

		/*auto vertex_it = boost::vertices(m_QuadGraph);
		for (auto v_it = vertex_it.first; v_it != vertex_it.second; ++v_it)
		{
			assert( (boost::in_degree(*v_it, m_QuadGraph) == 1) || (boost::in_degree(*v_it, m_QuadGraph) == 4));
		}*/

		return true;
	}

	template<class TetrahedralMeshGenerator>
	typename TranslucentObjectsPreprocessing<TetrahedralMeshGenerator>::BoostNodeHandle
		TranslucentObjectsPreprocessing<TetrahedralMeshGenerator>::make_inner_node(const Cell_handle& _c)
	{
		BoostNodeHandle nh = boost::add_vertex(m_QuadGraph);
		m_NumInnerNodes++;
		//compute centroid of tetrahedron cell
		CGEngine::Vec3 node_position(0.0f);
		cell_centroid(_c, node_position);

		//vertex info
		boost::put(m_vertex_info, nh, GraphNodeInfo(node_position, CGEngine::iVec2(0), false));

		return nh;
	}

	template<class TetrahedralMeshGenerator>
	typename TranslucentObjectsPreprocessing<TetrahedralMeshGenerator>::BoostNodeHandle
		TranslucentObjectsPreprocessing<TetrahedralMeshGenerator>::make_surface_node(
			const Cell_handle& _c,
			const BoostNodeHandle& _innerNode,
			int _opposite_vertex_index)
	{
		BoostNodeHandle nh = boost::add_vertex(m_QuadGraph);
		m_NumSurfaceNodes++;
		//auto T = m_TetMeshGenerator->GetTriangulation(); //too slow!
		Vertex_handle vh1 = _c->vertex(m_triangulation3D.vertex_triple_index(_opposite_vertex_index, 0));
		Vertex_handle vh2 = _c->vertex(m_triangulation3D.vertex_triple_index(_opposite_vertex_index, 1));
		Vertex_handle vh3 = _c->vertex(m_triangulation3D.vertex_triple_index(_opposite_vertex_index, 2));
		m_surfaceVertexToSurfaceNodesMap[vh1].push_back(nh);
		m_surfaceVertexToSurfaceNodesMap[vh2].push_back(nh);
		m_surfaceVertexToSurfaceNodesMap[vh3].push_back(nh);
		Facet f = Facet(_c, _opposite_vertex_index);
		m_surfaceFacetSurfaceNodesMap[f]=nh;
		CGEngine::Vec3 node_position(
			static_cast<float>((vh1->point().x() + vh2->point().x() + vh3->point().x()) / 3.),
			static_cast<float>((vh1->point().y() + vh2->point().y() + vh3->point().y()) / 3.),
			static_cast<float>((vh1->point().z() + vh2->point().z() + vh3->point().z()) / 3.)
		);
		//vertex info
		boost::put(m_vertex_info, nh, GraphNodeInfo(node_position, CGEngine::iVec2(0), true));
		boost::add_edge(_innerNode, nh, m_QuadGraph);
		return nh;
	}

	template<class TetrahedralMeshGenerator>
	void TranslucentObjectsPreprocessing<TetrahedralMeshGenerator>::virtual_cell_split(
		const Cell_handle& _c,
		std::vector<BoostNodeHandle>& _new_nodes)
	{
		//auto T = m_TetMeshGenerator->GetTriangulation(); //time consuming!! removed
		/*_new_nodes.clear();*/ 
		_new_nodes.reserve(4);
		//compute central position
		CGEngine::Vec3 centroid(0.0f);
		cell_centroid(_c, centroid);
		//compute centroid for each new cell and check for surface facets (to insert surface nodes)
		for (int i = 0; i < 4; ++i)
		{
			BoostNodeHandle nh = boost::add_vertex(m_QuadGraph);
			m_NumInnerNodes++;
			std::vector<CGEngine::Vec3> points;
			points.push_back(centroid);

			Vertex_handle vh1 = _c->vertex(m_triangulation3D.vertex_triple_index(i, 0));
			Vertex_handle vh2 = _c->vertex(m_triangulation3D.vertex_triple_index(i, 1));
			Vertex_handle vh3 = _c->vertex(m_triangulation3D.vertex_triple_index(i, 2));

			points.push_back(CGEngine::Vec3(static_cast<float>(vh1->point().x()), static_cast<float>(vh1->point().y()), static_cast<float>(vh1->point().z())));
			points.push_back(CGEngine::Vec3(static_cast<float>(vh2->point().x()), static_cast<float>(vh2->point().y()), static_cast<float>(vh2->point().z())));
			points.push_back(CGEngine::Vec3(static_cast<float>(vh3->point().x()), static_cast<float>(vh3->point().y()), static_cast<float>(vh3->point().z())));

			CGEngine::Vec3 virt_centroid = CGEngine::Functionality::compute_mean_recursive<float, CGEngine::Vec3>(points);

			//vertex info
			boost::put(m_vertex_info, nh, GraphNodeInfo(virt_centroid, CGEngine::iVec2(0), false));

			if (!is_in_triangulation(_c->neighbor(i)))
				make_surface_node(_c, nh, i); //make surface node, if currently split cell has a surface facet

			_new_nodes.push_back(nh);
		}
		//interconnect inner nodes of virtual cells
		boost::add_edge(_new_nodes[0], _new_nodes[1], m_QuadGraph);
		boost::add_edge(_new_nodes[0], _new_nodes[2], m_QuadGraph);
		boost::add_edge(_new_nodes[0], _new_nodes[3], m_QuadGraph);
		boost::add_edge(_new_nodes[1], _new_nodes[2], m_QuadGraph);
		boost::add_edge(_new_nodes[1], _new_nodes[3], m_QuadGraph);
		boost::add_edge(_new_nodes[2], _new_nodes[3], m_QuadGraph);
	}

	template<class TetrahedralMeshGenerator>
	void TranslucentObjectsPreprocessing<TetrahedralMeshGenerator>::ComputeNodeTextureCoordinates(std::uint32_t _block_size /* = 32 */)
	{
		if ((boost::num_vertices(m_QuadGraph) == 0) && (boost::num_edges(m_QuadGraph) == 0))
			return;
		/*std::uint32_t numNodes = m_NumInnerNodes + m_NumSurfaceNodes;
		std::uint32_t ns_max_v(0), ns_max_u(0), ni_max_v(0), ni_max_u(0);
		std::uint32_t r(_block_size), r_s(0); //r = block size for inner nodes, r_s = block height for surface nodes

		if (r * r > num_NumInnerNodesmNodes) //there is more space than inner nodes -> shrink block size
		{
			r = static_cast<std::uint32_t>(std::ceilf(std::sqrtf(static_cast<float>(m_NumInnerNodes))));
			r_s = static_cast<std::uint32_t>(std::ceilf(static_cast<float>(m_NumSurfaceNodes) / static_cast<float>(r)));
		}

		std::uint32_t numBlocks_ni = static_cast<std::uint32_t>(std::ceilf(static_cast<float>(m_NumInnerNodes) / static_cast<float>(r*r)));*/

		std::uint32_t r(_block_size), r_s(0); //r = block width for all node kinds, r_s = block height for surface nodes
		r = static_cast<std::uint32_t>(std::ceilf(std::sqrtf(static_cast<float>(m_NumInnerNodes))));
		r_s = static_cast<std::uint32_t>(std::ceilf(static_cast<float>(m_NumSurfaceNodes) / static_cast<float>(r)));

		bfs_state* bfsState = new bfs_state(r, r_s);
		texcoord_bfs_visitor vis(&m_vertex_info, bfsState);
		boost::breadth_first_search(m_QuadGraph, boost::vertex(0, m_QuadGraph), boost::visitor(vis));
		m_max_texcoord_ns = iVec2((bfsState->curr_u_ns == 0 ? (r) : bfsState->curr_u_ns), bfsState->curr_v_ns);
		m_max_texcoord_ni = iVec2((bfsState->curr_u_ni == 0 ? (r) : bfsState->curr_u_ni), bfsState->curr_v_ni);
		m_texture_dimensions = iVec2(r, r + r_s);
		delete bfsState;
	}

	//Define the preprocessing classes for the different tetrahedral mesh generators
	typedef TranslucentObjectsPreprocessing<CCGALConvexTetrahedralMeshGenerator> CGETranslucentObjectsPreprocessingConvexObjects;
	typedef TranslucentObjectsPreprocessing<CCGALMultiDomainTetrahedralMeshGenerator> CGETranslucentObjectPreprocessing;

	/************************************************************************/
	/* Solve diffusion equation to get radiant flux							*/
	/************************************************************************/

	static const std::string flux_computation_shader_vs_src(
		"#version 330\n"
		"layout(location = 0) in vec3 position;\n"
		"uniform mat4 mvp;\n"
		"void main() {\n"
		"gl_Position = mvp * vec4(position, 1.0f);\n"
		"}"
	);
	//TODO: uniform buffers for faster access
	static const std::string flux_computation_shader_fs_src(
		"#version 330\n"
		"layout(location = 0) out vec4 radiant_flux_value;\n"
		"uniform sampler2DRect node_positions;\n"
		"uniform sampler2DRect indices_ch1;\n"
		"uniform sampler2DRect indices_ch2;\n"
		"uniform sampler2DRect incoming_light;\n"
		"uniform sampler2DRect current_flux;\n"
		"uniform vec2 limits_inner_nodes;\n"
		"uniform vec2 limits_surface_nodes;\n"
		"uniform vec4 optical_properties;\n"
		"#define kappa	optical_properties.x\n"
		"#define mu		optical_properties.y\n"
		"#define A		optical_properties.z\n"
		"#define F_dr	optical_properties.w\n"
		"\n"
		"void main() {\n"
		"vec2 coord = gl_FragCoord.xy;\n"
		"vec3 node_position = texture(node_positions, coord).xyz;\n"
		"vec3 new_flux = vec3(0.0);\n"
		"vec4 out_buffer_value = vec4(0.0);\n"
		"if (coord.y < limits_surface_nodes.y) {\n"
		"	if ( (coord.y == (limits_surface_nodes.y - 1.0)) && (coord.x >= limits_surface_nodes.x)){discard;}\n"
		"	vec2 neigh_index = texture(indices_ch1, coord).xy;\n"
		"	float d_sk = distance(node_position, texture(node_positions, neigh_index).xyz);\n"
		"	vec3 q_ns = texture(incoming_light, coord).xyz;\n"
		"	float factor1 = 2.0 * A * kappa;\n"
		"	vec3 factor2 = d_sk * 4.0 * q_ns / (1 + F_dr);\n"
		"	new_flux = (factor1 * texture(current_flux, neigh_index).xyz + factor2) / (factor1 + d_sk);\n"
		"	out_buffer_value = vec4(new_flux, 0.0);\n"
		"}\n"
		"else{\n"
		"	if ( (coord.y == (limits_inner_nodes.y - 1.0)) && (coord.x >= limits_inner_nodes.x)){discard;}\n"
		"	vec2 neigh_indices[4];\n"
		"	vec3 neigh_positions[4];\n"
		"	float d_ij_sqrd[4]; /*squared distances to neighbor nodes*/\n"
		"	neigh_indices[0] = vec2(texture(indices_ch1, coord).xy);\n"
		"	neigh_indices[1] = vec2(texture(indices_ch1, coord).zw);\n"
		"	neigh_indices[2] = vec2(texture(indices_ch2, coord).xy);\n"
		"	neigh_indices[3] = vec2(texture(indices_ch2, coord).zw);\n"
		"	float inv_sum_dij_sqrd = 0.0;\n"
		"	for(int i = 0; i < 4; i++){\n"
		"		neigh_positions[i] = texture(node_positions, neigh_indices[i]).xyz;\n"
		"		d_ij_sqrd[i] = pow(distance(node_position, neigh_positions[i]), 2.0);\n"
		"		inv_sum_dij_sqrd += 1.0 / d_ij_sqrd[i];\n"
		"	}\n"
		"	/*Compute new flux phi for inner node*/\n"
		"	for(int i = 0; i < 4; i++){\n"
		"		new_flux += (kappa * texture(current_flux, neigh_indices[i]).xyz) / d_ij_sqrd[i];\n"
		"	}\n"
		"	new_flux /= (inv_sum_dij_sqrd * kappa + mu);\n"
		"	out_buffer_value = vec4(new_flux, 0.0);\n"
		"}\n"
		"radiant_flux_value = out_buffer_value;\n"
		"}"
	);

	static const std::string incoming_light_sampling_shader_vs_src(
		"#version 330\n"
		"layout(location = 0) in vec3 position;\n"
		"uniform mat4 mvp;\n"
		"void main() {\n"
		"gl_Position = mvp * vec4(position, 1.0f);\n"
		"}"
	);

	static const std::string incoming_light_sampling_shader_fs_src(
		"#version 330\n"
		"layout(location = 0) out vec4 incoming_light;\n"
		"uniform sampler2DRect irradiance;\n"
		"uniform sampler2DRectShadow light_depthmap;\n"
		"uniform sampler2DRect node_positions;\n"
		"uniform mat4 light_mvp;\n"
		"\n"
		"void main() {\n"
		"	vec2 coord = gl_FragCoord.xy;\n"
		"	ivec2 depthTexSize = textureSize(light_depthmap);\n"
		"	ivec2 irrTexSize = textureSize(irradiance);\n"
		"	vec4 node_position = texture(node_positions, coord);\n"
		"	vec4 node_position_light_clipspace = light_mvp * node_position;\n"
		"	vec4 node_position_light_ndc = node_position_light_clipspace / node_position_light_clipspace.w;\n"
		"	node_position_light_ndc = node_position_light_ndc * 0.5 + 0.5;\n"
		"	vec4 depth_access_coord = node_position_light_ndc;\n"
		"	vec4 light_access_coord = node_position_light_ndc;\n"
		"	depth_access_coord.xy = depth_access_coord.xy * vec2(depthTexSize);\n"
		"	light_access_coord.xy = light_access_coord.xy * vec2(irrTexSize);\n"
		"	float shadowFactor = textureProj(light_depthmap, depth_access_coord);\n"
		"	vec4 irr = texture(irradiance, light_access_coord.xy);\n"
		"	incoming_light = shadowFactor * irr;\n"
		"}"
	);

	/*static const std::string flux_computation_vs(
		"#version 330\n"
		"layout(location = 0) in vec3 position;\n"
		"uniform mat4 mvp;\n"
		"void main() {\n"
		"gl_Position = mvp * vec4(position, 1.0f);\n"
		"}"
	);

	static const std::string flux_computation_fs(
		"#version 330\n"
		"layout(location = 0) out vec4 fragment_value;\n"
		"//Uniform variables\n"
		"uniform sampler2DRect node_positions;\n"
		"uniform sampler2DRect indices_ch1;\n"
		"uniform sampler2DRect indices_ch2;\n"
		"uniform sampler2DRect current_flux;\n"
		"uniform sampler2DRect incoming_light;\n"
		"uniform vec2 limits_inner_nodes;\n"
		"uniform vec2 limits_surface_nodes;\n"
		"\n"
		"void main() {\n"
		"vec2 coord = gl_FragCoord.xy;\n"
		"if (coord.y < limits_surface_nodes.y) {\n"
		"	if ( (coord.y == (limits_inner_nodes.y - 1.0)) && (coord.x > limits_surface_nodes.x)){discard;}\n"
		"	float q_ns = texture(incoming_light, coord);\n"
		"	vec2 neigh_indices = texture(indices_ch1, coord).xy;\n"
		"	vec4 pos_neigh = texture(node_positions, neigh_indices);\n"
		"}\n"
		"else{\n"
		"}\n"
		"fragment_value = texture(indices, coord);\n"
		"}"
	);*/

	template<class PreprocessingStep>
	class DiffusionSolver
	{
	public:

		DiffusionSolver() : m_ViewportHeight(0), m_ViewportWidth(0), m_MVP_loc(0), m_ResultTexIndex(0),
			m_LightMVP_loc(0), m_IrradianceTex_loc(0), m_LightDepthmapTex_loc(0), m_MVP2_loc(0),
			m_NeighborIndicesTexCh1_loc(0), m_NeighborIndicesTexCh2_loc(0), m_PositionTex_loc(0),
			m_PositionTex2_loc(0), m_IOR_loc(0), m_Limits_ns_loc(0), m_Limits_ni_loc(0), m_IncomingLight_loc(0),
			m_ShadowSampler_loc(0), m_CurrentFlux_loc(0), m_OpticalProperties_loc(0)
		{ 
			m_ColorAttachments.push_back(GL_COLOR_ATTACHMENT0);
			m_ColorAttachments.push_back(GL_COLOR_ATTACHMENT1);
			m_RadiantFluxTex_rect.push_back(CGEngine::CGERectangleTexture());
			m_RadiantFluxTex_rect.push_back(CGEngine::CGERectangleTexture());
			//TODO: initialize members, throw wen not enough texture units
		
			//test optical properties (for jade)
			float eta = 1.667f; //IOR
			float F_dr = -1.44f / (eta*eta) + 0.71f / eta + 0.668f + 0.0636f*eta; //internal diffuse surface reflectance
			m_HomogenousOpticalProperties.x = 1.0f / (0.00389f + 0.781f * (1.0f)); //kappa; g = 0; formula: sig_reduced = sig(1 - g)
			m_HomogenousOpticalProperties.y = 0.00389f / 3.0f; //absorption coefficient
			m_HomogenousOpticalProperties.z = (1 + F_dr) / (1 - F_dr);
			m_HomogenousOpticalProperties.w = F_dr;
			m_IOR = eta;
		}
		~DiffusionSolver() { m_ColorAttachments.clear(); }

		//Setup buffers on GPU and transfer data
		bool SetupBuffers(const PreprocessingStep& _pp)
		{
			try
			{
				CGEngine::iVec2 in_max(0), sn_max(0), tex_dim(0);
				_pp.GetMaxNodeTexCoords(sn_max, in_max, tex_dim);
				GLsizei w = (GLsizei)tex_dim.s;
				GLsizei h = (GLsizei)tex_dim.t;
				m_ViewportWidth = w; m_ViewportHeight = h;
				m_SurfaceNodeMaxTexCoords = Vec2(static_cast<float>(sn_max.x)+0.5f, static_cast<float>(sn_max.y)+0.5f);
				m_InnerNodesMaxTexCoords = Vec2(static_cast<float>(in_max.x)+0.5f, static_cast<float>(in_max.y)+0.5f);

				Vec4DVec positions;
				Vec4DVec neighbor_indices_ch1;
				Vec4DVec neighbor_indices_ch2;
				positions.resize(w*h, Vec4(0.0f));
				neighbor_indices_ch1.resize(w*h, Vec4(0));
				neighbor_indices_ch2.resize(w*h, Vec4(0));

				//setup node positions and neighbor relations
				auto vertex_it = boost::vertices(_pp.GetQuadGraph());
				for (auto v_it = vertex_it.first; v_it != vertex_it.second; ++v_it)
				{
					//texture coordinates
					std::size_t u = static_cast<std::size_t>(_pp.GetNodeTexCoord(*v_it).s);
					std::size_t v = static_cast<std::size_t>(_pp.GetNodeTexCoord(*v_it).t);
					//position
					const Vec3& pos = _pp.GetNodePosition(*v_it);
					positions[v*w + u] = Vec4(pos.x, pos.y, pos.z, 1.0f);
					//neighbors
					auto adjacent_vertices_it = boost::adjacent_vertices(*v_it, _pp.GetQuadGraph());
					int ch_index(0);
					for (auto vv_it = adjacent_vertices_it.first; vv_it != adjacent_vertices_it.second; ++vv_it, ++ch_index)
					{
						//get texture coordinate of neighbor node and adjust to get OpenGL texture space coordinate
						const iVec2& adj_tex_coord = _pp.GetNodeTexCoord(*vv_it);
						switch (ch_index)
						{
						case 0:
							neighbor_indices_ch1[v*w + u].x = static_cast<float>(adj_tex_coord.x) + 0.5f;
							neighbor_indices_ch1[v*w + u].y = static_cast<float>(adj_tex_coord.y) + 0.5f;
							break;
						case 1:
							neighbor_indices_ch1[v*w + u].z = static_cast<float>(adj_tex_coord.x) + 0.5f;
							neighbor_indices_ch1[v*w + u].w = static_cast<float>(adj_tex_coord.y) + 0.5f;
							break;
						case 2:
							neighbor_indices_ch2[v*w + u].x = static_cast<float>(adj_tex_coord.x) + 0.5f;
							neighbor_indices_ch2[v*w + u].y = static_cast<float>(adj_tex_coord.y) + 0.5f;
							break;
						case 3:
							neighbor_indices_ch2[v*w + u].z = static_cast<float>(adj_tex_coord.x) + 0.5f;
							neighbor_indices_ch2[v*w + u].w = static_cast<float>(adj_tex_coord.y) + 0.5f;
							break;
						default:
							//Shouldn't get here
							throw std::string("Error: QuadGraph node with more than 4 neighbors encountered!");
							break;
						}
					}
				}

				m_PositionsTex_rect.VLoadTexture(w, h, GL_RGBA16F, GL_RGBA, GL_FLOAT, (GLvoid*)(&positions[0]));
				m_NeighborIndicesTexCh1_rect.VLoadTexture(w, h, GL_RGBA16F, GL_RGBA, GL_FLOAT, (GLvoid*)(&neighbor_indices_ch1[0]));
				m_NeighborIndicesTexCh2_rect.VLoadTexture(w, h, GL_RGBA16F, GL_RGBA, GL_FLOAT, (GLvoid*)(&neighbor_indices_ch2[0]));

				//testing
				/*{
					std::vector<Vec4> ni(w*h, Vec4(0));
					m_NeighborIndicesTexCh1_rect.Bind(GL_TEXTURE0);
					glGetTexImage(GL_TEXTURE_RECTANGLE, 0, GL_RGBA, GL_FLOAT, (GLvoid*)(&ni[0]));
					if (glGetError() != GL_NO_ERROR)
						return false;
					for (std::size_t ii=0; ii < (std::size_t)(h*w); ++ii)
					{
						assert(ni[ii] == neighbor_indices_ch1[ii]);
					}
				}*/
				
				//Flux computation setup
				m_FullscreenQuad.MakeFullscreenQuad(static_cast<float>(w), static_cast<float>(h));
				m_RadiantFluxTex_rect[0].MakeEmptyTexture(w, h, GL_RGBA16F, GL_RGBA, GL_FLOAT);
				m_RadiantFluxTex_rect[1].MakeEmptyTexture(w, h, GL_RGBA16F, GL_RGBA, GL_FLOAT);
				m_RadiantFluxDepthTex_rect.MakeEmptyTexture(w, h, GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT, GL_FLOAT);
				m_DiffusionSolverFBO.Initialize();
				m_DiffusionSolverFBO.AttachTexture2D(&m_RadiantFluxTex_rect[0],	GL_FRAMEBUFFER,	m_ColorAttachments[0]);
				m_DiffusionSolverFBO.AttachTexture2D(&m_RadiantFluxTex_rect[1],	GL_FRAMEBUFFER,	m_ColorAttachments[1]);
				m_DiffusionSolverFBO.AttachTexture2D(&m_RadiantFluxDepthTex_rect, GL_DRAW_FRAMEBUFFER,GL_DEPTH_ATTACHMENT);
				CGEngine::CGEFramebufferObject::Unbind();

				m_FluxComputationShader.VertexShaderFromSource(flux_computation_shader_vs_src.c_str());
				m_FluxComputationShader.FragmentShaderFromSource(flux_computation_shader_fs_src.c_str());
				m_FluxComputationShader.LinkProgram();

				m_MVP_loc					= m_FluxComputationShader.GetUniformVarLocation("mvp");
				m_NeighborIndicesTexCh1_loc = m_FluxComputationShader.GetUniformVarLocation("indices_ch1");
				m_NeighborIndicesTexCh2_loc = m_FluxComputationShader.GetUniformVarLocation("indices_ch2");
				m_PositionTex_loc			= m_FluxComputationShader.GetUniformVarLocation("node_positions");
				m_Limits_ni_loc				= m_FluxComputationShader.GetUniformVarLocation("limits_inner_nodes");
				m_Limits_ns_loc				= m_FluxComputationShader.GetUniformVarLocation("limits_surface_nodes");
				m_IncomingLight_loc			= m_FluxComputationShader.GetUniformVarLocation("incoming_light");
				m_ShadowSampler_loc			= m_FluxComputationShader.GetUniformVarLocation("light_depthmap");
				m_CurrentFlux_loc			= m_FluxComputationShader.GetUniformVarLocation("current_flux");
				m_OpticalProperties_loc		= m_FluxComputationShader.GetUniformVarLocation("optical_properties");

				//Sampling incoming light setup
				m_FullscreenQuadSurfaceNodes.MakeFullscreenQuad(static_cast<float>(tex_dim.s), static_cast<float>(sn_max.t));
				m_IncomingLight_rect.MakeEmptyTexture(w, static_cast<GLsizei>(sn_max.y), GL_RGBA16F, GL_RGBA, GL_FLOAT);
				m_IncomingLightDepth_rb.RenderbufferStorage(GL_DEPTH_COMPONENT24, w, static_cast<GLsizei>(sn_max.y));
				m_LightFBO.Initialize();
				m_LightFBO.AttachTexture2D(&m_IncomingLight_rect, GL_FRAMEBUFFER);
				m_LightFBO.AttachRenderBuffer(GL_DRAW_FRAMEBUFFER, m_IncomingLightDepth_rb, GL_DEPTH_ATTACHMENT);
				CGEngine::CGEFramebufferObject::Unbind();

				m_IncomingLightSamplingShader.VertexShaderFromSource(incoming_light_sampling_shader_vs_src.c_str());
				m_IncomingLightSamplingShader.FragmentShaderFromSource(incoming_light_sampling_shader_fs_src.c_str());
				m_IncomingLightSamplingShader.LinkProgram();

				m_MVP2_loc					= m_IncomingLightSamplingShader.GetUniformVarLocation("mvp");
				m_LightMVP_loc				= m_IncomingLightSamplingShader.GetUniformVarLocation("light_mvp");
				m_IrradianceTex_loc			= m_IncomingLightSamplingShader.GetUniformVarLocation("irradiance");
				m_LightDepthmapTex_loc		= m_IncomingLightSamplingShader.GetUniformVarLocation("light_depthmap");
				m_PositionTex2_loc			= m_IncomingLightSamplingShader.GetUniformVarLocation("node_positions");
			}
			catch (std::string& es)
			{
				std::cerr << "DiffusionSolver setup error:\n";
				std::cerr << es << std::endl;
				return false;
			}
			catch (ShaderError& sh_err)
			{
				std::cerr << "DiffusionSolver setup error:\n";
				std::cerr << sh_err.what() << std::endl;
				return false;
			}
			catch (...)
			{
				std::cerr << "DiffusionSolver setup error: Caught unknown exception!" << std::endl;
				return false;
			}
			return true;
		}

		void ComputeIncomingLightOnSurfaceNodes(
			const CGEngine::CGERectangleTexture& _lightShadowSampler,
			const CGEngine::CGERectangleTexture& _lightIntensitySampler,
			const CGEngine::Matrix4x4& _lightMVPMatrix)
		{
			GLsizei w = static_cast<GLsizei>(m_SurfaceNodeMaxTexCoords.x);
			GLsizei h = static_cast<GLsizei>(m_SurfaceNodeMaxTexCoords.y - 0.5f);
			Matrix4x4 projection = glm::ortho(0.0f, static_cast<float>(w), 0.0f, static_cast<float>(h));

			m_LightFBO.BindBufferForWriting();
			glDrawBuffers(1, &m_ColorAttachments[0]);
			std::vector<GLfloat> clear_color(4, 10.0f);
			glClearBufferfv(GL_COLOR, 0, &clear_color[0]);
			glClearDepth(1.0f);
			glClear(GL_DEPTH_BUFFER_BIT);
			glViewport(0, 0, w, h);

			m_PositionsTex_rect.Bind(GL_TEXTURE0);
			_lightIntensitySampler.Bind(GL_TEXTURE1);
			_lightShadowSampler.Bind(GL_TEXTURE2);

			m_IncomingLightSamplingShader.StartProgram();

			glUniformMatrix4fv(m_MVP2_loc, 1, GL_FALSE, glm::value_ptr(projection));
			glUniformMatrix4fv(m_LightMVP_loc, 1, GL_FALSE, glm::value_ptr(_lightMVPMatrix));
			glUniform1i(m_PositionTex2_loc, 0);
			glUniform1i(m_IrradianceTex_loc, 1);
			glUniform1i(m_LightDepthmapTex_loc, 2);

			m_FullscreenQuadSurfaceNodes.VOnDraw();

			m_IncomingLightSamplingShader.StopProgram();

			CGEngine::CGEFramebufferObject::Unbind();

			/*Vec4DVec result_1(w*h, Vec4(0));

			m_LightFBO.BindBufferForReading();
			glReadBuffer(GL_COLOR_ATTACHMENT0);
			glReadPixels(0, 0, w, h, GL_RGBA, GL_FLOAT, &result_1[0]);
			if (glGetError() != GL_NO_ERROR)
				return;
			std::cout << "Incoming light computation result:\n";
			for (int j = 0; j < h; ++j)
			{
				for (int i = 0; i < w; ++i)
				{
					std::cout << std::setprecision(3) << result_1[j*w + i] << "\t";
				}
				std::cout << std::endl;
			}*/
			
		}

		void Solve(
			int _num_iterations = 10)
		{
			Matrix4x4 projection = glm::ortho(0.0f, static_cast<float>(m_ViewportWidth), 0.0f, static_cast<float>(m_ViewportHeight));
			int CurrActiveBuffer = 0;
			int src = CurrActiveBuffer;
			int dest = 1 - CurrActiveBuffer;

			m_DiffusionSolverFBO.BindBufferForWriting();
			std::vector<GLfloat> clear_color(4, 0);
			glClearBufferfv(GL_COLOR, 0, &clear_color[0]);
			glClearBufferfv(GL_COLOR, 1, &clear_color[0]);
			glClearDepth(1.0f);
			glClear(GL_DEPTH_BUFFER_BIT);
			glViewport(0, 0, m_ViewportWidth, m_ViewportHeight);

			m_PositionsTex_rect.Bind(GL_TEXTURE0);
			m_NeighborIndicesTexCh1_rect.Bind(GL_TEXTURE1);
			m_NeighborIndicesTexCh2_rect.Bind(GL_TEXTURE2);
			m_IncomingLight_rect.Bind(GL_TEXTURE3);

			m_FluxComputationShader.StartProgram();
			glUniformMatrix4fv(m_MVP_loc, 1, GL_FALSE, glm::value_ptr(projection));
			glUniform1i(m_PositionTex_loc, 0);
			glUniform1i(m_NeighborIndicesTexCh1_loc, 1);
			glUniform1i(m_NeighborIndicesTexCh2_loc, 2);
			glUniform1i(m_IncomingLight_loc, 3);
			glUniform2fv(m_Limits_ni_loc, 1, glm::value_ptr(m_InnerNodesMaxTexCoords));
			glUniform2fv(m_Limits_ns_loc, 1, glm::value_ptr(m_SurfaceNodeMaxTexCoords));
			glUniform4fv(m_OpticalProperties_loc, 1, glm::value_ptr(m_HomogenousOpticalProperties));

			for (int i = 0; i < _num_iterations; ++i)
			{
				m_ResultTexIndex = dest;
				glDrawBuffer(m_ColorAttachments[dest]);
				glClear(GL_DEPTH_BUFFER_BIT); //Important or else depth test always fails in subsequent render calls (disable depth test?)
				m_RadiantFluxTex_rect[src].Bind(GL_TEXTURE4);
				glUniform1i(m_CurrentFlux_loc, 4);
				
				m_FullscreenQuad.VOnDraw();

				/*{
					std::string path_ = boost::filesystem::current_path().string();
					std::string filename = std::string("\\diffsolve_iteration_") + std::string(std::to_string(i)) + std::string(".ppm");
					m_DiffusionSolverFBO.BindBufferForReading();
					glReadBuffer(m_ColorAttachments[dest]);
					CGEngine::Functionality::ReadBufferFromGPUToFile(path_ + filename, m_ViewportWidth, m_ViewportHeight, 4, 100.0f);
					m_DiffusionSolverFBO.BindBufferForWriting();
				}*/

				src = (++src) % 2;
				dest = (++dest) % 2;
			}
			m_FluxComputationShader.StopProgram();
			CGEngine::CGEFramebufferObject::Unbind();

			/*std::cout << "Diffusion solver result:\n";
			Vec4DVec result_1(m_ViewportWidth*m_ViewportHeight, Vec4(0));
			Vec4DVec result_2(m_ViewportWidth*m_ViewportHeight, Vec4(0));
			m_DiffusionSolverFBO.BindBufferForReading();
			glReadBuffer(m_ColorAttachments[dest]);
			glReadPixels(0, 0, m_ViewportWidth, m_ViewportHeight, GL_RGBA, GL_FLOAT, &result_1[0]);
			if (glGetError() != GL_NO_ERROR)
				std::cout<< "Error reading pixels!\n";
			glReadBuffer(m_ColorAttachments[src]);
			glReadPixels(0, 0, m_ViewportWidth, m_ViewportHeight, GL_RGBA, GL_FLOAT, &result_2[0]);
			if (glGetError() != GL_NO_ERROR)
				return;
			std::cout << "Source buffer:\n";
			for (int h = 0; h < m_ViewportHeight; ++h)
			{
				for (int w = 0; w < m_ViewportWidth; ++w)
				{
					std::cout << std::setprecision(3) << result_1[h*m_ViewportWidth + w] << "\t";
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
			std::cout << "Destination buffer:\n";
			for (int h = 0; h < m_ViewportHeight; ++h)
			{
				for (int w = 0; w < m_ViewportWidth; ++w)
				{
					std::cout << std::setprecision(3) << result_2[h*m_ViewportWidth + w] << "\t";
				}
				std::cout << std::endl;
			}
			CGEngine::CGEFramebufferObject::Unbind();*/
		}

		void ComputeOutgoingLightOnSurfaceNodes()
		{
			throw std::string(BOOST_CURRENT_FUNCTION + std::string(": Not implemented!"));
		}

		const CGEngine::CGERectangleTexture& GetRadiantFluxTexture() const
		{
			return m_RadiantFluxTex_rect[m_ResultTexIndex];
		}

	protected:
		CGEngine::CGERectangleTexture m_PositionsTex_rect				;
		CGEngine::CGERectangleTexture m_NeighborIndicesTexCh1_rect		; //RGBA values encode 2 vec2 texture coordinates for neighboring nodes
		CGEngine::CGERectangleTexture m_NeighborIndicesTexCh2_rect		;
		std::vector<CGEngine::CGERectangleTexture> m_RadiantFluxTex_rect;
		CGEngine::CGERectangleTexture m_RadiantFluxDepthTex_rect		; //it is advised to attach depth texture
		CGEngine::CGERectangleTexture m_IncomingLight_rect				;
		CGEngine::CGERectangleTexture m_OutgoingLight_rect				;
		CGEngine::CGERenderbufferObject m_IncomingLightDepth_rb			;

		CGEngine::CGEFramebufferObject m_DiffusionSolverFBO;
		CGEngine::CGEFramebufferObject m_LightFBO;

		CGEngine::CFullScreenQuad m_FullscreenQuad;
		CGEngine::CFullScreenQuad m_FullscreenQuadSurfaceNodes;

		Vec4 m_HomogenousOpticalProperties;
		Vec2 m_SurfaceNodeMaxTexCoords;
		Vec2 m_InnerNodesMaxTexCoords;
		GLsizei m_ViewportWidth, m_ViewportHeight;
		std::vector<GLenum> m_ColorAttachments;
		GLint m_ResultTexIndex;

		CGEngine::ShaderProgram m_FluxComputationShader;
		CGEngine::ShaderProgram m_IncomingLightSamplingShader;
		//uniform locations
		GLint m_MVP_loc;
		GLint m_LightMVP_loc;
		GLint m_IrradianceTex_loc;
		GLint m_LightDepthmapTex_loc;
		GLint m_MVP2_loc;
		GLint m_NeighborIndicesTexCh1_loc;
		GLint m_NeighborIndicesTexCh2_loc;
		GLint m_PositionTex_loc;
		GLint m_PositionTex2_loc;
		GLint m_IOR_loc;
		GLint m_Limits_ns_loc, m_Limits_ni_loc;
		GLint m_IncomingLight_loc;
		GLint m_ShadowSampler_loc;
		GLint m_CurrentFlux_loc;
		GLint m_OpticalProperties_loc;
		GLfloat m_IOR;
	private:
	};

	typedef DiffusionSolver<CGETranslucentObjectsPreprocessingConvexObjects> DiffusionSolverConvexObjects;

} //End namespace CGEngine