#ifndef bounding_sphere_h__
#define bounding_sphere_h__

#include "CGEGeometry.h"
#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>
#include <boost/foreach.hpp>
#include <random>
#include <glm/gtx/norm.hpp>

typedef CGEngine::Vec3			Vec3	;
typedef CGEngine::Vec3DVec		Vec3DVec;

//simple class for bounding spheres
class BoundingSphere
{
public:
	float		m_Radius	;
	Vec3		m_Center	;

	BoundingSphere()
	{
		m_Radius = 0.0f;
		m_Center = Vec3(0.0f);
	}

	BoundingSphere(const BoundingSphere& _boundingSphere) :
		m_Radius(_boundingSphere.m_Radius), m_Center(_boundingSphere.m_Center)
	{
	}

	BoundingSphere(const Vec3& _center, float _radius)
	{
		m_Radius = _radius;
		m_Center = _center;
	}

	BoundingSphere& operator=(const BoundingSphere& _rhs)
	{
		m_Radius = _rhs.m_Radius;
		m_Center = _rhs.m_Center;
		return *this;
	}
};

typedef boost::optional<BoundingSphere> OptBoundingSphere	;

//////////////////////////////////////////////////////////////////////////
// Compute a bounding sphere using Ritter's algorithm
// Method:    ComputeBoundingSphereRitter
// FullName:  ComputeBoundingSphereRitter
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: const Vec3DVec & _points
// Parameter: BoundingSphere & _boundingSphere
//************************************
void ComputeBoundingSphereRitter(const Vec3DVec& _points, BoundingSphere& _boundingSphere);


#endif // bounding_sphere_h__
