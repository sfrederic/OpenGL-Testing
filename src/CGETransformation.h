/*********************************************************************************

  Geometrical Transformations

*********************************************************************************/

#ifndef CGETRANSFORMATION_H
#define CGETRANSFORMATION_H

#ifndef __GLEW_H__
#define GLEW_STATIC
#include <GL/glew.h>
#endif
#include <boost/shared_ptr.hpp>
#include <list>
#include "CGEGeometry.h"

namespace CGEngine {

//Class Header and Typedefs
class CMatrixStack ;
typedef boost::shared_ptr<CMatrixStack> MatrixStackPointer  ;
typedef std::list< Matrix4x4 >          MatrixStack         ;


class CMatrixStack
{
protected:
    MatrixStack     m_Stack             ;
	Matrix3x3		m_InvTransposeTemp	;

private:
    CMatrixStack(const CMatrixStack&) {}
public:
    CMatrixStack() ;
    virtual ~CMatrixStack();

    //Translation
    void Translate(const Vec3& _TranslationVector)              ;
    //Rotation
    void Rotate(const float _Angle, const Vec3& _RotationAxis)  ;
    //Scaling
    void Scale(const Vec3& _ScalingFactors)                     ;
    void UniformScale(const float _ScalingFactor)               ;
	//Multiply top of matrix stack with given matrix
	void MultMatrix(const Matrix4x4& _mat)						;
	void MultMatrix(Matrix4x4 const * _mat)						;
    //Load Identity-Matrix to top position of the Stack
    void LoadIdentity()                                         ;
    //Push Stack (identical to glPushMatrix)
    void PushMatrix()                                           ;
    //Pop Stack (identical to glPopMatrix)
    void PopMatrix()                                            ;
    //Get the current transformation matrix (on top of the stack)
    const Matrix4x4& getCurrentTransform() const                ;
    //Get Pointer to current transformation-matrix (on top of the stack)
    const float* getCurrentTransformPointer() const             ;

	Matrix3x3 getCurrentNormalMatrix()							;
	const float* getCurrentNormalMatrixPtr()					;
    //(Re-)initializes the stack
    void EmptyStack()                                           ;
	//Actual depth of the stack
	std::size_t StackDepth() const								;
    //Compute Normal matrix from given Model matrix and View matrix
    static void ComputeNormalMatrix(Matrix3x3& _NormalMatrix,
                                    const Matrix4x4& _Model = Matrix4x4(1.0),
                                    const Matrix4x4& _View = Matrix4x4(1.0));
	//Compute Normal matrix from given matrix
	static void ComputeNormalMatrix(Matrix3x3& _NormalMatrix,
									const Matrix4x4& _matrix = Matrix4x4(1.0));
	static void ComputeNormalMatrix(Matrix3x3& _NormalMatrix, 
									const Matrix4x4* _Model = NULL, 
									const Matrix4x4* _View = NULL);
};

}   //End Namespace

#endif // CGETRANSFORMATION_H
