﻿#pragma once

#ifndef __GLEW_H__
#define GLEW_STATIC
#include <GL/glew.h>
#endif

#include <vector>
#include <boost/current_function.hpp>

#include "functionality.h"
#include "CGETextureBase.h"


namespace CGEngine
{
	//Class headers and typedefs
	class CGEFramebufferObject;
	class CGERenderbufferObject;

	//print information about frame buffer errors
	void print_framebuffer_status_code(GLenum _fbo_status)
	{
		std::cerr << "glCheckFramebufferStatus returned an error. Code: ";
		switch (_fbo_status)
		{
		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			std::cerr << GL_ENUM_TO_STR(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT) << std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			std::cerr << GL_ENUM_TO_STR(GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT) << std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
			std::cerr << GL_ENUM_TO_STR(GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER) << std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
			std::cerr << GL_ENUM_TO_STR(GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER) << std::endl;
			break;
		case GL_FRAMEBUFFER_UNSUPPORTED:
			std::cerr << GL_ENUM_TO_STR(GL_FRAMEBUFFER_UNSUPPORTED) << std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
			std::cerr << GL_ENUM_TO_STR(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE) << std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
			std::cerr << GL_ENUM_TO_STR(GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS) << std::endl;
			break;
		case GL_OUT_OF_MEMORY:
			std::cerr << GL_ENUM_TO_STR(GL_OUT_OF_MEMORY) << std::endl;
			break;
		default:
			std::cerr << "Unknown" << std::endl;
			break;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//Implementation
	//////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////// 
	//Renderbuffer Object
	//////////////////////////////////////////////////////////////////////////
	
	class CGERenderbufferObject
	{
	public:
		CGERenderbufferObject() : m_RBO(0)
		{
			glGenRenderbuffers(1, &m_RBO);
		}

		CGERenderbufferObject(CGERenderbufferObject&& _rhs)
		{
			m_RBO = _rhs.m_RBO;
			_rhs.m_RBO = 0;
		}

		~CGERenderbufferObject()
		{
			Clear();
		}

		inline GLuint GetRBOName() const
		{
			return m_RBO;
		}

		inline void Clear()
		{
			if (m_RBO != 0)
				glDeleteRenderbuffers(1, &m_RBO);
		}

		inline void Bind() const
		{
			if (m_RBO != 0)
				glBindRenderbuffer(GL_RENDERBUFFER, m_RBO);
		}

		static void Unbind()
		{
			glBindRenderbuffer(GL_RENDERBUFFER, 0);
		}

		inline void RenderbufferStorage(GLenum _internalformat​, GLsizei _width​, GLsizei _height)
		{
			Bind();
			glRenderbufferStorage(GL_RENDERBUFFER, _internalformat​, _width​, _height);
			if (glGetError() != GL_NO_ERROR)
			{
				print_framebuffer_status_code(glGetError());
				throw;
			}
		}

		inline void RenderbufferStorageMultisample(GLsizei _samples, GLenum _internalformat, GLsizei _width, GLsizei _height)
		{
			Bind();
			glRenderbufferStorageMultisample(GL_RENDERBUFFER, _samples, _internalformat, _width, _height);
			if (glGetError() != GL_NO_ERROR)
			{
				print_framebuffer_status_code(glGetError());
				throw;
			}
		}

		CGERenderbufferObject& operator= (CGERenderbufferObject&& _rhs)
		{
			if (this != &_rhs)
			{
				Clear();
				std::swap(m_RBO, _rhs.m_RBO);
			}
			return *this;
		}

	private:
		GLuint m_RBO;

		CGERenderbufferObject(const CGERenderbufferObject&) {}
		CGERenderbufferObject& operator=(const CGERenderbufferObject&) {}
	};

	//////////////////////////////////////////////////////////////////////////
	//Framebuffer Object
	//////////////////////////////////////////////////////////////////////////

	class CGEFramebufferObject
	{
	public:
		CGEFramebufferObject() : m_is_valid(false), m_FBO(0)
		{
		}

		CGEFramebufferObject(bool _make_fbo) : m_is_valid(false), m_FBO(0)
		{
			glGenFramebuffers(1, &m_FBO);
			if ((glGetError() == GL_NO_ERROR) && GLEW_VERSION_4_3) //Empty frame buffers are valid since core version 4.3
				validate();
		}

		CGEFramebufferObject(CGEFramebufferObject&& _rhs)
		{
			m_FBO = _rhs.m_FBO;
			m_is_valid = _rhs.m_is_valid;
			_rhs.m_FBO = 0;
			_rhs.invalidate();
		}

		~CGEFramebufferObject()
		{
			Clear();
		}

		inline void Clear()
		{
			if (m_FBO != 0)
			{
				glDeleteFramebuffers(1, &m_FBO);
				m_FBO = 0;
				invalidate();
			}	
		}

		static void Unbind()
		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}

		inline GLuint GetFBOName() const
		{
			return m_FBO;
		}

		inline void Initialize(GLsizei _num_buffers = 1)
		{
			if (m_FBO != 0)
				Clear();
			glGenFramebuffers(_num_buffers, &m_FBO);
			if ((glGetError() == GL_NO_ERROR) && GLEW_VERSION_4_3) //Empty frame buffers are valid since core version 4.3
				validate();
		}

		inline void BindBuffer(GLenum _target = GL_FRAMEBUFFER) const
		{
			if (m_is_valid)
			{
				glBindFramebuffer(_target, m_FBO);
			}
			else
				throw;
		}

		inline void BindBufferForWriting() const
		{
			if (m_is_valid)
			{
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO);
			}
			else
				throw;
		}

		inline void BindBufferForReading() const
		{
			if (m_is_valid)
			{
				glBindFramebuffer(GL_READ_FRAMEBUFFER, m_FBO);
			}
			else
				throw;
		}

		inline bool SetFramebufferParameter(GLenum _fbo_target, GLenum _pname, GLint _param)
		{
			if (GLEW_ARB_framebuffer_no_attachments)
			{
				glBindFramebuffer(_fbo_target, m_FBO);
				glFramebufferParameteri(_fbo_target, _pname, _param);
				if (glGetError() != GL_NO_ERROR)
				{
					CGEngine::Functionality::cout_gl_error(glGetError());
					invalidate();
					return false;
				} 
				else
				{
					validate();
					return true;
				}
			} 
			else
			{
				throw std::string("ARB_framebuffer_no_attachments not supported!");
			}
		}

		inline void GetFramebufferParameter(GLenum _fbo_target​, GLenum _pname​, GLint* _params) const
		{
			if (GLEW_ARB_framebuffer_no_attachments)
			{
				glBindFramebuffer(_fbo_target​, m_FBO);
				glGetFramebufferParameteriv(_fbo_target​, _pname​, _params);
				if (glGetError() != GL_NO_ERROR)
					CGEngine::Functionality::cout_gl_error(glGetError());
			}
			else
			{
				throw std::string("ARB_framebuffer_no_attachments not supported!");
			}
		}

		inline bool AttachTexture1D()
		{
			return false;
		}

		inline bool AttachTexture2D(CGETextureBase* _texture, GLenum _fbo_target = GL_DRAW_FRAMEBUFFER, 
			GLenum _attachment_point = GL_COLOR_ATTACHMENT0, GLint _level = 0)
		{
			if (!_texture || (_level < 0))
			{
				return false;
			} 
			else
			{
				glBindFramebuffer(_fbo_target, m_FBO);
				glFramebufferTexture2D(_fbo_target, _attachment_point, _texture->VGetTextureTarget(),
					_texture->GetTextureId(), _level);

				if (glCheckFramebufferStatus(_fbo_target) != GL_FRAMEBUFFER_COMPLETE)
				{
					invalidate();
					print_framebuffer_status_code(glCheckFramebufferStatus(_fbo_target));
					return false;
				}
				else
				{
					validate();
					return true;
				}
			}
		}

		inline bool AttachCubemap(CGETextureBase* _texture, GLenum _textarget = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
			GLenum _fbo_target = GL_DRAW_FRAMEBUFFER, GLenum _attachment_point = GL_COLOR_ATTACHMENT0, GLint _level = 0)
		{
			throw (std::string(BOOST_CURRENT_FUNCTION) + std::string("Not yet implemented!"));
		}

		inline bool AttachTextureLayer()
		{
			return false;
		}

		inline bool AttachRenderBuffer(GLenum _fbo_target, const CGERenderbufferObject& _rbo, GLenum _attachment_point)
		{
			glBindFramebuffer(_fbo_target, m_FBO);
			glFramebufferRenderbuffer(_fbo_target, _attachment_point, GL_RENDERBUFFER, _rbo.GetRBOName());
			if (glCheckFramebufferStatus(_fbo_target) != GL_FRAMEBUFFER_COMPLETE)
			{
				invalidate();
				print_framebuffer_status_code(glCheckFramebufferStatus(_fbo_target));
				return false;
			}
			else
			{
				validate();
				return true;
			}
		}

		CGEFramebufferObject& operator=(CGEFramebufferObject&& _rhs)
		{
			if (this != &_rhs)
			{
				Clear();
				std::swap(m_FBO, _rhs.m_FBO);
				std::swap(m_is_valid, _rhs.m_is_valid);
			}
			return *this;
		}

	protected:
		GLuint	m_FBO;
		bool m_is_valid;

		inline void validate() { m_is_valid = true; }

		inline void invalidate() { m_is_valid = false; }

		CGEFramebufferObject(const CGEFramebufferObject&) {}
		CGEFramebufferObject& operator=(const CGEFramebufferObject&) {}
	};
}
