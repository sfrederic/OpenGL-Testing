#pragma once
#include "CGETextureBase.h"

namespace CGEngine
{
	class CGERectangleTexture : public CGETextureBase
	{
	protected:
		int m_Width;
		int m_Height;

		CGERectangleTexture(const CGERectangleTexture&) {}
	public:
		CGERectangleTexture() : CGETextureBase() {
			m_Width = 0;
			m_Height = 0;
		}

		CGERectangleTexture(CGERectangleTexture&& _rhs) : CGETextureBase(), m_Width(_rhs.m_Width), m_Height(_rhs.m_Height)
		{
			m_TextureID = _rhs.m_TextureID;

			_rhs.m_TextureID = 0;
			_rhs.m_Width = 0;
			_rhs.m_Height = 0;
		}

		~CGERectangleTexture()
		{
			VReleaseTexture();
		}

		CGERectangleTexture& operator=(CGERectangleTexture&& _rhs)
		{
			if (this != &_rhs)
			{
				VReleaseTexture();
				std::swap(m_TextureID, _rhs.m_TextureID);
				std::swap(m_Width, _rhs.m_Width);
				std::swap(m_Height, _rhs.m_Height);
			}
		}
		
		inline void Bind(const GLenum _ActiveTextureUnit) const
		{
			glActiveTexture(_ActiveTextureUnit);
			glBindTexture(GL_TEXTURE_RECTANGLE, m_TextureID);
			if (glGetError() != GL_NO_ERROR)
				return;
		}

		virtual bool VLoadTexture(const std::string& _fileName, GLenum _pixel_store_param, GLint _pixel_store_value) override;

		bool MakeEmptyTexture(GLsizei _width, GLsizei _height, GLenum _internal_format, GLenum _tex_format, GLenum _type);

		virtual GLenum VGetTextureTarget() override;

		virtual bool VLoadTexture(GLsizei _width, GLsizei _height, GLenum _internal_format, GLenum _tex_format, GLenum _type, GLvoid* _data) override;

	private:
	};
} //End namespace