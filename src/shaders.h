/**************************************************************************************************
*
*	Loading and compiling shaders
*
**************************************************************************************************/

#ifndef __GLEW_H__
#define GLEW_STATIC
#include <gl\glew.h>
#endif

#include <boost/shared_ptr.hpp>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string>
#include <list>

#ifndef SHADERS_H
#define SHADERS_H

namespace CGEngine
{
	//////////////////////////////////////////////////////////////////////////
	#define	SHADER_DEBUG	1
	#define TYPLEN			10
	#define NAMLEN			20
	#define VALLEN			100

	class ShaderProgram;
	typedef boost::shared_ptr<ShaderProgram> ShaderProgramPtr;
	
	typedef struct{
		char	name[NAMLEN]	;
		char	type[TYPLEN]	;
		char	value[VALLEN]	;
		GLint	location		;	
	} UniformVariable;

	typedef std::list<UniformVariable>	UniformList	;

	//////////////////////////////////////////////////////////////////////////

	class ShaderError
	{
	public:
		ShaderError(const std::string& _msg) : m_msg(_msg) {}
		virtual std::string what() {return m_msg;}
	protected:
		std::string m_msg;
	};

	//////////////////////////////////////////////////////////////////////////
	
	class ShaderProgram 
	{
	protected:
		GLuint		m_VertexShaderID	;		
		GLuint		m_FragmentShaderID	;
		GLuint		m_GeometryShaderID	;
		GLuint		m_Program			;
	
		UniformList	m_Uniformlist		;
		
		unsigned long getFileLength(FILE* pFile)			;
		//Load Shader source
		char* loadshader(const char* filename, 
						 unsigned long* len)				;	
	
		void printShaderInfoLog(GLuint obj)					;
	
		void printProgramInfoLog(GLuint obj)				;

		bool CreateShader(GLenum _shader_type, 
							GLuint _shader_id, 
							const char* _source)			;
	
	public:
	
		//Constructors 1
		ShaderProgram()										;
		//Constructor: loads two files with same name but different file endings:
		//".vert" for vertex shader, ".frag" for fragment shader, compiles shaders
		//and creates shader program, links shaders to program.
		ShaderProgram(const char* fileName)					;
		//Destructor
		~ShaderProgram()									;
		
		//Loads and compiles a vertex shader from a given text file (if no program is present it is created)
		bool LoadVertexShader(const char* fileName)			;
		//Loads and compiles a fragment shader from a given text file (if no program is present it is created)
		bool LoadFragmentShader(const char* fileName)		;
		//Loads and compiles a fragment shader from a given text file (if no program is present it is created)
		bool LoadGeometryShader(const char* fileName)		;
		//Not implemented!!
		bool LoadShaderInfoFile(const char* fileName)		;
		//Compiles a vertex shader from given character string as source code (if no program is present it is created)
		bool	VertexShaderFromSource(const char* _src)	;
		//Compiles a fragment shader from given character string as source code (if no program is present it is created)
		bool	FragmentShaderFromSource(const char* _src)	;
		//Compiles a fragment shader from given character string as source code (if no program is present it is created)
		bool GeometryShaderFromSource(const char* _src)		;
		//Links shaders to the program
		bool	LinkProgram()								;
		//Use this program for rendering
		void	StartProgram()								;
		//Stop program. Only stops the current program when it was activated with "StartProgram"
		void	StopProgram()								;
	
		//Getter-Methods
		GLuint	GetVertexShaderID()							;
		GLuint	GetFragmentShaderID()						;
		GLuint	GetGeometryShaderID()						;
		GLuint	GetProgram()								;
		//Gets the location of the uniform variable with the given name
		GLint	GetUniformVarLocation(const GLchar* name)	;
	
	};
}
#endif