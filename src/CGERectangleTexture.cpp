#include "CGERectangleTexture.h"

namespace CGEngine
{

	bool CGERectangleTexture::VLoadTexture(const std::string& _fileName, GLenum _pixel_store_param, GLint _pixel_store_value)
	{
		//TODO: check if texture already allocated
		Image<GLubyte> img(_fileName.c_str());
		if (img.getErrorCode() != OK)
		{
			return false;
		}

		m_Width = (int)img.getWidth();
		m_Height = (int)img.getHeight();
		GLint format = (img.getColorModel() == RGB) ? GL_RGB : GL_RGBA;

		//Load image to GPU
		glGenTextures(1, &m_TextureID);

		glBindTexture(GL_TEXTURE_RECTANGLE, m_TextureID);
		glPixelStorei(_pixel_store_param, _pixel_store_value);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAX_LEVEL, 0);
		glTexImage2D(GL_TEXTURE_RECTANGLE, 0, format, (GLsizei)(img.getWidth()),
			(GLsizei)(img.getHeight()), 0, format, GL_UNSIGNED_BYTE,
			img.getDataPointer());
		glBindTexture(GL_TEXTURE_RECTANGLE, 0);

		return true;
	}

	bool CGERectangleTexture::VLoadTexture(GLsizei _width, GLsizei _height, GLenum _internal_format, GLenum _tex_format, GLenum _type,
		GLvoid* _data)
	{
		bool ret_value = true;
		if (m_TextureID != 0)
		{
			this->VReleaseTexture();
		}
		GLint level = 0; GLint border = 0;
		glGenTextures(1, &m_TextureID);
		glBindTexture(GL_TEXTURE_RECTANGLE, m_TextureID);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAX_LEVEL, 0);
		glTexImage2D(GL_TEXTURE_RECTANGLE, level, _internal_format, _width, _height, border, _tex_format, _type, _data);
		if (glGetError() != GL_NO_ERROR)
			ret_value = false;
		glBindTexture(GL_TEXTURE_RECTANGLE, 0);
		m_Width = (int)_width; m_Height = (int)_height;
		return ret_value;
	}

	bool CGERectangleTexture::MakeEmptyTexture(GLsizei _width, GLsizei _height, GLenum _internal_format, GLenum _tex_format, GLenum _type)
	{
		bool ret_value = true;
		if (m_TextureID != 0)
		{
			this->VReleaseTexture();
		}
		GLint level = 0; GLint border = 0;
		glGenTextures(1, &m_TextureID);
		glBindTexture(GL_TEXTURE_RECTANGLE, m_TextureID);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAX_LEVEL, 0);
		glTexImage2D(GL_TEXTURE_RECTANGLE, level, _internal_format, _width, _height, border, _tex_format, _type, (GLvoid*)0);
		if (glGetError() != GL_NO_ERROR)
			ret_value = false;
		glBindTexture(GL_TEXTURE_RECTANGLE, 0);
		m_Width = (int)_width; m_Height = (int)_height;
		return ret_value;
	}

	GLenum CGERectangleTexture::VGetTextureTarget()
	{
		return GL_TEXTURE_RECTANGLE;
	}

} //End namespace