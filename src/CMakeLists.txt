################################################################
#   OpenGL Project - Main CMakeLists.txt
################################################################

#Project Name and Prog.-Language (C++)
project(OGLTest CXX)

#Minimal CMake-Version
cmake_minimum_required(VERSION 2.8)

message("OGLTest - Starting Project Build")

MESSAGE(STATUS "PROJECT_BINARY_DIR: ${PROJECT_BINARY_DIR}")
MESSAGE(STATUS "PROJECT_SOURCE_DIR: ${PROJECT_SOURCE_DIR}")
MESSAGE(STATUS "CMAKE_CURRENT_BINARY_DIR: ${CMAKE_CURRENT_BINARY_DIR}")
MESSAGE(STATUS "CMAKE_FILES_DIRECTORY: ${CMAKE_FILES_DIRECTORY}")
MESSAGE(STATUS "CMAKE_SOURCE_DIR: ${CMAKE_SOURCE_DIR}")
get_filename_component(PARENT ${PROJECT_BINARY_DIR} DIRECTORY)
MESSAGE(STATUS "PARENT OF PROJECT_BINARY_DIR: ${PARENT}")

##############################################
# Compiler Settings
##############################################

#include module for finding assimp and others
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}")


#MSVC-related settings for the compiler
if(MSVC)
	add_definitions(-D_CRT_SECURE_NO_WARNINGS -D_CONSOLE)
endif(MSVC)


##############################################
# Libraries
##############################################

#OpenGL
find_package(OpenGL REQUIRED)
if(NOT OPENGL_FOUND)
  message (FATAL_ERROR "OpengGL not found!")
endif ()

if(MSVC)
	set(EXTRA_LIBS "glu32.lib" "opengl32.lib")
else(MSVC)
	set(EXTRA_LIBS ${OPENGL_LIBRARIES})
endif(MSVC)

#GL Extension Wrangler
find_package(GLEW)
if(NOT GLEW_FOUND)
  message(FATAL_ERROR "GLEW not found!")
endif ()
message(STATUS "GLEW: " ${GLEW_LIBRARIES})

link_directories(${GLEW_INCLUDE_PATH})

#GLM Library
find_package(GLM)
if(NOT GLM_FOUND)
  message(FATAL_ERROR "GLM not found!")
endif ()

link_directories(${GLM_INCLUDE_DIRS})

#Assimp Library
find_package(ASSIMP)
if(NOT ASSIMP_FOUND)
  message(FATAL_ERROR "ASSIMP not found!")
endif ()

#Boost
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME ON)
find_package(Boost 1.58 COMPONENTS system filesystem REQUIRED)

if(NOT Boost_FOUND)
    message(FATAL_ERROR "BOOST not found!")
endif ()
message(STATUS "Boost libraries are ${Boost_LIBRARIES}")

# just print out the variables to see their values
MESSAGE(STATUS "Boost information:")
MESSAGE(STATUS "Boost_INCLUDE_DIRS: ${Boost_INCLUDE_DIRS}")
MESSAGE(STATUS "Boost_INCLUDE_DIR: ${Boost_INCLUDE_DIR}")
MESSAGE(STATUS "Boost_LIBRARIES: ${Boost_LIBRARIES}")
MESSAGE(STATUS "Boost_LIBRARY_DIRS: ${Boost_LIBRARY_DIRS}")

link_directories( ${Boost_LIBRARY_DIRS} )

set(CXX_FLAGS "-lboost_system" "-lboost_chrono")

#GLFW
set(GLFW_INCLUDE_DIRECTORY "" CACHE PATH "Path to GLFW include folder")
set(GLFW_LIB_DIRECTORY "" CACHE PATH "Path to the GLFW static library")

#warning: needs to be set depending on build configuration!! TODO: automatically
set(GLFW_LIB "${GLFW_LIB_DIRECTORY}/Debug/glfw3.lib")
set(EXTRA_LIBS ${EXTRA_LIBS} ${GLFW_LIB})

include_directories(    ${OPENGL_INCLUDE_DIR}
						${GLFW_INCLUDE_DIRECTORY}
						${GLEW_INCLUDE_PATH}
                        ${ASSIMP_INCLUDE_DIR}
                        ${Boost_INCLUDE_DIR}
                        ${Boost_INCLUDE_DIRS}
                        ${GLM_INCLUDE_DIRS}
)

get_filename_component(PARENT ${PROJECT_BINARY_DIR} DIRECTORY)

#file(GLOB_RECURSE SOURCE_FILES *.cpp)
#file(GLOB_RECURSE HEADER_FILES *.h)
include(basic_files.cmake)

###################################################################

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PARENT}/Bin/Noise )
message(STATUS "Binary Dir: ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")

add_executable(Noise	${HEADER_FILES} main.cc ${SOURCE_FILES})

target_link_libraries(Noise 	${Boost_LIBRARIES} 
				${GLEW_LIBRARIES}
				${ASSIMP_LIBRARY}
				${EXTRA_LIBS}
				)

###################################################################

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PARENT}/Bin/BoostTesting )
message(STATUS "Binary Dir: ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")

add_executable(BoostTest ${HEADER_FILES} _main_t.cc ${SOURCE_FILES})

target_link_libraries(BoostTest 	
				${Boost_LIBRARIES} 
				${GLEW_LIBRARIES}
				${ASSIMP_LIBRARY}
				${EXTRA_LIBS}
				)

###################################################################

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PARENT}/Bin/Shadowmapping )
message(STATUS "Binary Dir: ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")

add_executable(Shadowmapping	${HEADER_FILES} shadow_main.cc ${SOURCE_FILES})

target_link_libraries(Shadowmapping 	
				${Boost_LIBRARIES} 
				${GLEW_LIBRARIES}
				${ASSIMP_LIBRARY}
				${EXTRA_LIBS}
				)

###################################################################

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PARENT}/Bin/BoundingSpheres )
message(STATUS "Binary Dir: ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")

add_executable(BoundingSpheres	${HEADER_FILES} main_bs.cc ${SOURCE_FILES})

target_link_libraries(BoundingSpheres 	
				${Boost_LIBRARIES} 
				${GLEW_LIBRARIES}
				${ASSIMP_LIBRARY}
				${EXTRA_LIBS}
				)

###################################################################

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PARENT}/Bin/TetrahedralMeshes )
message(STATUS "Binary Dir: ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")

find_package(CGAL QUIET)

if(CGAL_FOUND)
	include(tetmeshes_files.cmake)
	include(${CGAL_USE_FILE})
	include( CGAL_CreateSingleSourceCGALProgram )
	
	set(CGAL_3RD_PARTY_LIBRARIES 	${CGAL_3RD_PARTY_LIBRARIES}
									${Boost_LIBRARIES}
									${GLEW_LIBRARIES}
									${ASSIMP_LIBRARY}
									${EXTRA_LIBS}
									)

	file(GLOB_RECURSE ALL_FILES *.cpp *.h)
	set(ALL_FILES main_tet.cc ${ALL_FILES})
	
	create_single_source_cgal_program(${ALL_FILES})

else(CGAL_FOUND)
  message (FATAL_ERROR "CGAL not found!")
endif (CGAL_FOUND)

#add_executable(TetrahedralMeshes ${HEADER_FILES} main_tet.cc ${SOURCE_FILES})

#target_link_libraries(TetrahedralMeshes 	
#				${Boost_LIBRARIES}
#				${CGAL_LIBS}
#				${GLEW_LIBRARIES}
#				${ASSIMP_LIBRARY}
#				${EXTRA_LIBS}
#				)
