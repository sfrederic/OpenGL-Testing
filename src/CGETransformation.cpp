#include "CGETransformation.h"

namespace CGEngine {

CMatrixStack::CMatrixStack()
{
    m_Stack.clear();
    m_Stack.push_back(Matrix4x4(1.0));
	m_InvTransposeTemp = Matrix3x3(1.0);
}

CMatrixStack::~CMatrixStack()
{
	m_Stack.clear();
}

void CMatrixStack::Translate(const Vec3 &_TranslationVector)
{
    m_Stack.back() = Matrix4x4( glm::translate(m_Stack.back(), _TranslationVector) ) ;
}

void CMatrixStack::Rotate(const float _Angle, const Vec3 &_RotationAxis)
{
    m_Stack.back() = Matrix4x4( glm::rotate(m_Stack.back(), glm::radians(_Angle), _RotationAxis) ) ;
}

void CMatrixStack::Scale(const Vec3 &_ScalingFactors)
{
    m_Stack.back() = Matrix4x4( glm::scale(m_Stack.back(), _ScalingFactors) ) ;
}

void CMatrixStack::UniformScale(const float _ScalingFactor)
{
    Scale( Vec3(_ScalingFactor,_ScalingFactor,_ScalingFactor) );
}

void CMatrixStack::MultMatrix( const Matrix4x4& _mat )
{
	m_Stack.back() *= _mat;
}

void CMatrixStack::MultMatrix( Matrix4x4 const * _mat )
{
	if ( _mat )
		m_Stack.back() *= *_mat;
	else
		return;
}

void CMatrixStack::LoadIdentity()
{
    m_Stack.back() = Matrix4x4(1.0);
	m_InvTransposeTemp = Matrix3x3(1.0);
}

void CMatrixStack::PushMatrix()
{
    m_Stack.push_back( m_Stack.back() );
}

void CMatrixStack::PopMatrix()
{
	if ( m_Stack.size() > 1 )
	{
		m_Stack.pop_back();
	}
}

const Matrix4x4 &CMatrixStack::getCurrentTransform() const
{
    return m_Stack.back();
}

const float *CMatrixStack::getCurrentTransformPointer() const
{
    return glm::value_ptr( m_Stack.back() );
}

CGEngine::Matrix3x3 CMatrixStack::getCurrentNormalMatrix()
{
	return Matrix3x3(glm::inverseTranspose( (m_Stack.back()) ));
}

const float* CMatrixStack::getCurrentNormalMatrixPtr()
{
	m_InvTransposeTemp = Matrix3x3(glm::inverseTranspose( (m_Stack.back()) ));
	return glm::value_ptr( m_InvTransposeTemp );
}

void CMatrixStack::EmptyStack()
{
    m_Stack.clear();
    m_Stack.push_back( Matrix4x4(1.0) );
}

std::size_t CMatrixStack::StackDepth() const
{
	return m_Stack.size();
}

void CMatrixStack::ComputeNormalMatrix(Matrix3x3 &_NormalMatrix, const Matrix4x4 &_Model, const Matrix4x4 &_View)
{
    Matrix4x4 modelview = _View * _Model;
    _NormalMatrix = glm::inverseTranspose( Matrix3x3( modelview ) );
}


void CMatrixStack::ComputeNormalMatrix( Matrix3x3& _NormalMatrix, const Matrix4x4& _matrix /*= Matrix4x4(1.0)*/ )
{
	_NormalMatrix = glm::inverseTranspose( Matrix3x3( _matrix ) );
}

void CMatrixStack::ComputeNormalMatrix( Matrix3x3& _NormalMatrix, const Matrix4x4* _Model /*= NULL*/, const Matrix4x4* _View /*= NULL*/ )
{
	if ( _Model == NULL && _View == NULL )
		_NormalMatrix = Matrix3x3(1.0);
	if ( _View == NULL )
		_NormalMatrix = Matrix3x3(glm::inverseTranspose(*_Model));
	else
		_NormalMatrix = Matrix3x3(glm::inverseTranspose(*_Model * *_View));
}

}   //End Namespace
