#pragma once

#include "CGEGeometry.h"
#include <algorithm>
#include <exception>
#include <memory>
#include <typeinfo>

namespace CGEngine
{
	namespace Attributes
	{
		struct AttributeHeader
		{
			GLint		m_NumComponentsPerVertex; //can be either 1,2,3,4 or GL_BGRA
			GLenum		m_Type;
			GLboolean	m_Normalized;
			GLsizei		m_Stride;

			AttributeHeader() : m_NumComponentsPerVertex(4),
				m_Type(GL_FLOAT), m_Normalized(GL_FALSE), m_Stride(0) {}

			AttributeHeader(GLint c, GLuint i, GLenum t, GLboolean n, GLsizei s) :
				m_NumComponentsPerVertex(c),
				m_Type(t), m_Normalized(n), m_Stride(s) {}

			AttributeHeader(const AttributeHeader& _rhs)
			{
				m_NumComponentsPerVertex = _rhs.m_NumComponentsPerVertex;
				m_Type = _rhs.m_Type;
				m_Normalized = _rhs.m_Normalized;
				m_Stride = _rhs.m_Stride;
			}

			AttributeHeader& operator=(const AttributeHeader& _rhs)
			{
				if (this != &_rhs)
				{
					m_NumComponentsPerVertex = _rhs.m_NumComponentsPerVertex;
					m_Type = _rhs.m_Type;
					m_Normalized = _rhs.m_Normalized;
					m_Stride = _rhs.m_Stride;
				}
				return *this;
			}
		};

		/************************************************************************/
		/* Defines for different data types how attributes are stored on server side
		/************************************************************************/
		class CAttributeStorageStrategy;
		using AttribStoragePtr = std::shared_ptr<CAttributeStorageStrategy>;

		class CAttributeStorageStrategy
		{
		public:
			CAttributeStorageStrategy();
			CAttributeStorageStrategy(const CAttributeStorageStrategy& _rhs);
			virtual ~CAttributeStorageStrategy();

			virtual void VAttributeStorage(GLuint _vertexArrayObject, GLuint _attribIndex, GLvoid* _data_offset) = 0;

			void AttributeBufferData(GLuint _bufferObject, GLsizeiptr _size, const GLvoid* _data, GLenum _usage);
		protected:
			AttributeHeader m_AttributeHeader;
		};

		class CFloatingPointAttributeStorage : public CAttributeStorageStrategy
		{
		public:
			CFloatingPointAttributeStorage() = delete;
			CFloatingPointAttributeStorage(const CFloatingPointAttributeStorage& _rhs);
			CFloatingPointAttributeStorage(GLint _numComponents, GLsizei _stride = 0, GLenum _concreteType = GL_FLOAT);
			~CFloatingPointAttributeStorage();

			virtual void VAttributeStorage(GLuint _vertexArrayObject, GLuint _attribIndex, GLvoid* _data_offset) override;
		};

		class CNormalizedIntegerAttributeStorage : public CAttributeStorageStrategy
		{
		public:
			CNormalizedIntegerAttributeStorage() = delete;
			CNormalizedIntegerAttributeStorage(const CNormalizedIntegerAttributeStorage& _rhs);
			CNormalizedIntegerAttributeStorage(GLint _numComponents, GLsizei _stride = 0, GLenum _concreteType = GL_INT);
			~CNormalizedIntegerAttributeStorage();

			virtual void VAttributeStorage(GLuint _vertexArrayObject, GLuint _attribIndex, GLvoid* _data_offset) override;
		};

		class CIntegerAttributeStorage : public CAttributeStorageStrategy
		{
		public:
			CIntegerAttributeStorage() = delete;
			CIntegerAttributeStorage(const CIntegerAttributeStorage& _rhs);
			CIntegerAttributeStorage(GLint _numComponents, GLsizei _stride = 0, GLenum _concreteType = GL_INT);
			~CIntegerAttributeStorage();

			virtual void VAttributeStorage(GLuint _vertexArrayObject, GLuint _attribIndex, GLvoid* _data_offset) override;
		};

		class CDoublePrecisionAttributeStorage : public CAttributeStorageStrategy
		{
		public:
			CDoublePrecisionAttributeStorage() = delete;
			CDoublePrecisionAttributeStorage(const CDoublePrecisionAttributeStorage& _rhs);
			CDoublePrecisionAttributeStorage(GLint _numComponents, GLsizei _stride = 0);
			~CDoublePrecisionAttributeStorage();

			virtual void VAttributeStorage(GLuint _vertexArrayObject, GLuint _attribIndex, GLvoid* _data_offset) override;
		};

		/************************************************************************/
		/* Attribute data vector
		 * Store attributes of type T in array
		/************************************************************************/

		class CAttribArrayBase /*Interface*/
		{
		public:
			CAttribArrayBase() {}
			CAttribArrayBase(const CAttribArrayBase&) {};
			~CAttribArrayBase(){}

			virtual void Reserve(std::size_t n) = 0;
			virtual void Resize(std::size_t n) = 0;
			virtual void PushBack() = 0;
			virtual void Clear() = 0;
			virtual const std::type_info& Type() = 0;
			virtual std::size_t Size() const = 0;
			virtual GLsizei MemSize() const = 0;
			virtual const GLvoid* AttribDataPointer() const = 0;
			virtual const std::string& AttributeName() const = 0;
		};

		using AttribArrayBaseShPtr = std::shared_ptr<CAttribArrayBase>;

		//////////////////////////////////////////////////////////////////////////
		// Stores an array of attribute values of type T
		//////////////////////////////////////////////////////////////////////////
		template <class T, 
			typename = typename std::enable_if<std::is_copy_constructible<T>::value>::type,
			typename = typename std::enable_if<std::is_default_constructible<T>::value>::type
		>
		class CAttributeArray : public CAttribArrayBase
		{
		public:
			using VectorT			= std::vector<T>;
			using reference			= typename VectorT::reference;
			using const_reference	= typename VectorT::const_reference;
			using iterator			= typename VectorT::iterator;
			using const_iterator	= typename VectorT::const_iterator;

			CAttributeArray() = delete;

			CAttributeArray(const std::string& _attrib_name, T _t = T()) : CAttribArrayBase(),
				m_AttribName(_attrib_name), m_AttribValueDummy(_t)
			{}

			//deep copy constructor
			CAttributeArray(const CAttributeArray& _rhs)
			{
				if (this != &_rhs)
				{
					Clear();
					std::copy(_rhs.ConstBegin(), _rhs.ConstEnd(), m_AttribData.begin());
					m_AttribName = _rhs.AttributeName();
				}
				return *this;
			}

			//deep copy assignment
			CAttributeArray& operator= (const CAttributeArray& _rhs)
			{
				if (this != &_rhs)
				{
					Clear();
					std::copy(_rhs.ConstBegin(), _rhs.ConstEnd(), m_AttribData.begin());
					m_AttribName = _rhs.AttributeName();
				}
				return *this;
			}

			~CAttributeArray()
			{
				Clear();
			}

			void Reserve(std::size_t _n) override
			{
				m_AttribData.reserve(_n);
			}

			void Resize(std::size_t _n) override
			{
				m_AttribData.resize(_n, m_AttribValueDummy);
			}

			void PushBack() override
			{
				m_AttribData.push_back(m_AttribValueDummy);
			}

			void Clear() override
			{
				m_AttribData.clear();
				m_AttribName.clear();
			}

			const std::type_info& Type() override
			{
				return typeid(T);
			}

			std::size_t Size() const override
			{
				return m_AttribData.size();
			}

			GLsizeiptr MemSize() const override
			{
				return static_cast<GLsizeiptr>(m_AttribData.size() * sizeof(T));
			}

			const std::string& AttributeName() const override
			{
				return m_AttribName;
			}

			const GLvoid* AttribDataPointer() const override
			{
				return static_cast<const GLvoid*>(&m_AttribData[0]);
			}

			reference operator[](std::size_t _index)
			{
				if (_index >= m_AttribData.size())
					throw std::out_of_range(std::string("Attribute ") + m_AttribName + std::string("::operator[]: _index out of range!"));
				return m_AttribData[_index];
			}

			const_reference operator[](std::size_t _index) const
			{
				if (_index >= m_AttribData.size())
					throw std::out_of_range(std::string("Attribute ") + m_AttribName + std::string("::operator[]: _index out of range!"));
				return m_AttribData[_index];
			}

			iterator Begin()
			{ 
				return m_AttribData.begin();
			}
			iterator End()
			{
				return m_AttribData.end();
			}
			const_iterator ConstBegin() const
			{
#ifdef WIN32
				return m_AttribData.cbegin();
#else
				return m_AttribData.begin();
#endif
			}
			const_iterator ConstEnd() const
			{
#ifdef WIN32
				return m_AttribData.cend();
#else
				return m_AttribData.end();
#endif
			}

		private:
			VectorT		m_AttribData;
			T			m_AttribValueDummy;
			std::string m_AttribName;
		};

		/************************************************************************/
		/* Attribute Container
		 * contains an array of attribute arrays
		/************************************************************************/

		class CAttributeContainer
		{
		public:
			CAttributeContainer() : m_Size(0){}
			~CAttributeContainer() { m_Attributes.clear(); }

			// Returns the number of attributes
			inline std::size_t GetNumAttributes() const 
			{ 
				return m_Attributes.size(); 
			}

			// Get all the attribute names
			void GetAttributeNames(std::vector<std::string>& _names) const
			{
				for (std::size_t i = 0; i < m_Attributes.size(); ++i)
					_names.push_back(m_Attributes[i]->AttributeName());
			}

			/* Add an attribute with name _name and default value _t. If an attribute with _name already exists,
			it returns the existing attribute */
			template<class T>
			AttribArrayBaseShPtr AddAttribute(const std::string& _name, const T _t = T())
			{
				//check if already present
				for (std::size_t i = 0; i < m_Attributes.size(); ++i)
				{
					AttribArrayBaseShPtr res = get_attrib_ptr(_name, i);
					if (res)
						return res;
				}
				//if not, add to array
				AttribArrayBaseShPtr pAttrb = std::make_shared<CAttributeArray<T>>(_name, _t);
				pAttrb->Resize(m_Size);
				m_Attributes.push_back(pAttrb);
				return pAttrb;
			}

			//Get an attribute by its name. Returns null pointer if it does not exist.
/*			template <class T>*/
			AttribArrayBaseShPtr Get(const std::string& _name) const
			{
				for (std::size_t i = 0; i < m_Attributes.size(); ++i)
				{
					AttribArrayBaseShPtr res = get_attrib_ptr(_name, i);
					if (res)
						return res;
				}
				return AttribArrayBaseShPtr(nullptr);
			}

			//Remove an attribute with _name, returns true if attribute was found and erased, false otherwise.
/*			template <class T>*/
			bool Remove(const std::string _name)
			{
				std::vector<AttribArrayBaseShPtr>::iterator it = m_Attributes.begin(), it_end = m_Attributes.end();
				for (; it != it_end; ++it)
				{
					if ((*it)->AttributeName() == _name)
					{
						m_Attributes.erase(it);
						return true;
					}
				}
				return false;
			}

		public:

			// delete all attributes
			void clear()
			{
				for (std::size_t i = 0; i < m_Attributes.size(); ++i)
					m_Attributes[i].reset();
				m_Attributes.clear();
				m_Size = 0;
			}

			// add a new element to each vector
			void push_back()
			{
				for (std::size_t i = 0; i < m_Attributes.size(); ++i)
					m_Attributes[i]->PushBack();
				m_Size++;
			}

			// reserve memory for _n entries in all arrays
			void reserve(std::size_t _n) const
			{
				for (std::size_t i = 0; i < m_Attributes.size(); ++i)
					m_Attributes[i]->Reserve(_n);
			}

			// resize all arrays to size _n
			void resize(std::size_t _n)
			{
				for (std::size_t i = 0; i < m_Attributes.size(); ++i)
					m_Attributes[i]->Resize(_n);
				m_Size = _n;
			}

			bool empty() const
			{
				return m_Attributes.empty();
			}

			std::size_t size() const
			{
				return m_Size;
			}

			//returns size of memory allocated by attribute with name _attrib_name.
			GLsizeiptr mem_size(const std::string& _attrib_name) const
			{
				for (std::size_t i = 0; i < m_Attributes.size(); ++i)
				{
					if (m_Attributes[i]->AttributeName() == _attrib_name)
						return m_Attributes[i]->MemSize();
				}
				return 0;
			}

			const GLvoid* const GetAttribDataPointer(const std::string& _attrib_name) const
			{
				for (std::size_t i = 0; i < m_Attributes.size(); ++i)
				{
					AttribArrayBaseShPtr res = get_attrib_ptr(_attrib_name, i);
					if (res)
						return res->AttribDataPointer();
				}
				return 0;
			}

		protected:

			inline AttribArrayBaseShPtr get_attrib_ptr(const std::string& _name, std::size_t _index) const
			{
				if (_index > m_Attributes.size())
					return AttribArrayBaseShPtr(nullptr);
				if (m_Attributes[_index]->AttributeName() == _name)
				{
					return m_Attributes[_index];
				}
				return AttribArrayBaseShPtr(nullptr);
			}

			std::vector<AttribArrayBaseShPtr>	m_Attributes;
			std::size_t							m_Size; //common size for all attribute arrays in m_Attributes
		private:
		};

		/************************************************************************/
		/* Attribute wrapper class
		 * ensures usage of attributes with type T without having to use dynamic casts from the
		 * shared base class pointer
		/************************************************************************/

		template<class T>
		class CAttributeWrapper
		{
		public:
			using reference			= typename CAttributeArray<T>::reference;
			using const_reference	= typename CAttributeArray<T>::const_reference;
			using iterator			= typename CAttributeArray<T>::iterator;
			using const_iterator	= typename CAttributeArray<T>::const_iterator;
			using VectorT			= typename CAttributeArray<T>::VectorT;

			friend class CAttributeContainer;

			CAttributeWrapper() = delete;
			~CAttributeWrapper(){}

			CAttributeWrapper(AttribArrayBaseShPtr _attrib_array)
			{
				try
				{
					m_pAttribArray = dynamic_cast<CAttributeArray<T>*>( _attrib_array.get() );
				}
				catch (std::bad_cast* e)
				{
					std::cerr << "Attribute wrapper class constructor exception: " << e->what() << std::endl;
					throw;
				}
			}

			CAttributeWrapper(const CAttributeWrapper& _rhs)
			{
				m_pAttribArray = _rhs.m_pAttribArray;
			}

			CAttributeWrapper& operator=(const CAttributeWrapper& _rhs)
			{
				if (this != &_rhs)
				{
					m_pAttribArray = _rhs.m_pAttribArray;
				}
				return *this;
			}

			CAttributeWrapper(CAttributeWrapper&& _rhs)
			{
				if (this != &_rhs)
				{
					m_pAttribArray = _rhs.m_pAttribArray;
					_rhs.m_pAttribArray = nullptr;
				}
			}

			CAttributeWrapper& operator=(CAttributeWrapper&& _rhs)
			{
				if (this != &_rhs)
				{
					m_pAttribArray = _rhs.m_pAttribArray;
					_rhs.m_pAttribArray = nullptr;
				}
				return *this;
			}

			reference operator[](std::size_t _index)
			{
				if (m_pAttribArray)
					return (*m_pAttribArray)[_index];
			}

			const_reference operator[](std::size_t& _index) const
			{
				if (m_pAttribArray)
					return (*m_pAttribArray)[_index];
			}

			void CopyFrom(const VectorT& _from)
			{
				std::copy(_from.begin(), _from.end(), m_pAttribArray->Begin());
			}

			void CopyFrom(iterator _from_begin, iterator _from_end)
			{
				std::copy(_from_begin, _from_end, m_pAttribArray->Begin());
			}

			iterator begin()
			{ 
				return m_pAttribArray->Begin();
			}

			iterator end() 
			{
				return m_pAttribArray->End();
			}

			const_iterator cbegin() const
			{
				return m_pAttribArray->ConstBegin();
			}

			const_iterator cend() const
			{
				return m_pAttribArray->ConstEnd();
			}

			const GLvoid* PointerToData() const
			{
				if (m_pAttribArray)
					return m_pAttribArray->AttribDataPointer();
				else
					return nullptr;
			}

			operator bool()
			{
				return (m_pAttribArray == nullptr);
			}

		protected:
			
			CAttributeArray<T>* m_pAttribArray;
		private:
		};

	} //End Namespace attributes
}