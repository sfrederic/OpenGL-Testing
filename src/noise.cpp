#include "noise.h"

NoiseTexture::NoiseTexture()
{
	noise3DTexName=0;
	noise3DTexPtr=nullptr;
	noise3DtexSize=128;
}

NoiseTexture::NoiseTexture( int _size )
{
	noise3DTexName=0;
	noise3DTexPtr=nullptr;
	noise3DtexSize=_size;
}

void NoiseTexture::make3DNoiseTexture()
{
	int f, i, j, k, inc;
	float startFrequency=4.0f;
	int numOctaves=4;
	glm::vec3 ni(0.0, 0.0, 0.0);
	float inci, incj, inck;
	float frequency = startFrequency;
	GLubyte* ptr;
	float amp=0.5f;

	if ( (noise3DTexPtr = (GLubyte*)malloc(noise3DtexSize * noise3DtexSize * noise3DtexSize * 4 * sizeof(GLubyte))) == nullptr )
	{
		std::cerr << "Could not allocate texture memory!\n";
		return;
	}
	srand(126);

	for ( f = 0, inc = 0; f < numOctaves;
		  ++f, frequency *= 2.0f, ++inc, amp *= 0.5f )
	{
		float p = 1.0f/frequency;
		glm::vec3 period_(frequency, frequency, frequency);
		ptr = noise3DTexPtr;
		ni = glm::vec3(0.0);

		inci = 1.0f/(noise3DtexSize/frequency);
		for ( i = 0; i < noise3DtexSize; ++i, ni[0] += inci )
		{
			incj = 1.0f/(noise3DtexSize/frequency);
			for ( j = 0; j < noise3DtexSize; ++j, ni[1] += incj )
			{
				inck = 1.0f/(noise3DtexSize/frequency);
				for ( k = 0; k < noise3DtexSize; ++k, ni[2] += inck, ptr += 4 )
				{
					*(ptr+inc) = static_cast<GLubyte>( ((glm::perlin( ni, period_ )+1.0) * amp)*128.0 );
					//*(ptr+inc) = static_cast<GLubyte>(( ((float)rand()/(float)RAND_MAX) * amp)*128.0 );
				}
			}
		}
	}

	/*std::ofstream outf("C:\\Users\\Frederic\\Entwicklung\\OpenGL_Testing\\Data\\noise.ppm");
	if (!outf)
	{
		// Print an error and exit
		std::cerr << "Uh oh, could not be opened for writing!\n";
		return;
	}
	std::vector<GLubyte> out_data(4*noise3DtexSize*noise3DtexSize, 0);
	int W =  noise3DtexSize*4;
	for (int j = 0; j < noise3DtexSize; ++j)
	{
		for (int i = 0, k=0; i < W; i+=4, ++k)
		{
			out_data[ j * 2*noise3DtexSize + k ]									= noise3DTexPtr[j*W+i+0];
			out_data[ j * 2*noise3DtexSize + k + noise3DtexSize ]					= noise3DTexPtr[j*W+i+1];
			out_data[(j + noise3DtexSize)*2*noise3DtexSize + k ]					= noise3DTexPtr[j*W+i+2];
			out_data[(j + noise3DtexSize)*2*noise3DtexSize + k + noise3DtexSize ]	= noise3DTexPtr[j*W+i+3];
		}
	}

	outf << "P3\n" << 2*noise3DtexSize << " " << 2*noise3DtexSize << "\n" << 255 << "\n";
	
	for (int j = 0; j < 2*noise3DtexSize; ++j)
	{
		for (int i = 0; i < 2*noise3DtexSize; ++i)
		{
			outf << (int)out_data[j*2*noise3DtexSize+i] << " " << (int)out_data[j*2*noise3DtexSize+i] << " " << (int)out_data[j*2*noise3DtexSize+i] << " ";
		}
		outf << "\n";
	}*/

	//init3DNoiseTexture(GL_TEXTURE2);
}

void NoiseTexture::init3DNoiseTexture(GLenum _activeTexUnit)
{
	glGenTextures (1, &noise3DTexName);

	glActiveTexture(_activeTexUnit);
	glBindTexture (GL_TEXTURE_3D, noise3DTexName);
	glTexParameterf (GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf (GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf (GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameterf (GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf (GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage3D( GL_TEXTURE_3D, 0, GL_RGBA, noise3DtexSize, noise3DtexSize, noise3DtexSize, 0, GL_RGBA, GL_UNSIGNED_BYTE, noise3DTexPtr );
	glBindTexture (GL_TEXTURE_3D, 0);

	free(noise3DTexPtr);

}

void NoiseTexture::outputToFile( const std::string& _fileName )
{
	if (!noise3DTexPtr)
	{
		return;
	}
	else
	{
		std::ofstream outf(_fileName);
		if (!outf)
		{
			// Print an error and exit
			std::cerr << "Uh oh, " << _fileName << " could not be opened for writing!\n";
			return;
		}

		outf << noise3DtexSize << " " << noise3DtexSize << " " << noise3DtexSize << "\n";
		outf << "4\n";

		for ( int d = 0; d < noise3DtexSize; ++d )
		{
			outf << d << "\n";
			for ( int h = 0; h < noise3DtexSize; ++h )
			{
				for ( int w = 0; w < 4*noise3DtexSize; ++ w )
				{
				}
			}
		}

		outf.close();
	}
}

bool NoiseTexture::loadFromFile( const std::string& _fileName )
{
	std::ifstream inf(_fileName);
	if (!inf)
	{
		// Print an error and exit
		std::cerr << "Uh oh, " << _fileName << " could not be opened for reading!\n";
		return false;
	}

	inf.close();
	return true;
}

void NoiseTexture::bind( GLenum _activeTexUnit )
{
	glActiveTexture(_activeTexUnit);
	glBindTexture (GL_TEXTURE_3D, noise3DTexName);
}

