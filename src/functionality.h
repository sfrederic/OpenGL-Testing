#pragma  once
#ifndef __GLEW_H__
#define GLEW_STATIC
#include <GL/glew.h>
#endif

#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <boost/foreach.hpp>

#define GL_ENUM_TO_STR(gl_enum) std::string("#gl_enum")

namespace CGEngine
{
	namespace Functionality
	{
	void cout_gl_error(GLenum _error);
		
		void cout_gl_frambufferError(GLenum _error);
		
		void ReadDepthTextureToFile(const std::string& _output_file, int width, int height, std::int8_t DepthPrecision = 24);
		
		void ReadTextureFromGPUToFile(const std::string& _output_file);
		
		void ReadBufferFromGPUToFile(const std::string& _output_file, int width, int height, int rgb_format = 4, float _scale = 1.0f);
		
		template<class ScalarType, class Type = ScalarType>
		Type compute_mean_recursive(const std::vector<Type>& _values)
		{
			if (_values.empty())
				return Type(0);
			Type mean(_values[0]);
			for (std::size_t i = 1; i < _values.size(); ++i)
			{
				mean = mean + (static_cast<ScalarType>(1) / static_cast<ScalarType>(i + 1))*(_values[i] - mean);
			}
			return mean;
		}
	} //End namespace
}// END NAMESPACE