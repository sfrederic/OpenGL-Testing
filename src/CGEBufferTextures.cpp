#include "CGEBufferTextures.h"


namespace CGEngine
{
	CGEBufferTexture::CGEBufferTexture() : m_BufferObjectName(0), CGETextureBase()
	{

	}

	CGEBufferTexture::~CGEBufferTexture()
	{
		VReleaseTexture();
	}

	bool CGEBufferTexture::VLoadTexture(const std::string& _fileName, GLenum _pixel_store_param, GLint _pixel_store_value)
	{
		return false;
	}

	bool CGEBufferTexture::VLoadTexture(GLsizei _width, GLsizei _height, GLenum _internal_format, GLenum _tex_format, GLenum _type, GLvoid* _data)
	{
		if (m_BufferObjectName == 0)
			glGenBuffers(1, &m_BufferObjectName);
		if (m_TextureID == 0)
			glGenTextures(1, &m_TextureID);

		if (m_BufferObjectName == 0 || m_TextureID == 0)
		{
			return false;
		}

		glBindBuffer(GL_TEXTURE_BUFFER, m_BufferObjectName);
		glBufferData(GL_TEXTURE_BUFFER, _width, _data, _type);
		glBindTexture(GL_TEXTURE_BUFFER, m_TextureID);
		glTexBuffer(GL_TEXTURE_BUFFER, _internal_format, m_BufferObjectName);

		glBindBuffer(GL_TEXTURE_BUFFER, 0);
		glBindTexture(GL_TEXTURE_BUFFER, 0);
		return true;
	}

	void CGEBufferTexture::VReleaseTexture()
	{
		if (m_TextureID)
			glDeleteTextures(1, &m_TextureID);
		m_TextureID = 0;
		if (m_BufferObjectName)
			glDeleteBuffers(1, &m_BufferObjectName);
		m_BufferObjectName = 0;
	}

	GLenum CGEBufferTexture::VGetTextureTarget()
	{
		return GL_TEXTURE_BUFFER;
	}

} //End namespace
