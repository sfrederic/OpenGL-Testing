#include "CGEFullScreenQuad.h"

namespace CGEngine {

bool CFullScreenQuad::LoadMesh(const std::string &_filename, unsigned int _postprocessingFlags)
{
    /*m_Entries.resize( 1 );

    Vec3DVec positions;
    Vec3DVec normals;
    Vec2DVec texcoords;
    std::vector<GLuint> indices;

    unsigned int num_vertices = 4;
    unsigned int num_indices = 6;

    m_Entries[0].m_NumIndices = 6;
    m_Entries[0].m_BaseVertex = 0;
    m_Entries[0].m_BaseIndex = 0;
	m_Entries[0].m_MaterialIndex = 0;

    positions.push_back(Vec3(-1.0, -1.0, 0.0)); positions.push_back(Vec3(1.0, -1.0, 0.0));
    positions.push_back(Vec3(1.0, 1.0, 0.0)); positions.push_back(Vec3(-1.0, 1.0, 0.0));

    texcoords.push_back(Vec2(0.0, 0.0)); texcoords.push_back(Vec2(1.0, 0.0));
    texcoords.push_back(Vec2(1.0, 1.0)); texcoords.push_back(Vec2(0.0, 1.0));

    for (unsigned int i = 0 ; i < 4 ; i++) {
        normals.push_back( Vec3(0.0, 0.0, 1.0) );
    }
    //Index buffer
    indices.push_back(0); indices.push_back(1); indices.push_back(2);
    indices.push_back(0); indices.push_back(2); indices.push_back(3);

    // Generate and populate the buffers with vertex attributes and the indices
    populateBuffers(0, positions, texcoords, normals, indices);*/

    return false;
}

bool CFullScreenQuad::HasProperty(CGEMeshProperty _property) const
{
    return false;
}

bool CFullScreenQuad::MakeFullscreenQuad(float _width, float _height, float _z_value /*= 1.0f*/)
{
	m_Entries.resize(1);
	Vec3DVec positions;
	Vec3DVec normals;
	Vec2DVec texture_coords;
	std::vector<GLuint> indices;

	m_Entries[0].m_NumIndices = 6;
	m_Entries[0].m_BaseVertex = 0;
	m_Entries[0].m_BaseIndex = 0;
	m_Entries[0].m_MaterialIndex = 0;

	positions.push_back(Vec3(-_width , -_height , _z_value)); positions.push_back(Vec3(_width , -_height , _z_value));
	positions.push_back(Vec3(_width , _height , _z_value)); positions.push_back(Vec3(-_width , _height , _z_value));
	texture_coords.push_back( Vec2(0.0f, 0.0f) ); texture_coords.push_back(Vec2(1.0f, 0.0f));
	texture_coords.push_back(Vec2(1.0f, 1.0f)); texture_coords.push_back(Vec2(0.0f, 1.0f));
	for (unsigned int i = 0; i < 4; i++) {
		normals.push_back(Vec3(0.0, 0.0, 1.0));
	}

	//Index buffer
	indices.push_back(0); indices.push_back(1); indices.push_back(2);
	indices.push_back(0); indices.push_back(2); indices.push_back(3);

	// Generate and populate the buffers with vertex attributes and the indices
	populateBuffers(0, positions, texture_coords, normals, indices);

	return (glGetError() == GL_NO_ERROR);
}

}   //Namespace
