
/***********************************************************************

  Class for loading and rendering meshes

***********************************************************************/

#ifndef MESH_H
#define MESH_H

#pragma once

#ifndef __GLEW_H__
#define  GLEW_STATIC
#include <GL/glew.h>
#endif

#ifdef _WIN32
    #include <assimp/Importer.hpp>
    #include <assimp/scene.h>
    #include <assimp/postprocess.h>
#else
    #include <assimp/Importer.hpp>
    #include <assimp/scene.h>
    #include <assimp/postprocess.h>
#endif
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <vector>
#include <string>
#include <iostream>
#include "CGEGeometry.h"
#ifndef CGETEXTURE2D_H
    #include "CGETexture2D.h"
#endif
#include "CGERenderableDataGenerator.h"


namespace CGEngine {

#define ARRAY_SIZE_IN_ELEMENTS(x) (sizeof(x)/sizeof(x[0]))

//Class Header and Typedefs
//=========================
class CMesh ;
typedef boost::shared_ptr<CMesh>    MeshPointer ;

enum CGEMeshProperty
{
    CGE_HAS_TEXCOORDS       = 1 ,
    CGE_HAS_NORMALS         = 2 ,
    CGE_HAS_TANGENTSPACE    = 4
};

enum CGEMeshProcessingFlags
{
    CGE_NO_POSTPROCESSING           = 0x0,
    CGE_COMPUTE_NORMALS             = aiProcess_GenSmoothNormals,
    CGE_COMPUTE_TANGENT_SPACE       = aiProcess_CalcTangentSpace,
    CGE_JOIN_IDENT_VERTICES         = aiProcess_JoinIdenticalVertices,
    CGE_TRIANGULATE                 = aiProcess_Triangulate,
    CGE_REMOVE_REDUNDANT_MATERIALS  = aiProcess_RemoveRedundantMaterials,
    CGE_GEN_TEXT_COORDS             = aiProcess_GenUVCoords,
    CGE_FLIP_TEX_COORDS             = aiProcess_FlipUVs,
    CGE_FIX_INFACING_NORMALS        = aiProcess_FixInfacingNormals
};

//Class for loading and rendering meshes
//======================================
class CMesh
{
private:
	//disallow copy constructor/assignment
	CMesh(const CMesh&) {}
	CMesh& operator= (const CMesh&) {}

protected:
    GLuint m_VAO		;
	GLenum m_PrimitiveType;
	GLenum m_IndexType;
	GLenum m_DrawType;
	std::vector<GLuint> m_Buffers;
	std::vector<GLuint> m_AttribBuffers;
	std::vector<GLuint> m_AttribIndices;//temporary solution

    bool initFromScene(const aiScene* _pScene, const std::string& _filename)	;
	bool initFromGenerator(const CRenderableDataGenerator* _mesh_generator);

    void initMesh(const aiMesh* _paiMesh,
                  Vec3DVec& _positions,
                  Vec3DVec& _normals,
                  Vec2DVec& _texCoords,
                  std::vector<unsigned int>& _indices)                      ;

    bool initMaterials(const aiScene* _pScene, const std::string& _filename);

	bool initAttributes(const CRenderableDataGenerator* _mesh_generator);

    void setProperties(unsigned int _index, const aiMesh* _paiMesh)         ;

	virtual void populateBuffers( unsigned int _entry_number,
		const Vec3DVec &_positions, const Vec2DVec &_texcoords, 
		const Vec3DVec &_normals, const std::vector<GLuint> &_indices )		;

    virtual void clear()													;

#define INDEX_BUFFER    0
#define POS_VB          1
#define NORMAL_VB       2
#define TEXCOORD_VB     3

    //MeshEntry defines a part of a mesh (if mesh is composed of several parts)
    //and provides functionality for initializing the mesh parts
    class CMeshEntry {
	public:
		friend class CMesh;
        CMeshEntry() ;

        virtual ~CMeshEntry();

        unsigned int m_NumIndices       ;
		unsigned int m_NumVertices		;
        unsigned int m_BaseVertex       ;
        unsigned int m_BaseIndex        ;
        unsigned int m_MaterialIndex    ;
        unsigned int m_MeshProperties   ;

		GLuint		m_IBO					;
		GLuint		m_VBO					;
		GLsizeiptr	m_VertexArraySize		;
		GLsizeiptr	m_NormalArraySize		;
		GLsizeiptr	m_TexCoordsArraySize	;
		GLsizeiptr	m_IndexBufferSize		;
    };

    typedef std::vector<CMeshEntry> MeshEntryVector ;

    MeshEntryVector                 m_Entries       ;
    std::vector<GLTexture2DShPtr>	m_Textures      ;

	std::string						m_FileName		;

public:
    //Constructor
	CMesh(GLenum _PrimitiveType = GL_TRIANGLES, GLenum _IndexType = GL_UNSIGNED_INT, GLenum _DrawType = GL_STATIC_DRAW) :
		m_PrimitiveType(_PrimitiveType), m_IndexType(_IndexType), m_DrawType(_DrawType), m_VAO(0)
	{
	}
    //Destructor
    virtual ~CMesh()
    {
        m_Entries.clear();
        clear();
    }

    //Load a mesh given given by the filename or path
    bool LoadMesh(const std::string& _filename, unsigned int _postprocessingFlags = CGE_NO_POSTPROCESSING);
	bool LoadMesh(const CRenderableDataGenerator* _mesh_generator);
	bool LoadMesh(const Vec3DVec& _vertex_positions, const GLuintVec& _indices, const Vec3DVec& _normals,
		const Vec2DVec _texture_coordinates);

	virtual bool VGetVerticesFromGPUBuffer(CGEngine::Vec3DVec& _vertices) { return true; };

    //Draw Mesh
    virtual void VOnDraw();

    //Check if mesh has indicated property
    bool HasProperty(CGEMeshProperty _property) const;

	std::string GetFileName() const;

};

} //End NameSpace

#endif // MESH_H
