#ifndef CGESphere_h__
#define CGESphere_h__

#include <algorithm>
#include <iostream>
#include "CGEGeometry.h"
#include "CGEPrimitivesGenerator.h"
#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>

namespace CGEngine
{
	class CSphere		;

	class CSphere
	{
	public:
		CSphere();
		~CSphere()	;
		CSphere(const CSphere&) = delete;

		virtual bool VInitObject(const CSphereGeneratorShPtr& _sphereGenerator);

		virtual void VOnDraw();
	protected:
		GLuint		m_VBO;
		GLuint		m_IBO;
		GLsizeiptr	m_VertexArraySize;
		GLsizeiptr	m_NormalArraySize;
		GLsizeiptr	m_TexCoordsArraySize;
		GLsizeiptr	m_IndexBufferSize;
	
		virtual void populate_buffers(const Vec3DVec &_positions, const Vec2DVec &_texcoords, const Vec3DVec &_normals, const std::vector<GLuint> &_indices);
	private:
	};
}

#endif // CGESphere_h__
