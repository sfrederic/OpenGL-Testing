/************************************************************************
 *
 * Class for meshes for older OpenGL support
 * 
 ************************************************************************/

#include "CGEMesh.h"

#ifndef CGEMesh_OGL2_h__
#define CGEMesh_OGL2_h__

namespace CGEngine
{
	class CMeshOGL2	;
	typedef boost::shared_ptr<CMeshOGL2>	CMeshOGL2Ptr	;
	
	class CMeshOGL2 : public CMesh
	{
	public:
		CMeshOGL2(GLenum _PrimitiveType = GL_TRIANGLES, GLenum _IndexType = GL_UNSIGNED_INT, GLenum _DrawType = GL_DYNAMIC_DRAW);
		~CMeshOGL2() ;

		virtual void VOnDraw()	;

		virtual bool VGetVerticesFromGPUBuffer(CGEngine::Vec3DVec& _vertices);

	protected:

		virtual void populateBuffers( unsigned int _entry_number, const Vec3DVec &_positions, 
						const Vec2DVec &_texcoords, const Vec3DVec &_normals, 
						const std::vector<GLuint> &_indices )		;

		virtual void clear()								;

	private:
	};
}

#endif // CGEMesh_OGL2_h__