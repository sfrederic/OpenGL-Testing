#include "CGECGALTetrahedralMeshGenerator.h"

namespace CGEngine
{

	CCGALConvexTetrahedralMeshGenerator::CCGALConvexTetrahedralMeshGenerator(const std::string& _fileName)
	{
		m_FileName = _fileName;
	}

	CCGALConvexTetrahedralMeshGenerator::~CCGALConvexTetrahedralMeshGenerator()
	{

	}

	bool CCGALConvexTetrahedralMeshGenerator::VMakeObject(bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace)
	{
		if (m_FileName.empty())
		{
			std::cerr << BOOST_CURRENT_FUNCTION << ": No file given to load. Please give a valid file name to load 3D data from.\n";
			return false;
		}
		std::ifstream ifs(m_FileName.c_str());
		Polyhedron_3 polyhedron;
		ifs >> polyhedron;
		if (ifs.fail())
		{
			std::cerr << BOOST_CURRENT_FUNCTION << ": Failed to load file "<< m_FileName << "!\n";
			return false;
		}
		if (!polyhedron.is_pure_triangle())
		{
			std::cerr << BOOST_CURRENT_FUNCTION << ": Input mesh must be a triangular mesh!\n";
			return false;
		}

		m_TetMesh = Triangulation3D(polyhedron.points_begin(), polyhedron.points_end());
		//TetMesh: setup cell indices
		GLuint cell_indices = 0;
		for (auto cell_it = m_TetMesh.all_cells_begin(); cell_it != m_TetMesh.all_cells_end(); ++cell_it)
			cell_it->info() = cell_indices++;
		
		m_face_indices.clear();
		m_vertices.clear();
		m_normals.clear();
		m_texcoords.clear();
		m_binormals.clear();
		m_tangents.clear();

		//vertices
		m_vertices.reserve(m_TetMesh.number_of_vertices());
		GLuint vertex_indices = 0;
		for (auto v_it = m_TetMesh.finite_vertices_begin(); v_it != m_TetMesh.finite_vertices_end(); ++v_it)
		{
			v_it->info() = vertex_indices++;
			Point p = v_it->point();
			m_vertices.push_back(Vec3(static_cast<float>(p.x()), static_cast<float>(p.y()), static_cast<float>(p.z())));
		}

		//face indices
		m_face_indices.reserve(3 * m_TetMesh.number_of_finite_facets());
		std::vector<std::vector<std::uint8_t>> cell_adjacency_matrix(m_TetMesh.number_of_cells(),
																	std::vector<std::uint8_t>(m_TetMesh.number_of_cells(), 0));
		for (auto cell_it = m_TetMesh.finite_cells_begin(); cell_it != m_TetMesh.finite_cells_end(); ++cell_it)
		{
			GLuint c1 = cell_it->info();
			for (int i = 0; i < 4; ++i)
			{
				GLuint c2 = cell_it->neighbor(i)->info();
				if (cell_adjacency_matrix[c1][c2] > 0)
					continue;
				else
				{
					cell_adjacency_matrix[c1][c2] = 1;
					cell_adjacency_matrix[c2][c1] = 1;
					m_face_indices.push_back(cell_it->vertex(m_TetMesh.vertex_triple_index(i, 0))->info());
					m_face_indices.push_back(cell_it->vertex(m_TetMesh.vertex_triple_index(i, 1))->info());
					m_face_indices.push_back(cell_it->vertex(m_TetMesh.vertex_triple_index(i, 2))->info());
				}
			}
		}

		if (_compute_normals)
		{
			make_normals();
		}
		if (_compute_texcoords)
		{
			make_texcoords();
		}
		if (_compute_tangentspace)
		{
			make_tangentspace();
		}

		return true;
	}

	void CCGALConvexTetrahedralMeshGenerator::make_normals()
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void CCGALConvexTetrahedralMeshGenerator::make_texcoords()
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void CCGALConvexTetrahedralMeshGenerator::make_tangentspace()
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

/************************************************************************/
/* Multi-domain tetrahedral meshes
/************************************************************************/

	CCGALMultiDomainTetrahedralMeshGenerator::CCGALMultiDomainTetrahedralMeshGenerator(const std::string& _SurfaceMeshFilename,
		const Mesh_criteria_3& _MeshCriteria)
	{
		m_FileName = _SurfaceMeshFilename;
		m_MeshCriteria = _MeshCriteria;
	}

	CCGALMultiDomainTetrahedralMeshGenerator::CCGALMultiDomainTetrahedralMeshGenerator(const std::string& _SurfaceMeshFilename, 
		const std::string& _MeshCriteriaConfigFile)
	{
		using namespace CGAL::parameters;
		m_FileName = _SurfaceMeshFilename;
		std::map<std::string, double> parameter_value_map;
		parameter_value_map["edge_size"]				= DBL_MAX;
		parameter_value_map["facet_angle"]				= 0.0;
		parameter_value_map["facet_size"]				= 0.0;
		parameter_value_map["facet_distance"]			= 0.0;
		parameter_value_map["cell_radius_edge_ratio"]	= 0.0;
		parameter_value_map["cell_size"]				= 0.0;
		//read config
		ptree pt;
		boost::property_tree::json_parser::read_json(_MeshCriteriaConfigFile, pt);
		
		BOOST_FOREACH(ptree::value_type& criteria, pt.get_child("MeshCriteria"))
		{
			if (parameter_value_map.count(criteria.first) > 0)
			{
				parameter_value_map[criteria.first] = criteria.second.get_value<double>();
			}
			else
			{
				throw std::string( std::string("Unknown parameter key: ") + std::string(criteria.first) );
			}
		}
		m_MeshCriteria = Mesh_criteria_3(
			edge_size = parameter_value_map["edge_size"],
			facet_angle = parameter_value_map["facet_angle"],
			facet_size = parameter_value_map["facet_size"],
			facet_distance = parameter_value_map["facet_distance"],
			cell_radius_edge_ratio = parameter_value_map["cell_radius_edge_ratio"],
			cell_size = parameter_value_map["cell_size"]
		);
	}

	CCGALMultiDomainTetrahedralMeshGenerator::~CCGALMultiDomainTetrahedralMeshGenerator()
	{

	}

	bool CCGALMultiDomainTetrahedralMeshGenerator::VMakeObject(bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace)
	{
		if (m_FileName.empty())
		{
			std::cerr << BOOST_CURRENT_FUNCTION << ": No file given to load. Please give a valid file name to load 3D data from.\n";
			return false;
		}
		std::ifstream ifs(m_FileName.c_str());
		Polyhedron_3 polyhedron;
		ifs >> polyhedron;
		if (ifs.fail())
		{
			std::cerr << BOOST_CURRENT_FUNCTION << ": Failed to load file " << m_FileName << "!\n";
			return false;
		}
		if (!polyhedron.is_pure_triangle())
		{
			std::cerr << BOOST_CURRENT_FUNCTION << ": Input mesh must be a triangular mesh!\n";
			return false;
		}
		using namespace CGAL::parameters;
		// Create domain
		Polyhedral_mesh_domain_3 domain(polyhedron);

		// Mesh generation
		std::cout << "Mesh generation start...\n";
		m_MDTetMesh = CGAL::make_mesh_3<C3t3>(domain, m_MeshCriteria, no_perturb(), no_exude());

		m_face_indices.clear();
		m_vertices.clear();
		m_normals.clear();
		m_texcoords.clear();
		m_binormals.clear();
		m_tangents.clear();

		setup_vertices_and_faces(m_MDTetMesh);

		if (_compute_normals)
		{
			make_normals();
		}
		if (_compute_texcoords)
		{
			make_texcoords();
		}
		if (_compute_tangentspace)
		{
			make_tangentspace();
		}

		return true;
	}

	void CCGALMultiDomainTetrahedralMeshGenerator::make_normals()
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void CCGALMultiDomainTetrahedralMeshGenerator::make_texcoords()
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void CCGALMultiDomainTetrahedralMeshGenerator::make_tangentspace()
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void CCGALMultiDomainTetrahedralMeshGenerator::setup_vertices_and_faces(const C3t3& c3t3)
	{
		GLuint counter = 0;
		CGAL::Handle_hash_function hash;
		std::map<std::size_t, GLuint> hash_index_map;

		auto T = c3t3.triangulation();

		//vertices
		m_vertices.reserve(T.number_of_vertices());
		for (auto v_it = c3t3.triangulation().finite_vertices_begin();
			v_it != c3t3.triangulation().finite_vertices_end();
			++v_it)
		{
			hash_index_map[hash(v_it)] = counter++;
			auto p = v_it->point();
			m_vertices.push_back(Vec3(static_cast<float>(p.x()), static_cast<float>(p.y()), static_cast<float>(p.z())));
		}
		assert(counter == T.number_of_vertices());

		std::map<std::size_t, bool> cell_finished;
		//face indices
		m_face_indices.reserve(3 * T.number_of_finite_facets());
		for (auto cell_it = c3t3.cells_in_complex_begin(); cell_it != c3t3.cells_in_complex_end(); ++cell_it)
		{
			for (int i = 0; i < 4; ++i)
			{
				if (cell_finished[hash(cell_it->neighbor(i))])
					continue;
				m_face_indices.push_back(hash_index_map[hash(cell_it->vertex(T.vertex_triple_index(i, 0)))]);
				m_face_indices.push_back(hash_index_map[hash(cell_it->vertex(T.vertex_triple_index(i, 1)))]);
				m_face_indices.push_back(hash_index_map[hash(cell_it->vertex(T.vertex_triple_index(i, 2)))]);
			}
			cell_finished[hash(cell_it)] = true;
		}
	}

} //End namespace