#include "CGEMesh_OGL2.h"
#include "shaders.h"
#include "CGETransformation.h"
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include "app_setup.h"
#include "bounding_sphere.h"
#include "CGESphere.h"
#include <glm/gtx/string_cast.hpp>

int main(void)
{
	std::cout << "Bounding sphere testing..\n";
	BoundingSphere bs;
	Vec3DVec points;

	//////////////////////////////////////////////////////////////////////////
	GLFWwindow* window;

	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	window = glfwCreateWindow(640, 480, "OpenGL Testing", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwSetKeyCallback(window, key_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetFramebufferSizeCallback(window, frambuffer_resize_callback);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	int width(0), height(0);
	glfwGetFramebufferSize(window, &width, &height);

	glewExperimental = GL_TRUE;
	GLenum initStatus = glewInit();
	if (initStatus != GLEW_OK) 
	{
		std::cerr << "Unable to initialize GLEW!\n";
		exit(EXIT_FAILURE);
	}

	//////////////////////////////////////////////////////////////////////////

	CGEngine::CSphere sphere;
	CGEngine::CMeshOGL2 suzanne;
	CGEngine::CMeshOGL2 bunny;

	if( !suzanne.LoadMesh( data_path + std::string("suzanne.obj")) )
		exit(EXIT_FAILURE);
	if (!bunny.LoadMesh(data_path + std::string("Bunny.obj")))
		exit(EXIT_FAILURE);

	suzanne.VGetVerticesFromGPUBuffer(points);

	ComputeBoundingSphereRitter(points, bs);

	std::cout << "Center :" << glm::to_string(bs.m_Center) << ", radius: " << bs.m_Radius << std::endl;
	points.clear();

	CGEngine::CSphereGeneratorShPtr p_spgen(new CGEngine::CSphereGenerator(CGEngine::CSphereGenerator::SUBDIVISION_NORMALIZATION,
		bs.m_Radius, 6, std::shared_ptr<Vec3>(nullptr)));
	p_spgen->VMakeObject(true, false, false);

	sphere.VInitObject(p_spgen);

	//////////////////////////////////////////////////////////////////////////

	CGEngine::ShaderProgram shader;
	CGEngine::ShaderProgram wireframe_shader;
	try
	{
		shader.VertexShaderFromSource(vertex_shader_text);
		shader.FragmentShaderFromSource(fragment_shader_text);
		shader.LinkProgram();

		wireframe_shader.VertexShaderFromSource(wireframe_vs);
		wireframe_shader.GeometryShaderFromSource(wireframe_gs);
		wireframe_shader.FragmentShaderFromSource(wireframe_fs);
		wireframe_shader.LinkProgram();
	}
	catch (CGEngine::ShaderError& _e)
	{
		std::cerr << _e.what() << "\n";
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	GLint tex_loc	= shader.GetUniformVarLocation("tex");
	GLint light_loc = shader.GetUniformVarLocation("lightDir");
	GLint proj_loc	= shader.GetUniformVarLocation("projection");
	GLint nm_loc	= shader.GetUniformVarLocation("normalMatrix");
	GLint mm_loc	= shader.GetUniformVarLocation("model_matrix");
	GLint view_loc	= shader.GetUniformVarLocation("view_matrix");

	GLint ws_proj_loc	= wireframe_shader.GetUniformVarLocation("projection");
	GLint ws_mm_loc		= wireframe_shader.GetUniformVarLocation("model_matrix");
	GLint ws_view_loc	= wireframe_shader.GetUniformVarLocation("view_matrix");

	//////////////////////////////////////////////////////////////////////////
	CGEngine::Vec3 lightDir(-1.0, 0.0, -1.0); //in world space
	CGEngine::CMatrixStack ModelTransformStack;
	CGEngine::Matrix4x4 projection = CGEngine::Matrix4x4(glm::perspective(45.0, (GLdouble)width / (GLdouble)height, 1.0, 100.0));
	CGEngine::Matrix4x4 view(1.0);
	//////////////////////////////////////////////////////////////////////////

	glViewport(0, 0, width, height);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);

	double lastTime = glfwGetTime();
	int nbFrames = 0;

	while (!glfwWindowShouldClose(window))
	{
		glfwGetFramebufferSize(window, &width, &height);
		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if ( currentTime - lastTime >= 1.0 )
		{ 
			// If last prinf() was more than 1 sec ago printf and reset timer
			double spf = 1000.0/double(nbFrames);
			int fps = static_cast<int>(1.0/spf*1000.0);
			std::string t("OpenGL Testing, ");
			std::string title = std::string("SPF: ") + std::to_string((long double)spf) + std::string(" ms/frame. FPS: ") + std::to_string((long double)fps);
			glfwSetWindowTitle(window, (t+title).c_str());
			//printf("SPF: %f ms/frame. FPS: %d \n", spf, fps);
			nbFrames = 0;
			lastTime += 1.0;
		}
		glClearDepth(1.0f);
		glViewport(0, 0, width, height);
		glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		ModelTransformStack.LoadIdentity();
		ModelTransformStack.Translate(CGEngine::Vec3(0.0, 0.0, UserInputTransformData::GetInstance()->translate_value));
		ModelTransformStack.Rotate(static_cast<GLfloat>(glfwGetTime())*15.0f, CGEngine::Vec3(0.0f, 1.0f, 0.0f));
		CGEngine::Matrix3x3 nm(1.0);
		CGEngine::CMatrixStack::ComputeNormalMatrix(nm, ModelTransformStack.getCurrentTransform(), view);

		//shader.StartProgram();
		wireframe_shader.StartProgram();

		glUniformMatrix4fv(ws_mm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
		glUniformMatrix4fv(ws_proj_loc, 1, false, glm::value_ptr(projection));
		glUniformMatrix4fv(ws_view_loc, 1, false, glm::value_ptr(view));

/*
		glUniform1i(tex_loc, 0);
		glUniform3fv(light_loc, 1, glm::value_ptr(lightDir));
		glUniformMatrix4fv(mm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
		glUniformMatrix3fv(nm_loc, 1, false, glm::value_ptr(nm));
		glUniformMatrix4fv(proj_loc, 1, false, glm::value_ptr(projection));
		glUniformMatrix4fv(view_loc, 1, false, glm::value_ptr(view));*/

		bunny.VOnDraw();

		wireframe_shader.StopProgram();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}