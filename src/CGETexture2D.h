/****************************************************************************************

  Class for 2D-Textures with OpenGL

*****************************************************************************************/

#include <boost/smart_ptr/shared_ptr.hpp>
#include "CGETextureBase.h"

#ifndef CGETEXTURE2D_H
#define CGETEXTURE2D_H

#pragma warning(push)
#if (_MSC_VER > 0)
#pragma warning(disable : 4003)
#endif

namespace CGEngine {

//Class Header and typedefs
class CGLTexture2D;
typedef std::shared_ptr<CGLTexture2D> GLTexture2DShPtr  ;

//Class for 2D-Textures with OpenGL
class CGLTexture2D : public CGETextureBase {
private:
    CGLTexture2D(const CGLTexture2D&) {}
	CGLTexture2D& operator=(const CGLTexture2D&) {}

	void SetTexParami(const GLenum _name, const GLint _param); //soon to be deprecated
	void SetTexParamf(const GLenum _name, const GLfloat _param); //soon to be deprecated

protected:
    int m_Width   ;
    int m_Height  ;

public:
    CGLTexture2D() : CGETextureBase() {
        m_Width  = 0 ;
        m_Height = 0 ;
    }

	CGLTexture2D(CGLTexture2D&& _rhs) : CGETextureBase(), m_Width(_rhs.m_Width), m_Height(_rhs.m_Height)
	{
		m_TextureID = _rhs.m_TextureID;

		_rhs.m_TextureID = 0;
		_rhs.m_Width = 0;
		_rhs.m_Height = 0;
	}

    virtual ~CGLTexture2D() {
		VReleaseTexture();
    }

	CGLTexture2D& operator=(CGLTexture2D&& _rhs)
	{
		if (this != &_rhs)
		{
			VReleaseTexture();
			std::swap(m_TextureID, _rhs.m_TextureID);
			std::swap(m_Width, _rhs.m_Width);
			std::swap(m_Height, _rhs.m_Height);
		}
		return *this;
	}

    virtual bool VLoadTexture(const std::string& _fileName, 
		GLenum _pixel_store_param = GL_UNPACK_ALIGNMENT,
		GLint _pixel_store_value = 1) ;

	inline void Bind(const GLenum _ActiveTextureUnit)
	{
		glActiveTexture(_ActiveTextureUnit);
		glBindTexture(GL_TEXTURE_2D, m_TextureID);
	}

	BOOST_PARAMETER_MEMBER_FUNCTION(
		(void), SetMipMapTextureParameter_fi,
		GLTextureParameters::tag,
		(required
			(texture_target, (GLenum)))
		(optional
			(texture_base_level,(GLint), -1)
			(texture_max_level, (GLint), -1)
			(texture_lod_bias_f,(GLfloat), 0.0f)
			(texture_min_lod_f, (GLfloat), -1000.f)
			(texture_max_lod_f, (GLfloat), 1000.0f)
			(texture_min_filter,(GLint), 0)
			(texture_mag_filter,(GLint), 0))
	)
	{
		if (GLEW_VERSION_4_5)
		{
			if (texture_base_level >= 0)
				glTextureParameteri(texture_target, GL_TEXTURE_BASE_LEVEL, texture_base_level);
			if (texture_max_level >= 0)
				glTextureParameteri(texture_target, GL_TEXTURE_MAX_LEVEL, texture_base_level);
			if (texture_min_filter)
				glTexParameteri(texture_target, GL_TEXTURE_MIN_FILTER, texture_min_filter);
			if (texture_mag_filter)
				glTextureParameteri(texture_target, GL_TEXTURE_MAG_FILTER, texture_mag_filter);

			glTextureParameterf(texture_target, GL_TEXTURE_LOD_BIAS, texture_lod_bias_f);
			glTextureParameterf(texture_target, GL_TEXTURE_MIN_LOD, texture_min_lod_f);
			glTextureParameterf(texture_target, GL_TEXTURE_MAX_LOD, texture_max_lod_f);
		}
	}

    //Getter-Functions
    int GetWidth() const {
        return m_Width ;
    }

    int GetHeight() const {
        return m_Height ;
    }


	virtual GLenum VGetTextureTarget() override;


	virtual bool VLoadTexture(GLsizei _width, GLsizei _height, GLenum _internal_format, GLenum _tex_format, GLenum _type, GLvoid* _data) override;

};

}   //End Namespace

#pragma warning(pop)
#endif // CGETEXTURE2D_H
