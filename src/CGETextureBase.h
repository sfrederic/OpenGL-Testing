/**************************************************************************************************

  Abstract Base-Class for Textures with QT and OpenGL

***************************************************************************************************/

#ifndef __GLEW_H__
#define GLEW_STATIC
#include <GL/glew.h>
#endif
#include <boost/parameter.hpp>
//#include <boost/parameter/keyword.hpp>
#include <string>
#include <memory>
#include "images.h"

#ifndef CGETextureBase_h__
#define CGETextureBase_h__

#pragma warning(push)
#if (_MSC_VER > 0)
#pragma warning(disable : 4003)
#endif

namespace CGEngine {

	/*
	* Declare parameters for the SetTexParama functions
	**/
	namespace GLTextureParameters
	{
		BOOST_PARAMETER_NAME(texture_target)
		BOOST_PARAMETER_NAME(texture_object)

		BOOST_PARAMETER_NAME(depth_stencil_texture_mode)
		BOOST_PARAMETER_NAME(texture_base_level)
		BOOST_PARAMETER_NAME(texture_compare_func)
		BOOST_PARAMETER_NAME(texture_compare_mode)
		BOOST_PARAMETER_NAME(texture_lod_bias_f) //floating point value
		BOOST_PARAMETER_NAME(texture_min_filter)
		BOOST_PARAMETER_NAME(texture_mag_filter)
		BOOST_PARAMETER_NAME(texture_min_lod_f) //floating point value
		BOOST_PARAMETER_NAME(texture_max_lod_f) //floating point value
		BOOST_PARAMETER_NAME(texture_max_level)
		BOOST_PARAMETER_NAME(texture_swizzle_r)
		BOOST_PARAMETER_NAME(texture_swizzle_g)
		BOOST_PARAMETER_NAME(texture_swizzle_b)
		BOOST_PARAMETER_NAME(texture_swizzle_a)
		BOOST_PARAMETER_NAME(texture_wrap_s)
		BOOST_PARAMETER_NAME(texture_wrap_t)
		BOOST_PARAMETER_NAME(texture_wrap_r)

		BOOST_PARAMETER_NAME(texture_border_color)
		BOOST_PARAMETER_NAME(texture_swizzle_rgba)
	}

class CGETextureBase 
{
protected:
    GLuint m_TextureID ;

    CGETextureBase() 
    {
        m_TextureID = 0;
    }

	CGETextureBase(const CGETextureBase&){}

public:
    virtual ~CGETextureBase() {}

    virtual bool VLoadTexture(const std::string& _fileName, GLenum _pixel_store_param, GLint _pixel_store_value) = 0 ;

	virtual bool VLoadTexture(GLsizei _width, GLsizei _height, GLenum _internal_format, GLenum _tex_format, GLenum _type, GLvoid* _data) = 0;

	virtual void VReleaseTexture()
	{
		if (m_TextureID)
			glDeleteTextures(1, &m_TextureID);
		m_TextureID = 0;
	}

	//Returns the OpenGL texture target value for the specific texture class (e.g GL_TEXTURE_2D, GL_TEXTURE_CUBE_MAP, etc.)
	virtual GLenum VGetTextureTarget() = 0;

	//Set texture parameters globally. Texture must be bound to current context.
	BOOST_PARAMETER_MEMBER_FUNCTION(
		(void), static SetSwizzleTexParameter_i,
		GLTextureParameters::tag,
		(required
			(texture_target, (GLenum)))
		(optional
			(texture_swizzle_r, (GLint), -1)
			(texture_swizzle_g, (GLint), -1)
			(texture_swizzle_b, (GLint), -1)
			(texture_swizzle_a, (GLint), -1)
			(texture_swizzle_rgba, (GLint*), nullptr))
	)
	{
		if (texture_swizzle_r)
			glTexParameteri(texture_target, GL_TEXTURE_SWIZZLE_R, texture_swizzle_r);
		if (texture_swizzle_g)
			glTexParameteri(texture_target, GL_TEXTURE_SWIZZLE_G, texture_swizzle_g);
		if (texture_swizzle_b)
			glTexParameteri(texture_target, GL_TEXTURE_SWIZZLE_B, texture_swizzle_b);
		if (texture_swizzle_rgba)
			glTexParameteri(texture_target, GL_TEXTURE_SWIZZLE_RGBA, texture_swizzle_rgba);
	}

	BOOST_PARAMETER_MEMBER_FUNCTION(
		(void), static SetMipMapTexParameter_fi,
		GLTextureParameters::tag,
		(required
			(texture_target, (GLenum)))
		(optional
			(texture_base_level,(GLint), -1)
			(texture_max_level,	(GLint), -1)
			(texture_lod_bias_f,(GLfloat), 0.0f)
			(texture_min_lod_f,	(GLfloat), -1000.f)
			(texture_max_lod_f,	(GLfloat), 1000.0f)
			(texture_min_filter,(GLint), 0)
			(texture_mag_filter,(GLint), 0))
	)
	{
		if ((texture_target == GL_TEXTURE_RECTANGLE) || (texture_target == GL_TEXTURE_RECTANGLE_ARB) ||
			(texture_target == GL_TEXTURE_RECTANGLE_EXT) || (texture_target == GL_TEXTURE_RECTANGLE_NV))
			throw std::string("Mipmapping not possible for rectangle textures.");

		if (texture_base_level >= 0)
			glTexParameteri(texture_target, GL_TEXTURE_BASE_LEVEL, texture_base_level);
		if (texture_max_level >= 0)
			glTexParameteri(texture_target, GL_TEXTURE_MAX_LEVEL, texture_base_level);
		if (texture_min_filter)
			glTexParameteri(texture_target, GL_TEXTURE_MIN_FILTER, texture_min_filter);
		if (texture_mag_filter)
			glTexParameteri(texture_target, GL_TEXTURE_MAG_FILTER, texture_mag_filter);

		glTexParameterf(texture_target, GL_TEXTURE_LOD_BIAS, texture_lod_bias_f);
		glTexParameterf(texture_target, GL_TEXTURE_MIN_LOD, texture_min_lod_f);
		glTexParameterf(texture_target, GL_TEXTURE_MAX_LOD, texture_max_lod_f);
	}

	BOOST_PARAMETER_MEMBER_FUNCTION(
		(void), static SetWrapTexParameter_i,
		GLTextureParameters::tag,
		(required
			(texture_target, (GLenum)))
		(optional
			(texture_wrap_s, (GLint), 0)
			(texture_wrap_t, (GLint), 0)
			(texture_wrap_r, (GLint), 0))
	)
	{
		if (texture_wrap_s)
			glTexParameteri(texture_target, GL_TEXTURE_WRAP_S, texture_wrap_s);
		if (texture_wrap_t)
			glTexParameteri(texture_target, GL_TEXTURE_WRAP_T, texture_wrap_t);
		if (texture_wrap_r)
			glTexParameteri(texture_target, GL_TEXTURE_WRAP_R, texture_wrap_r);
	}

	BOOST_PARAMETER_MEMBER_FUNCTION(
		(void), static SetTexParameter_i,
		GLTextureParameters::tag,
		(required
			(texture_target, (GLenum)))
		(optional
			(depth_stencil_texture_mode,(GLint), 0)
			(texture_border_color,		(GLint*), nullptr)
			(texture_compare_func,		(GLint), 0)
			(texture_compare_mode,		(GLint), 0))
	)
	{
		if (depth_stencil_texture_mode)
			glTexParameteri(texture_target, GL_DEPTH_STENCIL_TEXTURE_MODE, depth_stencil_texture_mode);
		if (texture_border_color)
			glTexParameteriv(texture_target, GL_TEXTURE_BORDER_COLOR, texture_border_color);
		if (texture_compare_func)
			glTexParameteri(texture_target, GL_TEXTURE_COMPARE_FUNC, texture_compare_func);
		if (texture_compare_mode)
			glTexParameteri(texture_target, GL_TEXTURE_COMPARE_MODE, texture_compare_mode);
	}

    //Getter-Functions

    GLuint GetTextureId() const
    {
        return m_TextureID ;
    }
};

}   //End Namespace
#pragma warning(pop)

#endif // CGETextureBase_h__