#include "CGEGeometry.h"

namespace CGEngine
{

	extern Vec3 GetTranslationPart( const Matrix4x4& _matrix )
	{
		return Vec3(_matrix[TRANSLATION_INDEX]);
	}

	extern Vec3 GetTranslationPart( const Matrix4x4* _matrix )
	{
		if (_matrix)
			return Vec3((*_matrix)[TRANSLATION_INDEX]);
		else
			return Vec3(0.0);
	}

	extern Matrix3x3 GetRotationPart( const Matrix4x4& _matrix )
	{
		return Matrix3x3(_matrix);
	}

	extern Matrix3x3 GetRotationPart( const Matrix4x4* _matrix )
	{
		if (_matrix)
			return Matrix3x3(*_matrix);
		else
			return Matrix3x3(1.0);
	}

}