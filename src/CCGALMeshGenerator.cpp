#include "CCGALMeshGenerator.h"

namespace CGEngine
{

	CCGALMeshGenerator::CCGALMeshGenerator(const std::string& _fileName) : m_FileName(_fileName)
	{
	}

	CCGALMeshGenerator::~CCGALMeshGenerator()
	{

	}

	bool CCGALMeshGenerator::VMakeObject(bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace)
	{
		if (GetFileName().empty() && !m_Mesh)
		{
			std::cerr << BOOST_CURRENT_FUNCTION << ": No file given to load. Please give a valid file name to load 3D data from.\n";
			return false;
		}
		if (!m_Mesh)
		{
			m_Mesh = std::unique_ptr<SurfaceMesh>(new SurfaceMesh);
			std::ifstream in(m_FileName);
			if (!read_off(in, *m_Mesh))
			{
				std::cerr << BOOST_CURRENT_FUNCTION <<": Failed to load " << m_FileName << "\n";
				return false;
			}
			std::cout << "Number of vertices loaded: " << m_Mesh->num_vertices() << std::endl;
			std::cout << "Number of faces loaded: " << m_Mesh->number_of_faces() << std::endl;
		}
		m_face_indices.clear();
		m_vertices.clear();
		m_normals.clear();
		m_texcoords.clear();
		m_binormals.clear();
		m_tangents.clear();

		m_vertices.reserve(m_Mesh->number_of_vertices());
		BOOST_FOREACH(SurfaceMesh::Vertex_index& vi, m_Mesh->vertices())
		{
			Point_3 p = m_Mesh->point(vi);
			m_vertices.push_back(Vec3( static_cast<float>(p.x()), static_cast<float>(p.y()), static_cast<float>(p.z())));
		}

		m_face_indices.reserve(3*m_Mesh->number_of_faces());
		BOOST_FOREACH(SurfaceMesh::Face_index& fi, m_Mesh->faces())
		{
			BOOST_FOREACH(SurfaceMesh::Vertex_index& vi, vertices_around_face(m_Mesh->halfedge(fi), *m_Mesh))
			{
				m_face_indices.push_back(static_cast<GLuint>(vi));
			}
		}

		if (_compute_normals)
		{
			make_normals();
		}
		if (_compute_texcoords)
		{
			make_texcoords();
		}
		if (_compute_tangentspace)
		{
			make_tangentspace();
		}
		/*//handle vertex attributes of cgal mesh
		std::vector<std::string> cgal_attribute_names = m_Mesh->properties<Vertex_handle>();
		if (!cgal_attribute_names.empty())
		{
			BOOST_FOREACH(std::string& attrib_name, cgal_attribute_names)
			{
				AddVertexAttribute(attrib_name)
			}
		}*/

		return true;
	}

	void CCGALMeshGenerator::make_normals()
	{
		m_normals.reserve(m_Mesh->num_vertices());
		SurfaceMesh::Property_map<SurfaceMesh::Vertex_index, Vector_3> vertex_normal;
		vertex_normal = (m_Mesh->add_property_map<SurfaceMesh::Vertex_index, Vector_3>("v:normal", Vector_3(0.0, 0.0, 0.0))).first;
		//compute normals
		CGAL::Polygon_mesh_processing::compute_vertex_normals(*m_Mesh, vertex_normal);

		BOOST_FOREACH(SurfaceMesh::Vertex_index& vi, m_Mesh->vertices())
		{
			m_normals.push_back(Vec3(static_cast<float>(vertex_normal[vi].x()),
				static_cast<float>(vertex_normal[vi].y()), 
				static_cast<float>(vertex_normal[vi].z())));
		}
	}

	void CCGALMeshGenerator::make_texcoords()
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void CCGALMeshGenerator::make_tangentspace()
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

}