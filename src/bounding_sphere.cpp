#include "bounding_sphere.h"

void ComputeBoundingSphereRitter( const Vec3DVec& _points, BoundingSphere& _boundingSphere )
{
	std::default_random_engine gen;
	std::uniform_int_distribution<size_t> uid(0, _points.size()-1);
	size_t start_index = uid(gen);
	//get random point a look for other point that is the furthest away
	const Vec3& p0 = _points[start_index];
	float max_dist = -1.0f;
	Vec3 p1(0.0f);
	BOOST_FOREACH(const Vec3& p, _points)
	{
		float dist = glm::distance(p0, p);
		if (dist > max_dist)
		{
			max_dist = dist;
			p1 = p;
		}
	}
	//initial guess for bounding sphere parameters
	Vec3 dir = glm::normalize( p1 - p0 );
	_boundingSphere.m_Center = p0 + ((max_dist/2.0f) * dir);
	_boundingSphere.m_Radius = max_dist/2.0f;

	//second pass
	float radius_sqrd = _boundingSphere.m_Radius * _boundingSphere.m_Radius;
	BOOST_FOREACH(const Vec3& p, _points)
	{
		float p_to_center2 = glm::distance2(_boundingSphere.m_Center, p);
		if (p_to_center2 > radius_sqrd)
		{
			float p_to_center = glm::sqrt(p_to_center2);
			_boundingSphere.m_Radius = (_boundingSphere.m_Radius + p_to_center)/2.0f;
			radius_sqrd = _boundingSphere.m_Radius * _boundingSphere.m_Radius;
			float a = _boundingSphere.m_Radius / p_to_center;
			float b = (p_to_center - _boundingSphere.m_Radius) / p_to_center;
			_boundingSphere.m_Center = a * _boundingSphere.m_Center + b * p;
		} 
		else
			continue;
	}
}
