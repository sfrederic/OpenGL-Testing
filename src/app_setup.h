#ifndef app_setup_h__
#define app_setup_h__

#include <memory>
#include <string>
#include <utility>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <chrono>

#include <glm/gtc/quaternion.hpp>

const std::string data_path("C:\\Users\\Frederic\\Entwicklung\\OpenGL_Testing\\Data\\");
const std::string shadercode_path("C:\\Users\\Frederic\\Entwicklung\\OpenGL_Testing\\src\\shaders\\");

static const char* wireframe_vs =
"uniform mat4 model_matrix;\n"
"uniform mat4 view_matrix;\n"
"uniform mat4 projection;\n"
"void main()\n"
"{\n"
"    gl_Position = projection * view_matrix * model_matrix * gl_Vertex;\n"
"}\n";

static const char* wireframe_gs =
"#version 150\n"
"layout(triangles) in;\n"
"layout(triangle_strip, max_vertices = 3) out\n;"
"out vec3 geom_outBarycentricCoord;\n"
"void main()\n"
"{\n"
	"gl_Position = gl_in[0].gl_Position;\n"
	"geom_outBarycentricCoord = vec3(1.0, 0.0, 0.0);\n"
	"EmitVertex();\n"
	"gl_Position = gl_in[1].gl_Position;\n"
	"geom_outBarycentricCoord = vec3(0.0, 1.0, 0.0);\n"
	"EmitVertex();\n"
	"gl_Position = gl_in[2].gl_Position;\n"
	"geom_outBarycentricCoord = vec3(0.0, 0.0, 1.0);\n"
	"EmitVertex();\n"
	"EndPrimitive();\n"
"}\n";

static const char* wireframe_fs =
"in vec3 geom_outBarycentricCoord;\n" 
"float edgeFactor(){\n"
"	vec3 d = fwidth(geom_outBarycentricCoord);\n"
"	vec3 a3 = smoothstep(vec3(0.0), d*1.5, geom_outBarycentricCoord);\n"
"	return min(min(a3.x, a3.y), a3.z);\n"
"}\n"
"void main()\n"
"{\n"
"	vec3 color = mix(vec3(0.0), vec3(0.5), edgeFactor());\n"
"	gl_FragColor = vec4(color, 1.0);\n"
"}\n";

static const char* tet_wireframe_fs =
"in vec3 geom_outBarycentricCoord;\n"
"float edgeFactor(){\n"
"	vec3 d = fwidth(geom_outBarycentricCoord);\n"
"	vec3 a3 = smoothstep(vec3(0.0), d*1.5, geom_outBarycentricCoord);\n"
"	return min(min(a3.x, a3.y), a3.z);\n"
"}\n"
"void main()\n"
"{\n"
"	vec4 color = vec4(0.0);\n"
"	float t = 1.0 - edgeFactor();\n"
"	color.a = t;\n"
"	gl_FragDepth = gl_FragCoord.z;\n"
"	if(t < 0.1){\n"
"		discard;\n"
"	}\n"
"	gl_FragColor = color;\n"
"}\n";

static const char* vertex_shader_text =
	"varying vec3 normal;\n"
	"uniform mat4 model_matrix;\n"
	"uniform mat4 view_matrix;\n"
	"uniform mat4 projection;\n"
	"uniform mat3 normalMatrix;\n"
	"void main()\n"
	"{\n"
	"    normal = normalize(normalMatrix * gl_Normal);\n"
	"	 gl_TexCoord[0] = gl_MultiTexCoord0;\n"
	"    gl_Position = projection * view_matrix * model_matrix * gl_Vertex;\n"
	"}\n";
static const char* fragment_shader_text =
	"varying vec3 normal;\n"
	"uniform sampler2D tex;\n"
	"uniform mat4 view_matrix;\n"
	"uniform vec3 lightDir;\n"
	"void main()\n"
	"{\n"
	"	float NdotL;\n"
	"	vec3 temp = texture2D(tex,gl_TexCoord[0].st).xyz;\n"
	"	vec4 color = vec4(temp*0.1, 1.0);\n"
	"	vec4 lightDir_viewspace = view_matrix * vec4(-lightDir, 0.0);\n"
	"	NdotL = max(dot(normalize(normal),vec3(lightDir_viewspace)),0.0);\n"
	"	if(NdotL>0.0){\n"
	"	color += texture2D(tex,gl_TexCoord[0].st)*NdotL;}\n"
	"	gl_FragColor = color;\n"
	"}\n";

static const char* vertex_shader_text2 =
	"varying vec3 normal;\n"
	"varying vec3 frag_pos;\n"
	"varying vec4 frag_pos_lightspace;\n"
	"uniform mat4 model_matrix;\n"
	"uniform mat4 view_matrix;\n"
	"uniform mat4 projection;\n"
	"uniform mat3 normalMatrix;\n"
	"uniform mat4 lightSpaceMatrix;\n"
	"void main()\n"
	"{\n"
	"    normal = normalize(normalMatrix * gl_Normal);\n"
	"	 frag_pos = vec3(model_matrix * gl_Vertex);\n"
	"	 frag_pos_lightspace = lightSpaceMatrix * vec4(frag_pos, 1.0);\n"
	"	 gl_TexCoord[0] = gl_MultiTexCoord0;\n"
	"    gl_Position = projection * view_matrix * model_matrix * gl_Vertex;\n"
	"}\n";

static const char* fragment_shader_text2 =
	"varying vec3 normal;\n"
	"varying vec3 frag_pos;\n"
	"varying vec4 frag_pos_lightspace;\n"
	"uniform sampler2D tex;\n"
	"uniform sampler2D shadowMap;\n"
	"uniform mat4 view_matrix;\n"
	"uniform vec3 lightDir;\n"
	"uniform vec2 shadowMapSize;\n"
	"float ShadowCalculation(in vec4 fragPosLightSpace, in float bias){\n"
	"	vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;\n"
	"	projCoords = projCoords * 0.5 + 0.5;\n"
	"	//float closestDepth = texture2D(shadowMap, projCoords.xy).r;\n"
	"	float currentDepth = projCoords.z;\n"
	"	float shadow = 0.0;\n"
	"	if(projCoords.z > 1.0){\n"
	"		shadow = 0.0;}\n"
	"	else{\n"
	"		vec2 texelsize = 1.0 / shadowMapSize;\n"
	"		for(int x=-1; x<=1; ++x){\n"
	"			for(int y =-1; y<=1; ++y){\n"
	"				float pcfDepth = texture2D(shadowMap, projCoords.xy + vec2(x, y) * texelsize).r;\n"
	"				shadow += (currentDepth-bias) > pcfDepth  ? 1.0 : 0.0;\n"
	"			}\n"
	"		}\n"
	"		shadow /= 9.0;"
	"	}\n"
	"	return shadow;\n}\n"
	"void main()\n"
	"{\n"
	"	float NdotL;\n"
	"	vec3 temp = texture2D(tex,gl_TexCoord[0].st).xyz;\n"
	"	vec4 color = vec4(temp*0.1, 1.0);\n"
	"	vec4 lightDir_viewspace = view_matrix * vec4(-lightDir, 0.0);\n"
	"	float bias = max(0.05 * (1.0 - dot(normalize(normal), vec3(lightDir_viewspace))), 0.005);\n"
	"	float shadow = ShadowCalculation(frag_pos_lightspace, bias);\n"
	"	NdotL = max(dot(normalize(normal),vec3(lightDir_viewspace)),0.0);\n"
	"	if(NdotL>0.0){\n"
	"	color += texture2D(tex,gl_TexCoord[0].st)*NdotL*(1.0-shadow);}\n"
	"	gl_FragColor = color;\n"
	"}\n";

static const char* fsq_vertex_shader =
	"#version 330\n "
	"layout(location = 0) in vec3 position;\n"
	"layout(location = 1) in vec2 texcoord;\n"
	"layout(location = 2) in vec3 normal;\n"
	"uniform mat4 ortho_projection;\n"
	"out vec2 texture_coordinate;\n"
	"void main()\n"
	"{\n"
	"	texture_coordinate = texcoord;\n"
	"	gl_Position = ortho_projection * vec4(position, 1.0);\n"
	"}\n";

static const char* fsq_fragment_shader =
	"#version 330\n"
	"layout(location = 0) out vec4 fragment_value;\n"
	"uniform sampler2DRect tex;\n"
	"in vec2 texture_coordinate;\n"
	"void main()\n"
	"{\n"
	"	vec2 texcoord = texture_coordinate * vec2(textureSize(tex));\n"
	"	fragment_value = texture(tex, texcoord) * 20.0;\n"
	"}\n";

//////////////////////////////////////////////////////////////////////////

typedef glm::highp_fquat Quaternion;
std::ostream& operator<<(std::ostream& _ostream, const Quaternion& _q)
{
	_ostream << "(" << _q.w << ", " << _q.x << ", " << _q.y << ", " << _q.z << ")";
	return _ostream;
}

std::ostream& operator<<(std::ostream& _ostream, const CGEngine::Vec3& _v)
{
	_ostream << "(" << _v.x << ", " << _v.y << ", " << _v.z << ")";
	return _ostream;
}

std::ostream& operator<<(std::ostream& _ostream, const CGEngine::iVec2& _v)
{
	_ostream << "(" << _v.x << ", " << _v.y << ")";
	return _ostream;
}

std::ostream& operator<<(std::ostream& _ostream, const CGEngine::ui32Vec4& _v)
{
	_ostream << "(" << _v.x << ", " << _v.y << ", " << _v.z << ", " << _v.w << ")";
	return _ostream;
}

std::ostream& operator<<(std::ostream& _ostream, const CGEngine::Vec4& _v)
{
	_ostream << "(" << _v.x << ", " << _v.y << ", " << _v.z << ", " << _v.w << ")";
	return _ostream;
}

/*
 *https://www.khronos.org/opengl/wiki/Object_Mouse_Trackball
 **/
class TrackballSystem
{
public:
	TrackballSystem() : m_trackballVec1(CGEngine::Vec3(0.0f)), m_trackballVec2(CGEngine::Vec3(0.0f))
	{
	}
	~TrackballSystem() {}

	friend class UserInputTransformData;

	inline CGEngine::Matrix4x4 GetRotationMatrix()
	{
		return glm::mat4_cast(m_rotationQuaternion);
	}

	inline void SwapV1V2()
	{
		std::swap(m_trackballVec1, m_trackballVec2);
	}

	inline void Clear()
	{
		m_trackballVec1 = Vec3(0.0f);
		m_trackballVec2 = Vec3(0.0f);
	}

	inline void MakeTrackballVec(float _x_ndc, float _y_ndc)
	{
		m_trackballVec2.x = _x_ndc;
		m_trackballVec2.y = _y_ndc;
		float sum_squares = _x_ndc*_x_ndc + _y_ndc*_y_ndc;
		if (sum_squares <= 0.5f) //sum_squares <= (r^2 / 2.0)
		{
			m_trackballVec2.z = std::sqrt(1.0f - sum_squares); //radius = 1.0f
		}
		else
		{
			m_trackballVec2.z = 0.5f / std::sqrt(sum_squares);
		}
		m_trackballVec2 = glm::normalize(m_trackballVec2);
		//std::cout << "m_trackballVec2:\t\t" << m_trackballVec2 << std::endl;
	}

	inline void MakeRotation()
	{		
		float angle = glm::acos(glm::dot(m_trackballVec1, m_trackballVec2));
		CGEngine::Vec3 N = glm::normalize( glm::cross(m_trackballVec1, m_trackballVec2) );
		m_rotationQuaternion = m_rotationQuaternion * glm::angleAxis(angle, N);
		//std::cout << "m_rotationQuaternion:\t" << m_rotationQuaternion << std::endl;
	}
protected:
	CGEngine::Vec3	m_trackballVec1;
	CGEngine::Vec3	m_trackballVec2;
	Quaternion		m_rotationQuaternion;
	
private:
};

class UserInputTransformData
{
public:
	static std::shared_ptr<UserInputTransformData> GetInstance()
	{
		if (!m_Instance)
		{
			m_Instance = std::shared_ptr<UserInputTransformData> (new UserInputTransformData());
			return m_Instance;
		} 
		else
		{
			return m_Instance;
		}
	}

	virtual ~UserInputTransformData()
	{
	}

	inline void SetViewportDimensions(float _xmin, float _ymin, float _xmax, float _ymax)
	{
		m_ViewportDimensions.x = _xmin; 
		m_ViewportDimensions.y = _ymin;
		m_ViewportDimensions.z = _xmax;
		m_ViewportDimensions.w = _ymax;
	}

	inline const CGEngine::Vec4& GetViewportDimensions() const
	{
		return m_ViewportDimensions;
	}

	inline CGEngine::Matrix4x4 GetTrackballRotationMatrix()
	{
		return trackBall.GetRotationMatrix();
	}

	float translate_value;
	float rotation_value_lr;
	float rotation_value_ud;

	TrackballSystem trackBall;

protected:
	static std::shared_ptr<UserInputTransformData> m_Instance;
#define VIEWPORT_XMIN 0
#define VIEWPORT_XMAX 2
#define VIEWPORT_YMIN 1
#define VIEWPORT_YMAX 3
	CGEngine::Vec4 m_ViewportDimensions;
private:
	UserInputTransformData() : translate_value(0.0f), rotation_value_lr(0.0f), rotation_value_ud(0.0f),
	m_ViewportDimensions(CGEngine::Vec4(0.0f))
	{}
};

std::shared_ptr<UserInputTransformData> UserInputTransformData::m_Instance = std::shared_ptr<UserInputTransformData>(nullptr);

//////////////////////////////////////////////////////////////////////////

static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	if (key == GLFW_KEY_KP_4 && action == GLFW_PRESS)
		UserInputTransformData::GetInstance()->rotation_value_lr -= 5.0f;
	if (key == GLFW_KEY_KP_6 && action == GLFW_PRESS)
		UserInputTransformData::GetInstance()->rotation_value_lr += 5.0f;
	if (key == GLFW_KEY_KP_8 && action == GLFW_PRESS)
		UserInputTransformData::GetInstance()->rotation_value_ud += 5.0f;
	if (key == GLFW_KEY_KP_2 && action == GLFW_PRESS)
		UserInputTransformData::GetInstance()->rotation_value_ud -= 5.0f;
	if (key == GLFW_KEY_KP_4 && action == GLFW_REPEAT)
		UserInputTransformData::GetInstance()->rotation_value_lr -= 5.0f;
	if (key == GLFW_KEY_KP_6 && action == GLFW_REPEAT)
		UserInputTransformData::GetInstance()->rotation_value_lr += 5.0f;
	if (key == GLFW_KEY_KP_8 && action == GLFW_REPEAT)
		UserInputTransformData::GetInstance()->rotation_value_ud += 5.0f;
	if (key == GLFW_KEY_KP_2 && action == GLFW_REPEAT)
		UserInputTransformData::GetInstance()->rotation_value_ud -= 5.0f;
}

static void frambuffer_resize_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

static void scroll_callback(GLFWwindow* window, double xoff, double yoff)
{
	UserInputTransformData::GetInstance()->translate_value -= static_cast<float>(yoff) * 0.5f;
}

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	{
		UserInputTransformData::GetInstance()->trackBall.SwapV1V2();

		const CGEngine::Vec4& view_port = UserInputTransformData::GetInstance()->GetViewportDimensions();
		ypos = view_port[VIEWPORT_YMAX] - ypos; //set origin at bottom left!
		float x_NDC = (static_cast<float>(xpos) - view_port[VIEWPORT_XMIN]) * 2.0f / (view_port[VIEWPORT_XMAX] - view_port[VIEWPORT_XMIN]) - 1.0f;
		float y_NDC = (static_cast<float>(ypos) - view_port[VIEWPORT_YMIN]) * 2.0f / (view_port[VIEWPORT_YMAX] - view_port[VIEWPORT_YMIN]) - 1.0f;

		UserInputTransformData::GetInstance()->trackBall.MakeTrackballVec(x_NDC, y_NDC);
		UserInputTransformData::GetInstance()->trackBall.MakeRotation();
	}
	else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE)
	{
		UserInputTransformData::GetInstance()->trackBall.Clear();
	}
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		const CGEngine::Vec4& view_port = UserInputTransformData::GetInstance()->GetViewportDimensions();
		ypos = view_port[VIEWPORT_YMAX] - ypos; //set origin at bottom left!

		float x_NDC = (static_cast<float>(xpos) - view_port[VIEWPORT_XMIN]) * 2.0f / (view_port[VIEWPORT_XMAX] - view_port[VIEWPORT_XMIN]) - 1.0f;
		float y_NDC = (static_cast<float>(ypos) - view_port[VIEWPORT_YMIN]) * 2.0f / (view_port[VIEWPORT_YMAX] - view_port[VIEWPORT_YMIN]) - 1.0f;

		UserInputTransformData::GetInstance()->trackBall.MakeTrackballVec(x_NDC, y_NDC);
	}
}

#endif // app_setup_h__
