#pragma once

#include "CGETextureBase.h"

namespace CGEngine
{
	class CGEBufferTexture : public CGEngine::CGETextureBase
	{
	public:
		CGEBufferTexture();
		~CGEBufferTexture();

		CGEBufferTexture(const CGEBufferTexture&) = delete;
		CGEBufferTexture& operator=(const CGEBufferTexture&) = delete;

		CGEBufferTexture(CGEBufferTexture&& _rhs) : CGETextureBase(), m_BufferObjectName(_rhs.m_BufferObjectName)
		{
			m_TextureID = _rhs.m_TextureID;
			_rhs.m_TextureID = 0;
			_rhs.m_BufferObjectName = 0;
		}

		CGEBufferTexture& operator=(CGEBufferTexture&& _rhs)
		{
			if (this != &_rhs)
			{
				VReleaseTexture();
				std::swap(m_TextureID, _rhs.m_TextureID);
				std::swap(m_BufferObjectName, _rhs.m_BufferObjectName);
			}
			return *this;
		}

		virtual bool VLoadTexture(const std::string& _fileName, GLenum _pixel_store_param, GLint _pixel_store_value) override;


		virtual bool VLoadTexture(GLsizei _width, GLsizei _height, GLenum _internal_format, GLenum _tex_format, GLenum _type, GLvoid* _data) override;


		virtual void VReleaseTexture() override;


		virtual GLenum VGetTextureTarget() override;

		inline void Bind(const GLenum _ActiveTextureUnit)
		{
			glActiveTexture(_ActiveTextureUnit);
			glBindTexture(GL_TEXTURE_BUFFER, m_TextureID);
		}

	protected:
		
	private:
		GLuint m_BufferObjectName;
	};

}//End namespace
