#include "CGEAttributes.h"

namespace CGEngine
{
	namespace Attributes
	{

		CAttributeStorageStrategy::CAttributeStorageStrategy()
		{

		}

		CAttributeStorageStrategy::CAttributeStorageStrategy(const CAttributeStorageStrategy& _rhs)
		{
			m_AttributeHeader = _rhs.m_AttributeHeader;
		}

		CAttributeStorageStrategy::~CAttributeStorageStrategy()
		{

		}

		void CAttributeStorageStrategy::AttributeBufferData(GLuint _bufferObject, GLsizeiptr _size, const GLvoid* _data, GLenum _usage)
		{
			if (_bufferObject == 0)
				throw std::string("Error while processing vertex attributes: Zero named buffer object!");

			glBindBuffer(GL_ARRAY_BUFFER, _bufferObject);
			glBufferData(GL_ARRAY_BUFFER, _size, _data, _usage);
			if (glGetError() != GL_NO_ERROR)
			{
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				throw std::string("Error while processing vertex attributes: Could not load attribute data to GPU!");
			}
			//glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		CFloatingPointAttributeStorage::CFloatingPointAttributeStorage(GLint _numComponents, GLsizei _stride, GLenum _concreteType) :
			CAttributeStorageStrategy()
		{
			if ( (_numComponents != GL_BGRA) && (_numComponents < 0 || _numComponents > 4))
			{
				throw std::string("AttributeStorage: Illegal number of components per attribute. Must range from 1 to 4 or set to GL_BGRA!");
			}
			if (_stride < 0)
			{
				throw std::string("AttributeStorage: Negative stride value!");
			}
			m_AttributeHeader.m_Normalized				= GL_FALSE;
			m_AttributeHeader.m_NumComponentsPerVertex	= _numComponents;
			m_AttributeHeader.m_Stride					= _stride;
			m_AttributeHeader.m_Type					= _concreteType;
		}

		CFloatingPointAttributeStorage::CFloatingPointAttributeStorage(const CFloatingPointAttributeStorage& _rhs) :
			CAttributeStorageStrategy(_rhs)
		{

		}

		CFloatingPointAttributeStorage::~CFloatingPointAttributeStorage()
		{

		}

		void CFloatingPointAttributeStorage::VAttributeStorage(GLuint _vertexArrayObject, GLuint _attribIndex, GLvoid* _data_offset)
		{
			if (_vertexArrayObject == 0)
				throw std::string("Error while processing vertex attributes: Zero named vertex array object!");

			//glBindVertexArray(_vertexArrayObject);
			glEnableVertexAttribArray(_attribIndex);
			glVertexAttribPointer(_attribIndex, m_AttributeHeader.m_NumComponentsPerVertex,
				m_AttributeHeader.m_Type, m_AttributeHeader.m_Normalized, m_AttributeHeader.m_Stride, _data_offset);
			if (glGetError() != GL_NO_ERROR)
			{
				glDisableVertexAttribArray(_attribIndex);
				throw std::string("Error while processing vertex attributes: Could not assign data to attribute index!");
			}
		}

		CNormalizedIntegerAttributeStorage::CNormalizedIntegerAttributeStorage(GLint _numComponents, GLsizei _stride, GLenum _concreteType) :
			CAttributeStorageStrategy()
		{
			if ((_numComponents != GL_BGRA) && (_numComponents < 0 || _numComponents > 4))
			{
				throw std::string("AttributeStorage: Illegal number of components per attribute. Must range from 1 to 4 or set to GL_BGRA!");
			}
			if (_stride < 0)
			{
				throw std::string("AttributeStorage: Negative stride value!");
			}
			m_AttributeHeader.m_Normalized = GL_TRUE;
			m_AttributeHeader.m_NumComponentsPerVertex = _numComponents;
			m_AttributeHeader.m_Stride = _stride;
			m_AttributeHeader.m_Type = _concreteType;
		}

		CNormalizedIntegerAttributeStorage::CNormalizedIntegerAttributeStorage(const CNormalizedIntegerAttributeStorage& _rhs) :
			CAttributeStorageStrategy(_rhs)
		{

		}

		CNormalizedIntegerAttributeStorage::~CNormalizedIntegerAttributeStorage()
		{

		}

		void CNormalizedIntegerAttributeStorage::VAttributeStorage(GLuint _vertexArrayObject, GLuint _attribIndex, GLvoid* _data_offset)
		{
			if (_vertexArrayObject == 0)
				throw std::string("Error while processing vertex attributes: Zero named vertex array object!");

			//glBindVertexArray(_vertexArrayObject);
			glEnableVertexAttribArray(_attribIndex);
			glVertexAttribPointer(_attribIndex, m_AttributeHeader.m_NumComponentsPerVertex,
				m_AttributeHeader.m_Type, m_AttributeHeader.m_Normalized, m_AttributeHeader.m_Stride, _data_offset);
			if (glGetError() != GL_NO_ERROR)
			{
				glDisableVertexAttribArray(_attribIndex);
				throw std::string("Error while processing vertex attributes: Could not assign data to attribute index!");
			}
		}

		CIntegerAttributeStorage::CIntegerAttributeStorage(GLint _numComponents, GLsizei _stride /*= 0*/, GLenum _concreteType /*= GL_INT*/) :
			CAttributeStorageStrategy()
		{
			if ((_numComponents != GL_BGRA) && (_numComponents < 0 || _numComponents > 4))
			{
				throw std::string("AttributeStorage: Illegal number of components per attribute. Must range from 1 to 4 or set to GL_BGRA!");
			}
			if (_stride < 0)
			{
				throw std::string("AttributeStorage: Negative stride value!");
			}
			m_AttributeHeader.m_Normalized = GL_FALSE;
			m_AttributeHeader.m_NumComponentsPerVertex = _numComponents;
			m_AttributeHeader.m_Stride = _stride;
			m_AttributeHeader.m_Type = _concreteType;
		}

		CIntegerAttributeStorage::CIntegerAttributeStorage(const CIntegerAttributeStorage& _rhs) :
			CAttributeStorageStrategy(_rhs)
		{

		}

		CIntegerAttributeStorage::~CIntegerAttributeStorage()
		{

		}

		void CIntegerAttributeStorage::VAttributeStorage(GLuint _vertexArrayObject, GLuint _attribIndex, GLvoid* _data_offset)
		{
			if (_vertexArrayObject == 0)
				throw std::string("Error while processing vertex attributes: Zero named vertex array object!");

			//glBindVertexArray(_vertexArrayObject);
			glEnableVertexAttribArray(_attribIndex);
			glVertexAttribIPointer(_attribIndex, m_AttributeHeader.m_NumComponentsPerVertex,
				m_AttributeHeader.m_Type, m_AttributeHeader.m_Stride, _data_offset);
			if (glGetError() != GL_NO_ERROR)
			{
				glDisableVertexAttribArray(_attribIndex);
				throw std::string("Error while processing vertex attributes: Could not assign data to attribute index!");
			}
		}

		CDoublePrecisionAttributeStorage::CDoublePrecisionAttributeStorage(GLint _numComponents, GLsizei _stride /*= 0*/)
		{
			if ((_numComponents != GL_BGRA) && (_numComponents < 0 || _numComponents > 4))
			{
				throw std::string("AttributeStorage: Illegal number of components per attribute. Must range from 1 to 4 or set to GL_BGRA!");
			}
			if (_stride < 0)
			{
				throw std::string("AttributeStorage: Negative stride value!");
			}
			if (!(GLEW_ARB_vertex_attrib_64bit || GLEW_EXT_vertex_attrib_64bit))
			{
				throw std::string("AttributeStorage: Double precision attributes not supported!");
			}
			m_AttributeHeader.m_Normalized = GL_FALSE;
			m_AttributeHeader.m_NumComponentsPerVertex = _numComponents;
			m_AttributeHeader.m_Stride = _stride;
			m_AttributeHeader.m_Type = GL_DOUBLE;
		}

		CDoublePrecisionAttributeStorage::CDoublePrecisionAttributeStorage(const CDoublePrecisionAttributeStorage& _rhs) :
			CAttributeStorageStrategy(_rhs)
		{

		}

		CDoublePrecisionAttributeStorage::~CDoublePrecisionAttributeStorage()
		{

		}

		void CDoublePrecisionAttributeStorage::VAttributeStorage(GLuint _vertexArrayObject, GLuint _attribIndex, GLvoid* _data_offset)
		{
			throw std::logic_error("The method or operation is not implemented.");
		}

	} //End Namespace Attributes

} //End Namespace CGEngine