
#include "CGETransformation.h"
#include "bounding_sphere.h"
#include "CGESphere.h"
#include "CGECGALTetrahedralMeshGenerator.h" //must be included before CCGALMeshGenerator!! (check header inclusion dependency)
#include "CCGALMeshGenerator.h"
#include "CGETranslucentObjects.h"
#include "CGEMesh_OGL2.h"
#include "shaders.h"
#include <GLFW/glfw3.h>
#include "app_setup.h"

typedef std::shared_ptr<CGEngine::CCGALConvexTetrahedralMeshGenerator> ConvexTetMeshGenPtr;
typedef std::shared_ptr<CGEngine::CCGALMultiDomainTetrahedralMeshGenerator> MDTetMeshGenPtr;

typedef std::chrono::high_resolution_clock highres_clock;

int main(void)
{
	std::cout << "Translucent objects... start.\n";

	//////////////////////////////////////////////////////////////////////////
	//GLFW setup
	
	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	window = glfwCreateWindow(1280, 720, "OpenGL Testing", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwSetKeyCallback(window, key_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetFramebufferSizeCallback(window, frambuffer_resize_callback);
	glfwSetCursorPosCallback(window, cursor_position_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	int width(0), height(0);
	glfwGetFramebufferSize(window, &width, &height);

	glewExperimental = GL_TRUE;
	GLenum initStatus = glewInit();
	if (initStatus != GLEW_OK)
	{
		std::cerr << "Unable to initialize GLEW!\n";
		exit(EXIT_FAILURE);
	}

	/*GLint num_extensions(0);
	glGetIntegerv(GL_NUM_EXTENSIONS, &num_extensions);
	std::cout << "OpenGL " << glGetString(GL_VERSION) << ", GLSL " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl
		<< "Number of extensions = " << num_extensions << std::endl;
	std::cout << "GL Renderer: " << glGetString(GL_RENDERER) << std::endl
		<< "GL Vendor: " << glGetString(GL_VENDOR) << std::endl;
	GLint maxtexsize;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxtexsize);
	std::cout << "Max texture size: " << maxtexsize << std::endl;*/

	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////

	CGEngine::ShaderProgram tetmesh_shader;
	CGEngine::ShaderProgram graph_shader;
	CGEngine::ShaderProgram light_pass_shader;
	CGEngine::ShaderProgram fsq_shader; //full screen quad rendering
	CGEngine::ShaderProgram translucency_shader;
	try
	{
		tetmesh_shader.VertexShaderFromSource(wireframe_vs);
		tetmesh_shader.GeometryShaderFromSource(wireframe_gs);
		tetmesh_shader.FragmentShaderFromSource(tet_wireframe_fs);
		tetmesh_shader.LinkProgram();

		graph_shader.LoadVertexShader((shadercode_path + std::string("graph_vertex_shader.glsl")).c_str());
		graph_shader.LoadFragmentShader((shadercode_path + std::string("graph_fragment_shader.glsl")).c_str());
		graph_shader.LinkProgram();

		light_pass_shader.LoadVertexShader((shadercode_path + std::string("light_pass.vert")).c_str());
		light_pass_shader.LoadFragmentShader((shadercode_path + std::string("light_pass.frag")).c_str());
		light_pass_shader.LinkProgram();

		fsq_shader.VertexShaderFromSource(fsq_vertex_shader);
		fsq_shader.FragmentShaderFromSource(fsq_fragment_shader);
		fsq_shader.LinkProgram();

		translucency_shader.LoadVertexShader((shadercode_path + std::string("translucent_objects_vs.glsl")).c_str());
		translucency_shader.LoadFragmentShader((shadercode_path + std::string("translucent_objects_fs.glsl")).c_str());
		translucency_shader.LinkProgram();
	}
	catch (CGEngine::ShaderError& _e)
	{
		std::cerr << _e.what() << "\n";
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	GLint ws_proj_loc2	= tetmesh_shader.GetUniformVarLocation("projection");
	GLint ws_mm_loc2	= tetmesh_shader.GetUniformVarLocation("model_matrix");
	GLint ws_view_loc2	= tetmesh_shader.GetUniformVarLocation("view_matrix");

	GLint gs_proj_loc	= graph_shader.GetUniformVarLocation("projection");
	GLint gs_nm_loc		= graph_shader.GetUniformVarLocation("model_matrix");
	GLint gs_view_loc	= graph_shader.GetUniformVarLocation("view_matrix");

	GLint lps_lsm_loc = light_pass_shader.GetUniformVarLocation("lightSpaceMatrix"); //view-projection matrix
	GLint lps_mtm_loc = light_pass_shader.GetUniformVarLocation("modelTransform"); //transform matrix
	GLint lps_nm_loc = light_pass_shader.GetUniformVarLocation("lightNormalMatrix");
	GLint lps_ld_loc = light_pass_shader.GetUniformVarLocation("light_direction");
	GLint lps_li_loc = light_pass_shader.GetUniformVarLocation("light_intensity");
	GLint lps_ior_loc = light_pass_shader.GetUniformVarLocation("ior_material");

	GLint fsq_mat4_loc = fsq_shader.GetUniformVarLocation("ortho_projection");
	GLint fsq_tex_loc = fsq_shader.GetUniformVarLocation("tex");

	GLint ts_mm_loc = translucency_shader.GetUniformVarLocation("modelMatrix");
	GLint ts_vm_loc = translucency_shader.GetUniformVarLocation("viewMatrix");
	GLint ts_pm_loc = translucency_shader.GetUniformVarLocation("projectionMatrix");
	GLint ts_nm_loc = translucency_shader.GetUniformVarLocation("normalMatrix");
	GLint ts_ldir_loc			= translucency_shader.GetUniformVarLocation("light_direction");
	GLint ts_lin_loc			= translucency_shader.GetUniformVarLocation("light_intensity");
	GLint ts_campos_loc			= translucency_shader.GetUniformVarLocation("camera_position");
	GLint ts_radfluxtex_loc		= translucency_shader.GetUniformVarLocation("radiantFluxTexture");
	GLint ts_radfluxcoords_loc	= translucency_shader.GetUniformVarLocation("radiantFluxTexCoords");
	GLint ts_ior_loc			= translucency_shader.GetUniformVarLocation("ior_material");
	GLint ts_tcrit_loc			= translucency_shader.GetUniformVarLocation("theta_crit");

	//////////////////////////////////////////////////////////////////////////
	CGEngine::Vec4 lightDir(0.0f, 0.0f, 1.0f, 0.0f);  //in world space, w = 0.0 for directional light, 1.0 for point light
	CGEngine::Vec3 lightIntensity(1.0f, 1.0f, 1.0f);
	CGEngine::CMatrixStack ModelTransformStack;
	CGEngine::Matrix4x4 projection = CGEngine::Matrix4x4(glm::perspective(50.0f, (GLfloat)width / (GLfloat)height, 0.1f, 500.0f));
	CGEngine::Matrix4x4 projection_multiview = CGEngine::Matrix4x4(glm::perspective(50.0f, (GLfloat)(width/2) / (GLfloat)(height/2), 0.1f, 500.0f));
	CGEngine::Matrix4x4 view(1.0);
	CGEngine::Vec3 cam_pos(0.0f);
	//Shadow mapping and light intensity computation setup
	const GLsizei light_map_width = 1024;
	const GLsizei light_map_height = 1024;
	CGEngine::Matrix4x4 light_view(1.0);
	light_view = glm::lookAt(CGEngine::Vec3(0.0, 0.0, -20.0),
		CGEngine::Vec3(0.0, 0.0, -20.0) + CGEngine::Vec3(lightDir),
		CGEngine::Vec3(0.0, 1.0, 0.0));
	CGEngine::Matrix4x4 lightSpaceMatrix =
		glm::ortho(-5.0f, 5.0f, -5.0f, 5.0f, 0.0f, 50.0f)
		* light_view;

	CGEngine::CGERectangleTexture LightIntensityMap, LightDepthMap;
	LightIntensityMap.MakeEmptyTexture(light_map_width, light_map_height, GL_RGBA32F, GL_RGBA, GL_FLOAT);
	LightDepthMap.MakeEmptyTexture(light_map_width, light_map_height, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_FLOAT);
	LightDepthMap.Bind(GL_TEXTURE0);
	using namespace CGEngine::GLTextureParameters;
	CGEngine::CGETextureBase::SetTexParameter_i(_texture_target = GL_TEXTURE_RECTANGLE, _texture_compare_mode = GL_COMPARE_R_TO_TEXTURE,
		_texture_compare_func = GL_LEQUAL);
	CGEngine::CGEFramebufferObject LightMapFBO; LightMapFBO.Initialize();
	LightMapFBO.AttachTexture2D(&LightIntensityMap, GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0);
	LightMapFBO.AttachTexture2D(&LightDepthMap, GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT);
	//////////////////////////////////////////////////////////////////////////

	//Convex tet mesh
	ConvexTetMeshGenPtr cgal_tetmeshgen = ConvexTetMeshGenPtr(
		new CGEngine::CCGALConvexTetrahedralMeshGenerator(data_path + std::string("dpyramid.off"))
	);
	if (!cgal_tetmeshgen->VMakeObject(false, false, false))
		exit(EXIT_FAILURE);
	
	CGEngine::CMesh dpyramid;
	if (!dpyramid.LoadMesh(cgal_tetmeshgen.get()))
		exit(EXIT_FAILURE);
	//preprocessing step
	CGEngine::CGETranslucentObjectsPreprocessingConvexObjects translucencyPreprocessing_co(cgal_tetmeshgen);
	translucencyPreprocessing_co.MakeQuadGraph();
	translucencyPreprocessing_co.ComputeNodeTextureCoordinates();

	//get surface mesh
	CGEngine::CCGALMeshGenerator surface_mesh_gen("");
	//translucencyPreprocessing_co.ExtractSurfaceRenderMesh(surface_mesh_gen, true, false, false);
	std::unique_ptr<CGEngine::CGEBufferTexture> fluxTexCoords =
		translucencyPreprocessing_co.ExtractSurfaceMesh(surface_mesh_gen, true, false, false);
	CGEngine::CMesh surface_mesh;
	surface_mesh.LoadMesh(&surface_mesh_gen);

	//get quad graph
	CGEngine::Vec3DVec graph_vertices;
	CGEngine::GLuintVec graph_indices;
	translucencyPreprocessing_co.GetNodePositions(graph_vertices, graph_indices);

	CGEngine::CMesh quad_graph(GL_LINES);
	quad_graph.LoadMesh(graph_vertices, graph_indices, CGEngine::Vec3DVec(0), CGEngine::Vec2DVec(0));

	CGEngine::DiffusionSolverConvexObjects diffusionsolver_co;
	diffusionsolver_co.SetupBuffers(translucencyPreprocessing_co);

	CGEngine::iVec2 v1, v2, tex_dim;
	translucencyPreprocessing_co.GetMaxNodeTexCoords(v1, v2, tex_dim);

	//Multi domain tet mesh
	/*MDTetMeshGenPtr cgal_mdtetmeshgen = MDTetMeshGenPtr( 
		new CGEngine::CCGALMultiDomainTetrahedralMeshGenerator(data_path + std::string("elephant.off"),
		data_path + std::string("mesh_3_criteria.json")));

	cgal_mdtetmeshgen->VMakeObject(false, false, false);

/ *
	CGEngine::CMeshOGL2 cow;
	if (!cow.LoadMesh(cgal_mdtetmeshgen.get()))
		exit(EXIT_FAILURE);* /

	CGEngine::CGETranslucentObjectPreprocessing topp(cgal_mdtetmeshgen);
	topp.MakeQuadGraph();
	topp.ComputeNodeTextureCoordinates();

	topp.GetMaxNodeTexCoords(max_tc_ns, max_tc_ni, tex_dims);
	std::cout << max_tc_ns << " " << max_tc_ni << std::endl;*/

	/*topp.GetNodePositions(graph_vertices, graph_indices);
	CGEngine::CMeshOGL2 cow_quad_graph(GL_LINES);
	cow_quad_graph.LoadMesh(graph_vertices, graph_indices, CGEngine::Vec3DVec(0), CGEngine::Vec2DVec(0));*/

	//////////////////////////////////////////////////////////////////////////
	UserInputTransformData::GetInstance()->SetViewportDimensions(0.0f, 0.0f, (float)width, (float)height);

	glEnable(GL_DEPTH_TEST);

	double lastTime = glfwGetTime();
	int nbFrames = 0;
	//bool b(true);

	CGEngine::CFullScreenQuad fsq;
	fsq.MakeFullscreenQuad(static_cast<float>(tex_dim.x), static_cast<float>(tex_dim.y));

	while (!glfwWindowShouldClose(window))
	{
		glfwGetFramebufferSize(window, &width, &height);
		UserInputTransformData::GetInstance()->SetViewportDimensions(0.0f, 0.0f, (float)width, (float)height);
		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if (currentTime - lastTime >= 1.0)
		{
			// If last prinf() was more than 1 sec ago printf and reset timer
			double spf = 1000.0 / double(nbFrames);
			int fps = static_cast<int>(1.0 / spf * 1000.0);
			std::string t("OpenGL Testing, ");
			std::string title = std::string("SPF: ") + std::to_string((long double)spf) + std::string(" ms/frame. FPS: ") + std::to_string((long double)fps);
			glfwSetWindowTitle(window, (t + title).c_str());
			//printf("SPF: %f ms/frame. FPS: %d \n", spf, fps);
			nbFrames = 0;
			lastTime += 1.0;
		}
		//////////////////////////////////////////////////////////////////////////
		// Light pass
		//////////////////////////////////////////////////////////////////////////

		LightMapFBO.BindBufferForWriting();
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		glClearDepth(1.0f);
		glViewport(0, 0, light_map_width, light_map_height);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);

		ModelTransformStack.LoadIdentity();
		ModelTransformStack.Translate(CGEngine::Vec3(0.0f, 0.0f, -10.0f));
		ModelTransformStack.Translate(CGEngine::Vec3(0.0, 0.0, UserInputTransformData::GetInstance()->translate_value));
		ModelTransformStack.Rotate(UserInputTransformData::GetInstance()->rotation_value_lr, CGEngine::Vec3(0.0f, 1.0f, 0.0f));
		ModelTransformStack.MultMatrix(UserInputTransformData::GetInstance()->GetTrackballRotationMatrix());
		CGEngine::Matrix3x3 lightNormalMat(1.0);
		CGEngine::CMatrixStack::ComputeNormalMatrix(lightNormalMat, ModelTransformStack.getCurrentTransform(), light_view);

		light_pass_shader.StartProgram();
		glUniformMatrix4fv(lps_mtm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
		glUniformMatrix4fv(lps_lsm_loc, 1, false, glm::value_ptr(lightSpaceMatrix));
		glUniformMatrix3fv(lps_nm_loc, 1, false, glm::value_ptr(lightNormalMat));
		glUniform4fv(lps_ld_loc, 1, glm::value_ptr(lightDir));
		glUniform3fv(lps_li_loc, 1, glm::value_ptr(lightIntensity));
		glUniform1f(lps_ior_loc, 1.667f);

		surface_mesh.VOnDraw();

		light_pass_shader.StopProgram();

		CGEngine::CGEFramebufferObject::Unbind();

		//if (b)
		{
			/*LightMapFBO.BindBufferForReading();
			glReadBuffer(GL_COLOR_ATTACHMENT0);
			ReadDepthTextureToFile(std::string("C://Users//Frederic//Entwicklung//quad_graph_depth.ppm"), light_map_width, light_map_height);
			ReadBufferFromGPUToFile(std::string("C://Users//Frederic//Entwicklung//quad_graph_color.ppm"), light_map_width, light_map_height);
			b = false;*/
			CGEngine::Matrix4x4 lightMVP = lightSpaceMatrix * ModelTransformStack.getCurrentTransform();
			diffusionsolver_co.ComputeIncomingLightOnSurfaceNodes(LightDepthMap, LightIntensityMap, lightMVP);
			diffusionsolver_co.Solve();
			//b = false;
		}

		//////////////////////////////////////////////////////////////////////////
		// Render pass
		//////////////////////////////////////////////////////////////////////////

		ModelTransformStack.LoadIdentity();
		ModelTransformStack.Translate(CGEngine::Vec3(0.0f, 0.0f, -10.0f));
		ModelTransformStack.Translate(CGEngine::Vec3(0.0, 0.0, UserInputTransformData::GetInstance()->translate_value));
		ModelTransformStack.Rotate(UserInputTransformData::GetInstance()->rotation_value_lr, CGEngine::Vec3(0.0f, 1.0f, 0.0f));
		ModelTransformStack.MultMatrix(UserInputTransformData::GetInstance()->GetTrackballRotationMatrix());
		CGEngine::Matrix3x3 nm(1.0);
		CGEngine::CMatrixStack::ComputeNormalMatrix(nm, ModelTransformStack.getCurrentTransform(), view);

		glClearDepth(1.0f);
		glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Tetrahedral mesh
		glViewport(0, height / 2, width / 2, height / 2);
		
		tetmesh_shader.StartProgram();

		glUniformMatrix4fv(ws_mm_loc2, 1, false, ModelTransformStack.getCurrentTransformPointer());
		glUniformMatrix4fv(ws_proj_loc2, 1, false, glm::value_ptr(projection_multiview));
		glUniformMatrix4fv(ws_view_loc2, 1, false, glm::value_ptr(view));

		dpyramid.VOnDraw();

		tetmesh_shader.StopProgram();

		//Quad graph
		glViewport(width / 2, height / 2, width / 2, height / 2);

		graph_shader.StartProgram();

		glUniformMatrix4fv(gs_nm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
		glUniformMatrix4fv(gs_proj_loc, 1, false, glm::value_ptr(projection_multiview));
		glUniformMatrix4fv(gs_view_loc, 1, false, glm::value_ptr(view));

		quad_graph.VOnDraw();

		graph_shader.StopProgram();

		//Solution of diffusion equation in texture space
		glViewport(0, 0, width / 2, height / 2);
		CGEngine::Matrix4x4 fsq_projection = glm::ortho(0.0f, static_cast<float>(tex_dim.x), 0.0f, static_cast<float>(tex_dim.y));
		diffusionsolver_co.GetRadiantFluxTexture().Bind(GL_TEXTURE0);

		fsq_shader.StartProgram();
		glUniform1i(fsq_tex_loc, 0);
		glUniformMatrix4fv(fsq_mat4_loc, 1, GL_FALSE, glm::value_ptr(fsq_projection));

		fsq.VOnDraw();

		fsq_shader.StopProgram();

		//Final result
		glViewport(width / 2, 0, width / 2, height / 2);
		fluxTexCoords->Bind(GL_TEXTURE1);

		translucency_shader.StartProgram();
		glUniformMatrix4fv(ts_mm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
		glUniformMatrix4fv(ts_pm_loc, 1, false, glm::value_ptr(projection_multiview));
		glUniformMatrix4fv(ts_vm_loc, 1, false, glm::value_ptr(view));
		glUniformMatrix3fv(ts_nm_loc, 1, false, glm::value_ptr(nm));
		glUniform3fv(ts_ldir_loc, 1, glm::value_ptr(lightDir));
		glUniform3fv(ts_lin_loc, 1, glm::value_ptr(lightIntensity));
		glUniform3fv(ts_campos_loc, 1, glm::value_ptr(cam_pos));
		glUniform1f(ts_ior_loc, 1.667f);
		glUniform1f(ts_tcrit_loc, glm::radians(38.68f));
		glUniform1i(ts_radfluxtex_loc, 0);
		glUniform1i(ts_radfluxcoords_loc, 1);	

		surface_mesh.VOnDraw();

		translucency_shader.StopProgram();

		glViewport(0, 0, width, height);
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}