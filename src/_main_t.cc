#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "CGETransformation.h"
#define GLEW_STATIC
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

class MatrixTestClass4x4 : public CGEngine::Matrix4x4
{
public:
	MatrixTestClass4x4(float x) : CGEngine::Matrix4x4(x) {}
	MatrixTestClass4x4(const CGEngine::Matrix4x4& _m) : CGEngine::Matrix4x4(_m) {}

	friend std::ostream &operator<< (std::ostream &out, const MatrixTestClass4x4& vec)
	{
		out << glm::to_string(vec);
		return out;
	}
};
class MatrixTestClass3x3 : public CGEngine::Matrix3x3
{
public:
	MatrixTestClass3x3(float x) : CGEngine::Matrix3x3(x) {}
	MatrixTestClass3x3(const CGEngine::Matrix3x3& _m) : CGEngine::Matrix3x3(_m) {}

	friend std::ostream &operator<< (std::ostream &out, const MatrixTestClass3x3& vec)
	{
		out << glm::to_string(vec);
		return out;
	}
};

BOOST_AUTO_TEST_CASE(MatrixStackTest)
{
	GLFWwindow* window;
	if (!glfwInit())
		exit(EXIT_FAILURE);
	window = glfwCreateWindow(640, 480, "OpenGL Testing", NULL, NULL);
	glfwMakeContextCurrent(window);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	GLenum initStatus = glewInit();
	
	CGEngine::CMatrixStack stack;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0, 2.0, 3.0);
	glPushMatrix();
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	float* data = new float[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, data);
	MatrixTestClass4x4 ogl_mat( glm::make_mat4(data) );

	stack.LoadIdentity();
	stack.Translate( CGEngine::Vec3(1.0, 2.0, 3.0) );
	stack.PushMatrix();
	stack.Rotate(45.0f, CGEngine::Vec3(0.0, 1.0, 0.0));
	MatrixTestClass4x4 my_mat( stack.getCurrentTransform() );

	std::cout << glm::to_string(glm::mat3(stack.getCurrentTransform()) ) << "\n";
	std::cout << glm::to_string(glm::inverseTranspose( stack.getCurrentNormalMatrix() ))<< "\n";

	const float* data2 = stack.getCurrentTransformPointer();

	for (int i = 0; i < 16; ++i)
	{
		std::cout << i << ": " << data[i] << ", " << data2[i] << ";\n";
		BOOST_WARN_CLOSE((double)data[i], (double)data2[i], 0.0000001);
	}

	delete[] data;
	//delete[] data2;
	glfwTerminate();
}