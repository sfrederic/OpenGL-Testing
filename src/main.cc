#include "CGEMesh_OGL2.h"
#include "CGEFullScreenQuad.h"
#include "shaders.h"
#include "CGETransformation.h"
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include "app_setup.h"
#include "noise.h"

int main(void)
{
	//////////////////////////////////////////////////////////////////////////
	GLFWwindow* window;

	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	window = glfwCreateWindow(640, 480, "OpenGL Testing", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwSetKeyCallback(window, key_callback);
	glfwSetFramebufferSizeCallback(window, frambuffer_resize_callback);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	int width(0), height(0);
	glfwGetFramebufferSize(window, &width, &height);

	glewExperimental = GL_TRUE;
	GLenum initStatus = glewInit();
	if (initStatus != GLEW_OK) 
	{
		std::cerr << "Unable to initialize GLEW!\n";
		exit(EXIT_FAILURE);
	}

	glViewport(0, 0, width, height);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);

	//////////////////////////////////////////////////////////////////////////
	CGEngine::ShaderProgram shader	;
	try
	{
		shader.LoadVertexShader( (shadercode_path + std::string("std_vertex_shader.glsl")).c_str () );
		shader.LoadFragmentShader((shadercode_path + std::string("std_fragment_shader.glsl")).c_str ());
		shader.LinkProgram();
	}
	catch (CGEngine::ShaderError& _e)
	{
		std::cerr << _e.what() << "\n";
		exit(EXIT_FAILURE);
	}

	NoiseTexture noise_tex(128);
	noise_tex.make3DNoiseTexture();
	noise_tex.init3DNoiseTexture(GL_TEXTURE2);

	GLint tex_loc = shader.GetUniformVarLocation("tex");
	GLint noise_tex_loc = shader.GetUniformVarLocation("noise_tex");
	GLint light_loc = shader.GetUniformVarLocation("lightDir");
	GLint scale_loc = shader.GetUniformVarLocation("Scale");
	GLint material_def_loc = shader.GetUniformVarLocation("materialDefinition");
	GLint effectid_loc = shader.GetUniformVarLocation("effect_ID");

	GLint proj_loc = shader.GetUniformVarLocation("projection");
	GLint nm_loc = shader.GetUniformVarLocation("normalMatrix");
	GLint mm_loc = shader.GetUniformVarLocation("model_matrix");
	GLint view_loc = shader.GetUniformVarLocation("view_matrix");
	GLint lspmat_loc = shader.GetUniformVarLocation("lightSpaceMatrix");

	CGEngine::CMeshOGL2 suzanne;
	if( !suzanne.LoadMesh( data_path + std::string("suzanne.obj")) )
		exit(EXIT_FAILURE);
	CGEngine::CMeshOGL2 dice;
	if( !dice.LoadMesh( data_path + std::string("dice.obj")) )
		exit(EXIT_FAILURE);
	CGEngine::CMeshOGL2 sphere;
	if( !sphere.LoadMesh( data_path + std::string("sphere.obj")) )
		exit(EXIT_FAILURE);
	CGEngine::CMeshOGL2 jeep;
	if( !jeep.LoadMesh( data_path + std::string("jeep1.obj")) )
		exit(EXIT_FAILURE);
	CGEngine::CFullScreenQuad fsq;
	fsq.LoadMesh(std::string(""));

	CGEngine::Vec3 lightDir(-1.0, 0.0, -1.0); //in world space
	CGEngine::CMatrixStack ModelTransformStack;
	CGEngine::Matrix4x4 projection = CGEngine::Matrix4x4( glm::perspective(45.0, (GLdouble)width/(GLdouble)height, 1.0, 100.0) );
	CGEngine::Matrix4x4 view(1.0);

	glm::ivec2 material_diffuse_def(0, 1);
	GLint effectID = 2;

	//////////////////////////////////////////////////////////////////////////

	double lastTime = glfwGetTime();
	int nbFrames = 0;
	
	while (!glfwWindowShouldClose(window))
	{
		glfwGetFramebufferSize(window, &width, &height);
		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if ( currentTime - lastTime >= 1.0 )
		{ 
			// If last prinf() was more than 1 sec ago printf and reset timer
			double spf = 1000.0/double(nbFrames);
			int fps = static_cast<int>(1.0/spf*1000.0);
			std::string t("OpenGL Testing, ");
			std::string title = std::string("SPF: ") + std::to_string((long double)spf) + std::string(" ms/frame. FPS: ") + std::to_string((long double)fps);
			glfwSetWindowTitle(window, (t+title).c_str());
			//printf("SPF: %f ms/frame. FPS: %d \n", spf, fps);
			nbFrames = 0;
			lastTime += 1.0;
		}
		glClearDepth(1.0f);
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		noise_tex.bind(GL_TEXTURE2);

		ModelTransformStack.LoadIdentity();
		ModelTransformStack.Translate(CGEngine::Vec3(0.0, 0.0, -5.0));
		CGEngine::Matrix3x3 nm(1.0);
		material_diffuse_def = glm::ivec2(0,1);

		shader.StartProgram();
		//Draw sphere
		{
			ModelTransformStack.PushMatrix();
			ModelTransformStack.UniformScale(1.0f);
			ModelTransformStack.Translate(CGEngine::Vec3(1.5,0.0,0.0));
			ModelTransformStack.Rotate(static_cast<GLfloat>(glfwGetTime())*15.0f, CGEngine::Vec3(0.0f, 1.0f, 0.0f));

			glUniform1i( tex_loc, 0 );
			glUniform1i( noise_tex_loc, 2 );
			glUniform1f( scale_loc, 1.0f );
			glUniform3fv( light_loc, 1, glm::value_ptr(lightDir) );
			glUniform2iv( material_def_loc, 1, glm::value_ptr(material_diffuse_def));
			glUniform1i( effectid_loc, effectID);
			CGEngine::CMatrixStack::ComputeNormalMatrix( nm, ModelTransformStack.getCurrentTransform(), view);
			glUniformMatrix4fv(mm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
			glUniformMatrix3fv(nm_loc, 1, false, glm::value_ptr(nm));
			glUniformMatrix4fv(proj_loc, 1, false, glm::value_ptr(projection));
			glUniformMatrix4fv(view_loc, 1, false, glm::value_ptr(view));

			sphere.VOnDraw();

			ModelTransformStack.PopMatrix();
		}

		material_diffuse_def = glm::ivec2(1,0);

		//Draw Dice
		{
			ModelTransformStack.PushMatrix();
			ModelTransformStack.Translate(CGEngine::Vec3(-1.5, -1.0, 0.0));
			ModelTransformStack.Rotate(static_cast<GLfloat>(glfwGetTime())*-15.0f, CGEngine::Vec3(0.0f, 1.0f, 0.0f));

			glUniform1i( tex_loc, 0 );
			glUniform3fv( light_loc, 1, glm::value_ptr(lightDir) );
			glUniform2iv( material_def_loc, 1, glm::value_ptr(material_diffuse_def));
			CGEngine::CMatrixStack::ComputeNormalMatrix( nm, ModelTransformStack.getCurrentTransform(), view);
			glUniformMatrix4fv(mm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
			glUniformMatrix3fv(nm_loc, 1, false, glm::value_ptr(nm));
			glUniformMatrix4fv(proj_loc, 1, false, glm::value_ptr(projection));
			glUniformMatrix4fv(view_loc, 1, false, glm::value_ptr(view));

			dice.VOnDraw();
			ModelTransformStack.PopMatrix();
		}
		//Draw Jeep
		{
			ModelTransformStack.PushMatrix();
			ModelTransformStack.UniformScale(0.5);
			ModelTransformStack.Translate(CGEngine::Vec3(0.0, 0.0, -10.0));
			ModelTransformStack.Rotate(static_cast<GLfloat>(glfwGetTime())*-15.0f, CGEngine::Vec3(0.0f, 1.0f, 0.0f));

			glUniform1i( tex_loc, 0 );
			glUniform3fv( light_loc, 1, glm::value_ptr(lightDir) );
			glUniform2iv( material_def_loc, 1, glm::value_ptr(material_diffuse_def));
			CGEngine::CMatrixStack::ComputeNormalMatrix( nm, ModelTransformStack.getCurrentTransform(), view);
			glUniformMatrix4fv(mm_loc, 1, false, ModelTransformStack.getCurrentTransformPointer());
			glUniformMatrix3fv(nm_loc, 1, false, glm::value_ptr(nm));
			glUniformMatrix4fv(proj_loc, 1, false, glm::value_ptr(projection));
			glUniformMatrix4fv(view_loc, 1, false, glm::value_ptr(view));

			jeep.VOnDraw();
			ModelTransformStack.PopMatrix();
		}
		shader.StopProgram();
		
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}