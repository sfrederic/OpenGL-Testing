#pragma once

#include "CGERenderableDataGenerator.h"

//CGAL includes (Important: keep include order!)
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
//#include <CGAL/boost/graph/helpers.h>
#include <CGAL/Polyhedral_mesh_domain_3.h>
#include <CGAL/Polyhedral_mesh_domain_with_features_3.h>
#include <CGAL/Implicit_mesh_domain_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/refine_mesh_3.h>

#include <CGAL/Polyhedron_3.h>
#include <CGAL/Triangulation_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Triangulation_cell_base_with_info_3.h>

#include <CGAL/Handle_hash_function.h>

#include <fstream>
#include <iostream>
#include <memory>

#include <boost/current_function.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace CGEngine
{
	typedef boost::property_tree::ptree ptree;
	/*
	 * Class for generating convex tetrahedral meshes with CGAL
	 */

	class CCGALConvexTetrahedralMeshGenerator : public CRenderableDataGenerator
	{
	public:
		//CGAL typedefs
		typedef CGAL::Exact_predicates_inexact_constructions_kernel EPICK;
		typedef CGAL::Polyhedron_3<EPICK>							Polyhedron_3;

		typedef CGAL::Triangulation_vertex_base_with_info_3<GLuint, EPICK>			TriangulationVertexWithIndex_3;
		typedef CGAL::Triangulation_cell_base_with_info_3<GLuint, EPICK>			TriangulationCellWithIndex_3;
		typedef CGAL::Triangulation_data_structure_3<TriangulationVertexWithIndex_3,
			TriangulationCellWithIndex_3>											Triangulation_data_structure_3;

		typedef CGAL::Triangulation_3<EPICK, Triangulation_data_structure_3>		Triangulation3D;

		typedef Triangulation3D::Cell_handle			Cell_handle;
		typedef Triangulation3D::Finite_cells_iterator	Cell_iterator;
		typedef Triangulation3D::Vertex_handle			Vertex_handle;
		typedef Triangulation3D::Vertex_iterator		Vertex_iterator;
		typedef Triangulation3D::Facet					Facet;
		typedef Triangulation3D::Point					Point;

		CCGALConvexTetrahedralMeshGenerator() = delete;
		CCGALConvexTetrahedralMeshGenerator(const std::string& _fileName);
		~CCGALConvexTetrahedralMeshGenerator();

		inline bool IsInTriangulation(const Cell_handle& _cell)
		{
			return !(m_TetMesh.is_infinite(_cell));
		}

		inline Cell_iterator CellsBegin()
		{
			return m_TetMesh.finite_cells_begin();
		}

		inline Cell_iterator CellsEnd()
		{
			return m_TetMesh.finite_cells_end();
		}

		virtual bool VMakeObject(bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace) override;

		const CGEngine::CCGALConvexTetrahedralMeshGenerator::Triangulation3D& GetTriangulation() const { return m_TetMesh; }

	protected:
		Triangulation3D m_TetMesh;
		std::string m_FileName;

		virtual void make_normals() override;

		virtual void make_texcoords() override;

		virtual void make_tangentspace() override;

	private:
	};

	/*
	 * Class for generating multi-domain tetrahedral meshes with CGAL
	 */

	class CCGALMultiDomainTetrahedralMeshGenerator : public CRenderableDataGenerator
	{
	public:
		typedef CGAL::Exact_predicates_inexact_constructions_kernel		EPICK;
		typedef CGAL::Polyhedron_3<EPICK>								Polyhedron_3;
		//Domain
		typedef CGAL::Polyhedral_mesh_domain_3<Polyhedron_3, EPICK>		Polyhedral_mesh_domain_3;
		typedef CGAL::Mesh_polyhedron_3<EPICK>::type					Mesh_polyhedron_3;
		typedef CGAL::Polyhedral_mesh_domain_with_features_3<EPICK>		Polyhedral_mesh_domain_with_features_3;

#ifdef CGAL_CONCURRENT_MESH_3
		typedef CGAL::Parallel_tag Concurrency_tag;
#else
		typedef CGAL::Sequential_tag Concurrency_tag;
#endif
		//Triangulation
		typedef CGAL::Mesh_triangulation_3<Polyhedral_mesh_domain_3,
			CGAL::Default, Concurrency_tag>::type						Triangulation3D;

		typedef CGAL::Mesh_complex_3_in_triangulation_3<Triangulation3D> C3t3;

		// Criteria
		typedef CGAL::Mesh_criteria_3<Triangulation3D>					Mesh_criteria_3;

		typedef C3t3::Cell_handle		Cell_handle;
		typedef C3t3::Cell_iterator		Cell_iterator;
		typedef C3t3::Vertex_handle		Vertex_handle;
		typedef C3t3::Facet				Facet;
		typedef C3t3::Point				Point;
		typedef C3t3::Triangulation::Vertex_iterator	Vertex_iterator;


		CCGALMultiDomainTetrahedralMeshGenerator() = delete;
		CCGALMultiDomainTetrahedralMeshGenerator(const std::string& _SurfaceMeshFilename, const Mesh_criteria_3& _MeshCriteria);
		CCGALMultiDomainTetrahedralMeshGenerator(const std::string& _SurfaceMeshFilename, const std::string& _MeshCriteriaConfigFile);
		~CCGALMultiDomainTetrahedralMeshGenerator();

		inline bool IsInTriangulation(const Cell_handle& _cell)
		{
			return m_MDTetMesh.is_in_complex(_cell);
		}

		inline Cell_iterator CellsBegin()
		{
			return m_MDTetMesh.cells_in_complex_begin();
		}

		inline Cell_iterator CellsEnd()
		{
			return m_MDTetMesh.cells_in_complex_end();
		}

		C3t3& MeshComplex_3_InTriangulation_3()
		{
			return m_MDTetMesh;
		}

		Triangulation3D& GetTriangulation()
		{
			return m_MDTetMesh.triangulation();
		}

		const C3t3::Triangulation& GetTriangulation() const
		{
			return m_MDTetMesh.triangulation();
		}

		virtual bool VMakeObject(bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace) override;

	protected:

		std::string m_FileName;
		C3t3 m_MDTetMesh;
		Mesh_criteria_3	m_MeshCriteria;
		

		virtual void make_normals() override;


		virtual void make_texcoords() override;


		virtual void make_tangentspace() override;

		void setup_vertices_and_faces(const C3t3& c3t3);

	private:
	};
} //End namespace