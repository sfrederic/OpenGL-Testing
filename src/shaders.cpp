#include "shaders.h"

namespace CGEngine
{
	char i = 0 ;
	
	ShaderProgram::ShaderProgram()
	{
		this->m_FragmentShaderID	= 0	;
		this->m_Program				= 0	;
		this->m_VertexShaderID		= 0 ;
		this->m_Uniformlist.clear()		;
	}
	
	ShaderProgram::ShaderProgram(const char* fileName) 
	{
		this->LoadVertexShader((std::string(fileName) + std::string(".vert")).c_str())	;
		this->LoadFragmentShader((std::string(fileName) + std::string(".frag")).c_str());
		this->LinkProgram()					;
		this->m_Uniformlist.clear()			;
	}
	
	ShaderProgram::~ShaderProgram() 
	{
		glDetachShader(m_Program, m_FragmentShaderID);
		glDetachShader(m_Program, m_VertexShaderID);
		glDeleteShader(m_FragmentShaderID);
		glDeleteShader(m_VertexShaderID);
		glDeleteProgram(m_Program) ;
		m_Uniformlist.clear();
	}
	
	unsigned long ShaderProgram::getFileLength(FILE* pFile) 
	{
	    if(!pFile) 
			return 0 ;
	    
		unsigned long size ;
	
		fseek(pFile, 0, SEEK_END) ;
	    
		size = ftell(pFile) ;
	
		rewind(pFile) ;
	    
	    return size ;
	}
	
	char* ShaderProgram::loadshader(const char* filename, unsigned long* len)
	{
		std::ifstream file_stream(filename);
		file_stream.seekg(0, std::ios::end);
		auto size = static_cast<size_t>(file_stream.tellg());
		auto ShaderSource = new char[size + 1];
		for (unsigned int i = 0; i < size + 1; ++i)
			ShaderSource[i] = 0;
		file_stream.seekg(0);
		file_stream.read(&ShaderSource[0], size);
		ShaderSource[size] = 0;
		return ShaderSource;
	}
	
	void ShaderProgram::printShaderInfoLog( GLuint obj )
	{
		int infologLength = 0;
		int charsWritten  = 0;
		char *infoLog;
	
		glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &infologLength);
	
		if (infologLength > 0)
		{
			infoLog = (char *)malloc(infologLength);
			glGetShaderInfoLog(obj, infologLength, &charsWritten, infoLog);
			printf("%s\n",infoLog);
			free(infoLog);
		}
	}
	
	void ShaderProgram::printProgramInfoLog( GLuint obj )
	{
		int infologLength = 0;
		int charsWritten  = 0;
		char *infoLog;
	
		glGetProgramiv(obj, GL_INFO_LOG_LENGTH,&infologLength);
	
		if (infologLength > 0)
		{
			infoLog = (char *)malloc(infologLength);
			glGetProgramInfoLog(obj, infologLength, &charsWritten, infoLog);
			printf("%s\n",infoLog);
			free(infoLog);
		}
	}
	
	bool ShaderProgram::CreateShader( GLenum _shader_type, GLuint _shader_id, const char* _source )
	{
		int compiled(0);
		//Create shader object
		_shader_id = glCreateShader(_shader_type) ;
		if (glGetError() != GL_NO_ERROR)
		{
			throw ShaderError(std::string("Failed to create shader!"));
		}

		if (!_source)
			return false ;

		glShaderSource(_shader_id, 1, &_source, NULL) ;

		//Compile shader
		glCompileShader(_shader_id) ;

		glGetShaderiv(_shader_id, GL_COMPILE_STATUS, &compiled) ;

		if (!compiled) 
		{
			printShaderInfoLog(_shader_id);
			throw ShaderError(std::string("Failed to compile shader!"));
			//return false ;
		}

		//Create a program object and attach shader
		if (m_Program == 0)
			m_Program = glCreateProgram() ;

		glAttachShader(m_Program, _shader_id) ;
		if (glGetError() != GL_NO_ERROR)
		{
			throw ShaderError(std::string("Failed to attach shader to program!"));
		}
		return true;
	}

	bool ShaderProgram::LoadVertexShader(const char* fileName)
	{
		unsigned long length = 0 ;
		try
		{
			char* vsSource = loadshader(fileName, &length) ;
			return CreateShader( GL_VERTEX_SHADER, m_VertexShaderID, vsSource );
		}
		catch (CGEngine::ShaderError& _e)
		{
			std::cerr << fileName << " error: " << _e.what() << std::endl;
			throw ShaderError(std::string("Failed to load shader!"));
		}
	}

	bool ShaderProgram::LoadFragmentShader(const char* fileName)
	{
		unsigned long length = 0 ;
		try
		{
			char* fsSource = loadshader(fileName, &length);
			return CreateShader(GL_FRAGMENT_SHADER, m_FragmentShaderID, fsSource);
		}
		catch (CGEngine::ShaderError& _e)
		{
			std::cerr << fileName << " error: " << _e.what() << std::endl;
			throw ShaderError(std::string("Failed to load shader!"));
		}
	}
	
	bool ShaderProgram::LoadGeometryShader(const char* fileName)
	{
		unsigned long length = 0;
		try
		{
			char* gsSource = loadshader(fileName, &length);
			return CreateShader(GL_GEOMETRY_SHADER, m_VertexShaderID, gsSource);
		}
		catch (CGEngine::ShaderError& _e)
		{
			std::cerr << fileName << " error: " << _e.what() << std::endl;
			throw ShaderError(std::string("Failed to load shader!"));
		}
	}

	bool ShaderProgram::LoadShaderInfoFile(const char* fileName)
	{
		return (!fileName);
	}

	bool ShaderProgram::VertexShaderFromSource( const char* _src )
	{
		if ( !_src )
		{
			return false;
		}
		else
		{
			return CreateShader( GL_VERTEX_SHADER, m_VertexShaderID, _src );
		}
	}

	bool ShaderProgram::FragmentShaderFromSource( const char* _src )
	{
		if ( !_src )
		{
			return false;
		} 
		else
		{
			return CreateShader(GL_FRAGMENT_SHADER, m_FragmentShaderID, _src) ;
		}
	}

	bool ShaderProgram::GeometryShaderFromSource(const char* _src)
	{
		if (!_src)
		{
			return false;
		}
		else
		{
			return CreateShader(GL_GEOMETRY_SHADER, m_GeometryShaderID, _src);
		}
	}

	bool ShaderProgram::LinkProgram() {
		GLint linked ;
		glLinkProgram(m_Program) ;
		glGetProgramiv(m_Program, GL_LINK_STATUS, &linked) ;
	
		if (!linked) {
			printProgramInfoLog(m_Program);
			throw ShaderError(std::string("Failed to link program!"));
			//return false;
		}
		else
			return true;
	}
	
	void ShaderProgram::StartProgram()
	{
		glUseProgram( m_Program );
	}

	void ShaderProgram::StopProgram()
	{
		GLint curr_prog(0);
		glGetIntegerv(GL_CURRENT_PROGRAM, &curr_prog);
		if ( curr_prog == m_Program )
		{
			glUseProgram(0);
		}
		else
			return;
	}

	GLuint ShaderProgram::GetVertexShaderID() {
		return m_VertexShaderID ;
	}
	
	GLuint ShaderProgram::GetFragmentShaderID() {
		return m_FragmentShaderID ;
	}
	
	GLuint ShaderProgram::GetGeometryShaderID()
	{
		return m_GeometryShaderID;
	}

	GLuint ShaderProgram::GetProgram() {
		return m_Program ;
	}
	
	GLint ShaderProgram::GetUniformVarLocation(const GLchar* name) 
	{
		GLint loc ;
		loc = glGetUniformLocation(this->m_Program, name) ;
		return loc ;
	}
}