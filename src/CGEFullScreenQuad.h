#ifndef CGEFULLSCREENQUAD_H
#define CGEFULLSCREENQUAD_H

#ifndef MESH_H
    #include "CGEMesh_OGL2.h"
#endif

namespace CGEngine {

class CFullScreenQuad;
typedef boost::shared_ptr<CFullScreenQuad>  FullScreenQuadPtr   ;

class CFullScreenQuad : public CMesh
{
private:
    CFullScreenQuad(const CFullScreenQuad&) {}
public:
    CFullScreenQuad() : CMesh() {}
    ~CFullScreenQuad() {}

    bool LoadMesh(const std::string &_filename, unsigned int _postprocessingFlags  = CGE_NO_POSTPROCESSING);
    bool HasProperty(CGEMeshProperty _property) const;
	bool MakeFullscreenQuad(float _width, float _height, float _z_value = 0.5f);
};

}   //Namespace

#endif // CGEFULLSCREENQUAD_H
