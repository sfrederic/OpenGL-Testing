#include "CGEMesh_OGL2.h"

CGEngine::CMeshOGL2::CMeshOGL2(
	GLenum _PrimitiveType, GLenum _IndexType, GLenum _DrawType) :
	CMesh(_PrimitiveType, _IndexType, _DrawType)
{
}

CGEngine::CMeshOGL2::~CMeshOGL2()
{
	clear();
}

void CGEngine::CMeshOGL2::VOnDraw()
{
	for (unsigned int i = 0 ; i < m_Entries.size() ; i++) 
	{
		const unsigned int MaterialIndex = m_Entries[i].m_MaterialIndex;

		if ( MaterialIndex < m_Textures.size() && m_Textures[MaterialIndex] )
		{
			glActiveTexture(GL_TEXTURE0);
			m_Textures[MaterialIndex]->Bind(GL_TEXTURE0);
		}
		glBindBuffer(GL_ARRAY_BUFFER, m_Entries[i].m_VBO) ;
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Entries[i].m_IBO) ;

		if (m_Entries[i].m_NormalArraySize)
			glEnableClientState(GL_NORMAL_ARRAY);
		if (m_Entries[i].m_TexCoordsArraySize)
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		if (m_Entries[i].m_VertexArraySize)
			glEnableClientState(GL_VERTEX_ARRAY);

		glVertexPointer		(3,	GL_FLOAT,	0, (GLvoid*)0) ;
		glTexCoordPointer	(2, GL_FLOAT,	0, (GLvoid*)(m_Entries[i].m_VertexArraySize)) ;
		glNormalPointer		(GL_FLOAT,		0, (GLvoid*)(m_Entries[i].m_VertexArraySize + m_Entries[i].m_TexCoordsArraySize)) ;

		GLsizei num_elements = m_Entries[i].m_IndexBufferSize / sizeof(GLuint);
		glDrawElements(m_PrimitiveType, num_elements, m_IndexType, 0) ;

		glDisableClientState(GL_VERTEX_ARRAY) ;
		glDisableClientState(GL_TEXTURE_COORD_ARRAY) ;
		glDisableClientState(GL_NORMAL_ARRAY) ;

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

	}
}

bool CGEngine::CMeshOGL2::VGetVerticesFromGPUBuffer( CGEngine::Vec3DVec& _vertices )
{
	_vertices.clear();
	size_t num_vertices(0);
	bool ok(true);
	for ( unsigned int e = 0; e < m_Entries.size(); e++ )
	{
		num_vertices += m_Entries[e].m_NumVertices;
		if (m_Entries[e].m_VBO == 0)
			ok = false;
	}
	if ( (num_vertices == 0) || !ok )
		return false;

	_vertices.resize(num_vertices, Vec3(0.0f));

	for ( unsigned int e = 0; e < m_Entries.size(); e++ )
	{
		int index = 0;
		if (e > 0) index = m_Entries[e-1].m_NumVertices;

		glBindBuffer(GL_ARRAY_BUFFER, m_Entries[e].m_VBO) ;
		glGetBufferSubData(GL_ARRAY_BUFFER, 0, m_Entries[e].m_VertexArraySize, (GLvoid*)&_vertices[index]);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return true;
}

void CGEngine::CMeshOGL2::populateBuffers( unsigned int _entry_number, const Vec3DVec &_positions, const Vec2DVec &_texcoords, const Vec3DVec &_normals, const std::vector<GLuint> &_indices )
{
	// create new Vertex Buffer Object
	glGenBuffers(1, &(m_Entries[_entry_number].m_VBO)) ;
	glGenBuffers(1, &(m_Entries[_entry_number].m_IBO)) ;

	m_Entries[_entry_number].m_VertexArraySize		= 3 * _positions.size()	* sizeof(GLfloat)	;
	m_Entries[_entry_number].m_TexCoordsArraySize	= 2 * _texcoords.size()	* sizeof(GLfloat)	;
	m_Entries[_entry_number].m_NormalArraySize		= 3 * _normals.size()	* sizeof(GLfloat)	;

	m_Entries[_entry_number].m_IndexBufferSize		= _indices.size()		* sizeof(GLuint)	;

	// bind buffer and load data
	glBindBuffer(GL_ARRAY_BUFFER, m_Entries[_entry_number].m_VBO) ;

	// reserve buffer size
	glBufferData(GL_ARRAY_BUFFER, m_Entries[_entry_number].m_VertexArraySize + m_Entries[_entry_number].m_TexCoordsArraySize + m_Entries[_entry_number].m_NormalArraySize,
		0, m_DrawType);

	// first part: vertex data, second part: texture coordinates, third part: normal coordinates
	if (m_Entries[_entry_number].m_VertexArraySize > 0)
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_Entries[_entry_number].m_VertexArraySize, &_positions[0]);
	if (m_Entries[_entry_number].m_TexCoordsArraySize > 0)
		glBufferSubData(GL_ARRAY_BUFFER, m_Entries[_entry_number].m_VertexArraySize, m_Entries[_entry_number].m_TexCoordsArraySize,
			&_texcoords[0]);
	if (m_Entries[_entry_number].m_NormalArraySize > 0)
		glBufferSubData(GL_ARRAY_BUFFER, m_Entries[_entry_number].m_VertexArraySize + m_Entries[_entry_number].m_TexCoordsArraySize, 
			m_Entries[_entry_number].m_NormalArraySize, &_normals[0]);

	// index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Entries[_entry_number].m_IBO) ;
	if (m_Entries[_entry_number].m_IndexBufferSize > 0)
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Entries[_entry_number].m_IndexBufferSize, &_indices[0], m_DrawType);

// 	glVertexPointer(3, GL_FLOAT, 0, (GLvoid*)((char*)NULL));
// 	glTexCoordPointer(2, GL_FLOAT, 0, (GLvoid*)((char*)NULL + m_VertexArraySize));
// 	glNormalPointer(GL_FLOAT, 0, (GLvoid*)((char*)NULL + m_VertexArraySize + m_TexCoordsArraySize));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void CGEngine::CMeshOGL2::clear()
{
	if ( m_VAO != 0 )
		glDeleteBuffers(1, &m_VAO);
	BOOST_FOREACH( CMeshEntry& me, m_Entries )
	{
		glDeleteBuffers(1, &(me.m_VBO));
		glDeleteBuffers(1, &(me.m_IBO));
	}
	m_Entries.clear();

	m_Textures.clear();
}
