#pragma once

#include "CGERenderableDataGenerator.h"

#include <iostream>
#include <fstream>
#include <memory>

#include <boost/noncopyable.hpp>
#include <boost/foreach.hpp>
#include <boost/current_function.hpp>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>

namespace CGEngine
{
	/*
	 *class for setting up and processing meshes loaded by CGAL
	 */
	
	class CCGALMeshGenerator : public CRenderableDataGenerator
	{
	public:
		typedef CGAL::Simple_cartesian<double> SCK;
		typedef SCK::Point_3	Point_3;
		typedef SCK::Vector_3	Vector_3;
		typedef CGAL::Surface_mesh<Point_3>	SurfaceMesh;
		typedef SurfaceMesh::Vertex_index Vertex_handle;

		const std::string& GetFileName() const { return m_FileName; }
		void SetFileName(std::string val) { m_FileName = val; }
	
		CCGALMeshGenerator() = delete;
		CCGALMeshGenerator(const std::string& _fileName);
		virtual ~CCGALMeshGenerator();

		virtual bool VMakeObject(bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace) override;

		void SetSurfaceMesh(std::unique_ptr<SurfaceMesh>& _new_mesh)
		{
			m_Mesh = std::move(_new_mesh);
		}

		SurfaceMesh& GetSurfaceMesh()
		{
			return *m_Mesh;
		}

		const SurfaceMesh& GetSurfaceMesh() const
		{
			return *m_Mesh;
		}

	private:
		std::string	m_FileName;
		std::unique_ptr<SurfaceMesh> m_Mesh;

		template<typename...>
		struct property_collector;

		template<typename T, typename... Ts>
		struct property_collector<T, Ts...>
		{
			static void collect_vertex_properties_of_type_T(const std::vector<std::string>& _prop_names, CCGALMeshGenerator* _pGen)
			{
				using property_map = typename SurfaceMesh::Property_map<Vertex_handle, T>;
				using tmp_pair = typename std::pair<property_map, bool>;
				BOOST_FOREACH(const std::string& prop_name, _prop_names)
				{
					tmp_pair res = _pGen->GetSurfaceMesh().property_map<Vertex_handle, T>(prop_name);
					if (res.second)
					{
						Attributes::CAttributeWrapper<T> a = _pGen->AddVertexAttribute<T>(prop_name);
						a.CopyFrom(res.first.begin(), res.first.end());
					}
				}
				property_collector<Ts...>::collect_vertex_properties_of_type_T(_prop_names, _pGen);
			}
		};

		template<>
		struct property_collector<>
		{
			static void collect_vertex_properties_of_type_T(const std::vector<std::string>& _prop_names, CCGALMeshGenerator* _pGen)
			{
			}
		};

	public:

		//************************************
		// Method:    CollectAttributes
		// FullName:  CGEngine::CCGALMeshGenerator::CollectAttributes
		// Access:    public 
		// Returns:   void
		// Qualifier:
		//************************************
		template<typename... Ts>
		void CollectAttributes()
		{
			std::vector<std::string> cgal_attribute_names = m_Mesh->properties<Vertex_handle>();
			if (!cgal_attribute_names.empty())
			{
				m_VertexAttributeContainer.resize(m_Mesh->num_vertices());
				property_collector<Ts...>::collect_vertex_properties_of_type_T(cgal_attribute_names, this);
			}
		}

	protected:
		virtual void make_normals() override;


		virtual void make_texcoords() override;


		virtual void make_tangentspace() override;
	};
}
