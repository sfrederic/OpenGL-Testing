#include "CGETexture2D.h"

namespace CGEngine {

bool CGLTexture2D::VLoadTexture(const std::string& _fileName, GLenum _pixel_store_param, GLint _pixel_store_value) {
    //TODO : border and level(mipmaps)
	Image<GLubyte> img(_fileName.c_str());
	if ( img.getErrorCode() != OK)
	{
		return false;
	}

	m_Width = (int)img.getWidth() ;
	m_Height = (int)img.getHeight() ;
	GLint format = ( img.getColorModel()==RGB ) ? GL_RGB : GL_RGBA ;

	//Load image to GPU
	glGenTextures(1, &m_TextureID);

	glBindTexture(GL_TEXTURE_2D, m_TextureID);
	glPixelStorei(_pixel_store_param, _pixel_store_value);
	glTexImage2D(GL_TEXTURE_2D, 0, format, (GLsizei)(img.getWidth()),
				(GLsizei)(img.getHeight()), 0, format, GL_UNSIGNED_BYTE,
				(GLvoid*)img.getDataPointer());
	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}

bool CGLTexture2D::VLoadTexture(GLsizei _width, GLsizei _height, GLenum _internal_format, GLenum _tex_format, GLenum _type, GLvoid* _data)
{
	bool ret_value = true;
	if (m_TextureID != 0)
	{
		VReleaseTexture();
	}
	GLint level = 0; GLint border = 0; //TODO
	glGenTextures(1, &m_TextureID);
	glBindTexture(GL_TEXTURE_2D, m_TextureID);
	glTexImage2D(GL_TEXTURE_2D, level, _internal_format, _width, _height, border, _tex_format, _type, _data);
	if (glGetError() != GL_NO_ERROR)
		ret_value = false;
	glBindTexture(GL_TEXTURE_2D, 0);
	m_Width = (int)_width; m_Height = (int)_height;
	return ret_value;
}

GLenum CGLTexture2D::VGetTextureTarget()
{
	return GL_TEXTURE_2D;
}

void CGLTexture2D::SetTexParami(const GLenum _name, const GLint _param) {
	GLuint boundTexture = 0;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*)&boundTexture);
    glBindTexture(GL_TEXTURE_2D, m_TextureID);
    glTexParameteri(GL_TEXTURE_2D, _name, _param);
    glBindTexture(GL_TEXTURE_2D, boundTexture);
}

void CGLTexture2D::SetTexParamf(const GLenum _name, const GLfloat _param) {
    glBindTexture(GL_TEXTURE_2D, m_TextureID);
    glTexParameterf(GL_TEXTURE_2D, _name, _param);
    glBindTexture(GL_TEXTURE_2D, 0);
}

}   //End Namespace
