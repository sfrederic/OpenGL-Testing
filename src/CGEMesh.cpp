#include "CGEMesh.h"
#include "functionality.h"
#include <assert.h>

namespace CGEngine {

//////////////////////////////////////////////////////////////////////////////////////////////
//  CMeshEntry
//////////////////////////////////////////////////////////////////////////////////////////////

CMesh::CMeshEntry::CMeshEntry()
{
    m_NumIndices  = 0;
    m_MeshProperties = 0 ;
	m_BaseVertex =0;
	m_BaseIndex=0;
	m_MaterialIndex=0;
	m_MeshProperties=0;
	m_VBO = m_IBO = m_VertexArraySize = m_NormalArraySize = m_TexCoordsArraySize = m_IndexBufferSize=0;
}

CMesh::CMeshEntry::~CMeshEntry()
{
}

///////////////////////////////////////////////////////////////////////////////////////////////
//  CMesh
///////////////////////////////////////////////////////////////////////////////////////////////

bool CMesh::LoadMesh(const std::string &_filename, unsigned int _postprocessingFlags)
{
    bool return_value = false;
    clear();

    Assimp::Importer Importer;

    const aiScene* pScene = Importer.ReadFile(_filename.c_str(), _postprocessingFlags );

	Vec3DVec positions;
	Vec3DVec normals;
	Vec2DVec texcoords;
	std::vector<GLuint> indices;

    if (pScene)
    {
        return_value = initFromScene(pScene, _filename);
		
		if ( _postprocessingFlags & CGE_COMPUTE_TANGENT_SPACE )
		{
		}

		return_value = return_value && (glGetError() == GL_NO_ERROR);
    }
    else
    {
        std::cerr << "Error parsing " << _filename.c_str() << "Assimp: " << Importer.GetErrorString() << std::endl ;
    }

	m_FileName = _filename;

    return return_value;
}

bool CMesh::LoadMesh(const CRenderableDataGenerator* _mesh_generator)
{
	bool return_value = false;
	clear();
	
	if (!_mesh_generator)
	{
		return return_value;
	}

	return_value = initFromGenerator(_mesh_generator);

	return return_value;
}

bool CMesh::LoadMesh(const Vec3DVec& _vertex_positions, const GLuintVec& _indices, const Vec3DVec& _normals, const Vec2DVec _texture_coordinates)
{
	m_Entries.resize(1);

	for (unsigned int i = 0; i < m_Entries.size(); i++)
	{
		m_Entries[i].m_MaterialIndex = 0;
		m_Entries[i].m_NumIndices = _indices.size();
		m_Entries[i].m_BaseVertex = 0;
		m_Entries[i].m_BaseIndex = 0;
		m_Entries[i].m_NumVertices = _vertex_positions.size();
	}
	populateBuffers(0, _vertex_positions, _texture_coordinates, _normals, _indices);

	return true;
}

void CMesh::populateBuffers( unsigned int _entry_number, const Vec3DVec &_positions, 
	const Vec2DVec &_texcoords, const Vec3DVec &_normals, const std::vector<GLuint> &_indices )
{
	//Create Vertex-Array
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);
	//Create buffers for the vertex attributes
	m_Buffers.resize(4, 0);
	glGenBuffers(m_Buffers.size(), &m_Buffers[0]);

	// Generate and populate the buffers with vertex attributes and the indices
	if (_positions.size() > 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[POS_VB]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(_positions[0]) * _positions.size(), &_positions[0], m_DrawType);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
	else
		glDeleteBuffers(1, &m_Buffers[POS_VB]);

	if (_texcoords.size()>0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[TEXCOORD_VB]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(_texcoords[0]) * _texcoords.size(), &_texcoords[0], m_DrawType);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}
	else
		glDeleteBuffers(1, &m_Buffers[TEXCOORD_VB]);

	if (_normals.size()>0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[NORMAL_VB]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(_normals[0]) * _normals.size(), &_normals[0], m_DrawType);
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
	else
		glDeleteBuffers(1, &m_Buffers[NORMAL_VB]);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Buffers[INDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(_indices[0]) * _indices.size(), &_indices[0], m_DrawType);

	//must never be called here!
	//glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

bool CMesh::initAttributes(const CRenderableDataGenerator* _mesh_generator)
{
	if (m_VAO == 0)
		return false;
	glBindVertexArray(m_VAO);

	if (_mesh_generator->HasAttributes())
	{
		std::vector<std::string> attrib_names;
		_mesh_generator->GetAttributeNames(attrib_names);

		m_AttribBuffers.resize(_mesh_generator->GetNumAttributes(), 0);
		glGenBuffers(m_AttribBuffers.size(), &m_AttribBuffers[0]);

		//temporary solution for attribute indices
		m_AttribIndices.clear(); m_AttribIndices.resize(_mesh_generator->GetNumAttributes(), 0);
		for (int i = 0, ii = 4; i < m_AttribIndices.size(); ++i, ++ii)
			m_AttribIndices[i] = ii;

		const Attributes::CAttributeContainer& attrib_container = _mesh_generator->GetAttributeContainer();

		int i = 0;
		BOOST_FOREACH(const std::string& name, attrib_names)
		{
			try
			{
				Attributes::AttribStoragePtr pAttribStorage = _mesh_generator->GetVertexAttributeStorage(name);
				pAttribStorage->AttributeBufferData(
					m_AttribBuffers[i],
					attrib_container.mem_size(name), 
					attrib_container.GetAttribDataPointer(name),
					m_DrawType);
				
				pAttribStorage->VAttributeStorage(m_VAO, m_AttribIndices[i], 0);
				i++;
			}
			catch (std::string& _s)
			{
				std::cerr << _s << std::endl;
			}
		}
	}
	glBindVertexArray(0);
	return true;
}

bool CMesh::initFromScene(const aiScene* _pScene, const std::string& _filename)
{
    m_Entries.resize( _pScene->mNumMeshes );
    m_Textures.resize( _pScene->mNumMaterials );

    unsigned int num_vertices = 0;
    unsigned int num_indices = 0;
    for ( unsigned int i = 0; i < m_Entries.size(); i++)
    {
        m_Entries[i].m_MaterialIndex	= _pScene->mMeshes[i]->mMaterialIndex	;
        m_Entries[i].m_NumIndices		= _pScene->mMeshes[i]->mNumFaces * 3	;
        m_Entries[i].m_BaseVertex		= num_vertices							;
        m_Entries[i].m_BaseIndex		= num_indices							;
		m_Entries[i].m_NumVertices		= _pScene->mMeshes[i]->mNumVertices		;

        num_vertices	+= _pScene->mMeshes[i]->mNumVertices	;
        num_indices		+= m_Entries[i].m_NumIndices			;
    }

    Vec3DVec positions;
    Vec3DVec normals;
    Vec2DVec texcoords;
    std::vector<GLuint> indices;

    // Initialize the meshes in the scene one by one
    for (unsigned int i = 0 ; i < m_Entries.size() ; i++)
    {
        const aiMesh* paiMesh = _pScene->mMeshes[i];
        initMesh(paiMesh, positions, normals, texcoords, indices);
        setProperties(i, paiMesh);
		populateBuffers(i, positions, texcoords, normals, indices);
		positions.clear(); normals.clear(); texcoords.clear(); indices.clear();
    }

    if ( !initMaterials(_pScene, _filename) )
        return false;

    return true;
}

bool CMesh::initFromGenerator(const CRenderableDataGenerator* _mesh_generator)
{
	m_Entries.resize(1);
	bool return_val = false;

	for (unsigned int i = 0; i < m_Entries.size(); i++)
	{
		m_Entries[i].m_MaterialIndex = 0;
		m_Entries[i].m_NumIndices = _mesh_generator->GetFaceIndices().size();
		m_Entries[i].m_BaseVertex = 0;
		m_Entries[i].m_BaseIndex = 0;
		m_Entries[i].m_NumVertices = _mesh_generator->GetVertices().size();
	}
	
	populateBuffers(0, _mesh_generator->GetVertices(), _mesh_generator->GetTextureCoordinates(),
		_mesh_generator->GetNormalVectors(), _mesh_generator->GetFaceIndices());

	return_val = return_val || initAttributes(_mesh_generator);

	return return_val;
}

void CMesh::initMesh(const aiMesh* _paiMesh, Vec3DVec& _positions, Vec3DVec& _normals, Vec2DVec& _texCoords, std::vector<unsigned int>& _indices)
{
    const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

    for (unsigned int i = 0 ; i < _paiMesh->mNumVertices ; i++) {
        const aiVector3D* pPos      = &(_paiMesh->mVertices[i]);
        const aiVector3D* pNormal   = _paiMesh->HasNormals() ? &(_paiMesh->mNormals[i]) : &Zero3D;
        const aiVector3D* pTexCoord = _paiMesh->HasTextureCoords(0) ? &(_paiMesh->mTextureCoords[0][i]) : &Zero3D;

        _positions.push_back( Vec3(pPos->x, pPos->y, pPos->z) );
        _normals.push_back( Vec3(pNormal->x, pNormal->y, pNormal->z) );
        _texCoords.push_back( Vec2(pTexCoord->x, pTexCoord->y ) );
    }
    //Index buffer
    for (unsigned int i = 0 ; i < _paiMesh->mNumFaces ; i++) 
	{
        const aiFace& Face = _paiMesh->mFaces[i];
        assert(Face.mNumIndices == 3);
        _indices.push_back(Face.mIndices[0]);
        _indices.push_back(Face.mIndices[1]);
        _indices.push_back(Face.mIndices[2]);
    }
}

void CMesh::setProperties(unsigned int _index, const aiMesh *_paiMesh)
{
    if ( _paiMesh->HasTextureCoords(0) )
    {
        m_Entries[_index].m_MeshProperties |= CGE_HAS_TEXCOORDS ;
    }
    if ( _paiMesh->HasNormals() )
    {
        m_Entries[_index].m_MeshProperties |= CGE_HAS_NORMALS ;
    }
    if ( _paiMesh->HasTangentsAndBitangents() )
    {
        m_Entries[_index].m_MeshProperties |= CGE_HAS_TANGENTSPACE ;
    }

}

void CMesh::clear()
{
    m_Textures.clear();
	m_Entries.clear();

	if (!m_Buffers.empty())
	{
		glDeleteBuffers(m_Buffers.size(), &m_Buffers[0]);
	}

	if (!m_AttribBuffers.empty())
	{
		glDeleteBuffers(m_AttribBuffers.size(), &m_AttribBuffers[0]);
	}

    if (m_VAO != 0)
    {
        glDeleteVertexArrays(1, &m_VAO);
        m_VAO = 0;
    }
}

/*
 * Still a lot to do here (for different material types and textures...)
 */
bool CMesh::initMaterials(const aiScene *_pScene, const std::string &_filename)
{
    bool return_value = true;

    int pos = _filename.find_last_of("\\/");
    std::string current_dir = _filename.substr(0, pos);

    // Initialize the materials
    for (unsigned int i = 0 ; i < _pScene->mNumMaterials ; i++)
    {
        const aiMaterial* pMaterial = _pScene->mMaterials[i];

        if (pMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
            aiString aiPath;

            if (pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &aiPath, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
            {
                std::string path(aiPath.data);
#ifdef _WIN32
                std::string slash("\\");
#else
                std::string slash("/");
#endif
                size_t p = path.find("..\\");
                size_t len = 3;
                if ( p != std::string::npos )
                {
                    path.replace(p, len, "");
                }
                std::string fullPath = current_dir + slash + path;
#ifndef _WIN32
                p = fullPath.find_first_of("\\");
                while ( p != std::string::npos )
                {
                    fullPath.replace(p, 1, slash);
                    p = fullPath.find_first_of("\\");
                } ;
#endif

                GLTexture2DShPtr tex_ptr(new CGLTexture2D);

                if ( !tex_ptr->VLoadTexture(fullPath) )
                {
                    std::cout << "Error Loading Texture: " << "For Mesh Object." << std::endl;
                    m_Textures[i] = GLTexture2DShPtr() ;
                    return_value = false;
                }
                else
                {
                    m_Textures[i] = tex_ptr ;
                }
            }
        }

    }

    return return_value;

}

void CMesh::VOnDraw()
{
    glBindVertexArray(m_VAO);

    for (unsigned int i = 0 ; i < m_Entries.size() ; i++) {

        const unsigned int MaterialIndex = m_Entries[i].m_MaterialIndex;

        if (MaterialIndex < m_Textures.size() && m_Textures[MaterialIndex])
        {
            m_Textures[MaterialIndex]->Bind(GL_TEXTURE0);
        }

        /*glDrawElementsBaseVertex(m_PrimitiveType, m_Entries[i].m_NumIndices, m_IndexType,
                                 (GLvoid*)(sizeof(unsigned int) * m_Entries[i].m_BaseIndex), m_Entries[i].m_BaseVertex);*/
		glDrawElements(m_PrimitiveType, m_Entries[i].m_NumIndices, m_IndexType, 0);
    }

    glBindVertexArray(0);
}

bool CMesh::HasProperty(CGEMeshProperty _property) const
{
    unsigned int a = 0x0 | _property ;
    bool return_value = true;

    for (unsigned int i = 0 ; i < m_Entries.size(); ++i)
    {
        unsigned int b = m_Entries[i].m_MeshProperties & a ;
        if ( (b >> (_property-1)) != 1 )
            return_value = false;
    }

    return return_value;
}

std::string CMesh::GetFileName() const
{
	return m_FileName;
}

}   //End Namespace