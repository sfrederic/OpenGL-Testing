/********************************************************************
*
*	Loading images
*	Supported files: .ppm, .bmp
*	(heavy legacy code!!)
*
********************************************************************/

#include<stdio.h>
#include<string.h>
#ifndef _WINDOWS_H
	#include<windows.h>	//for loading bitmaps
#endif

#ifndef IMAGES_H
#define IMAGES_H

namespace CGEngine
{
	#define RGB_IMAGE			3
	#define RGBA_IMAGE			4
	#define COLOR_DEPTH_8BIT	256
	
	enum ColorModel 
	{
		NONE	= -1,
		RGB		= 3,
		RGBA
	};
	
	enum ErrorCode
	{
		OK,
		FILE_NOT_FOUND,
		FILE_ERROR,
		MEMORY_ERROR,
		UNSUPPORTED_FILE_EXT,
		COMPRESSION_UNSUPPORTED
	};
	
	//For Bitmaps: Defines Color palette entries:
	struct ColorMapEntry
	{
		unsigned char R;
		unsigned char G;
		unsigned char B;
		unsigned char A;
	};
	
	//Template for image types
	template<typename ImDataType>
	
	//Class for images
	class Image {
	private:
		ImDataType*	m_imageData		;	//Image data
		long		m_lSize			;	//Size in bytes
		long		m_lWidth		;
		long		m_lHeight		;
		ColorModel	m_eColorModel	;
		ErrorCode	m_eErrorCode	;
	
		Image() {}
		Image(const Image<ImDataType>&) {}
	
		void initNull() 
		{
			m_imageData		= NULL	;
			m_lWidth		= 0		;
			m_lHeight		= 0		;
			m_lSize			= 0		;
			m_eColorModel	= NONE	;
		}
	
		//Loading images (ppm file format)
		void load_ppm(const char* fileName) ;
		//Loading images (bmp file format)
		void load_bmp(const char* fileName)	;
	
	public:
		//Constructor
		Image(const char* imageFile)
		{
			//load ppm-File
			if ((strstr(imageFile, ".ppm") != NULL) || (strstr(imageFile, ".PPM") != NULL))
			{
				this->load_ppm(imageFile) ;
			}
			else if ((strstr(imageFile, ".bmp") != NULL) || (strstr(imageFile, ".BMP") != NULL))
			{
				//load bmp
				this->load_bmp(imageFile) ;
			}
			else 
			{
				initNull() ;
				m_eErrorCode = UNSUPPORTED_FILE_EXT ;
			}
	
		}
	
		//Destructor
		~Image() 
		{
			if (m_imageData != NULL)
				delete[] m_imageData	;
			m_imageData = NULL			;
		}
	
		ImDataType*	getDataPointer(){return this->m_imageData	;}
		ImDataType	getData(long i)	{return this->m_imageData[i];}
		long		getWidth()		{return this->m_lWidth		;}
		long		getHeight()		{return this->m_lHeight		;}
		ColorModel	getColorModel()	{return this->m_eColorModel	;}
		ErrorCode	getErrorCode()	{return this->m_eErrorCode	;}
	};
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Implementation
	//
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	template<typename ImDataType>
	
	void Image<ImDataType>::load_ppm(const char* fileName) 
	{
		FILE *fp = fopen(fileName, "r") ;
	
		if (fp == NULL) { 
			initNull() ;
			m_eErrorCode = FILE_NOT_FOUND ;
			return ;
		}
	
		int max_value ;
		char type[3]  ;
	
		m_eColorModel = RGB ;
	
		fscanf(fp, "%s", type) ;
		fscanf(fp, "%d %d", &m_lWidth, &m_lHeight) ;
		fscanf(fp, "%d", &max_value) ;
	
		m_imageData = (ImDataType*) malloc(RGB_IMAGE * m_lWidth * m_lHeight * sizeof(ImDataType)) ;
	
		if (!m_imageData) { 
			initNull() ;
			m_eErrorCode = MEMORY_ERROR ;
			return ;
		}
	
		for (int i = 0 ; i < m_lHeight ; i++) 
		{
			for (int j = 0 ; j < m_lWidth * RGB_IMAGE ; j++) 
			{
				unsigned int data ;
				fscanf(fp, "%ld", &data) ;
				m_imageData[RGB_IMAGE * i * m_lWidth + j] = static_cast<ImDataType>(data) ;
			}
		}
		fclose(fp);
	
		m_eErrorCode = OK ;
	}
	
	template<typename ImDataType>
	
	void Image<ImDataType>::load_bmp(const char* fileName)
	{
		BITMAPFILEHEADER bmp_fh	;
		BITMAPINFOHEADER bmp_ih ;
	
		FILE* fp = fopen(fileName, "rb") ;
	
		if (!fp) 
		{
			initNull() ;
			m_eErrorCode = FILE_NOT_FOUND ;
			return ;
		}
	
		fread(&bmp_fh, sizeof(BITMAPFILEHEADER), 1, fp) ;
	
		fread(&bmp_ih, sizeof(BITMAPINFOHEADER), 1, fp) ;
	
		if (bmp_ih.biCompression != 0)	//compressed bitmaps not (yet) supported
		{
			initNull() ;
			m_eErrorCode = COMPRESSION_UNSUPPORTED ;
			return ;
		}
	
		m_lWidth  = static_cast<long>(bmp_ih.biWidth)	;
		m_lHeight = static_cast<long>(bmp_ih.biHeight)	;
		m_lSize	  = static_cast<long>(bmp_ih.biSize)	;
	
		if(m_lHeight < 0) m_lHeight = abs(m_lHeight) ;	//depending on the bitmap the height is negative
	
		//----------------------------------------------------------------------------------------------------------
		// 8-bit Bitmap (256 Color-Bitmap)																		  //
		//----------------------------------------------------------------------------------------------------------
		if (bmp_ih.biBitCount == 8) 
		{
			//Get a color palette
			ColorMapEntry* color_map = new ColorMapEntry[COLOR_DEPTH_8BIT] ;
	
			if (!color_map)
			{
				initNull() ;
				m_eErrorCode = MEMORY_ERROR ;
				return ;
			}
	
			fread(color_map, sizeof(color_map), COLOR_DEPTH_8BIT, fp);
	
			/*
			 *Number of bytes in a bitmap line must be divisible by four, means depending on the
			 *number of columns an offset (padding) must be added
			 */
			int padding = 0 ;
			
			for(padding = 0 ;; padding++)
				if ((m_lWidth + padding) % 4 == 0) break ;
	
			unsigned char* pixels = new unsigned char[m_lWidth * m_lHeight * 3] ;
			
			if (!pixels)
			{
				initNull() ;
				m_eErrorCode = MEMORY_ERROR ;
				return ;
			}
			//get image
			fseek(fp, (long)bmp_fh.bfOffBits, SEEK_SET) ;
			for (long i = 0 ; i < m_lHeight ; i++)
			{
				for (long j = 0 ; j < m_lWidth ; j++)
				{
					fread(&pixels[i * m_lWidth + j], sizeof(unsigned char), 1, fp);
				}
				fseek(fp, padding, SEEK_CUR) ;
			}
			//read and save data from color palette
			m_imageData = (ImDataType*) malloc(RGB_IMAGE * m_lWidth * m_lHeight * sizeof(ImDataType)) ;
			
			if (!m_imageData)
			{
				initNull() ;
				m_eErrorCode = MEMORY_ERROR ;
				return ;
			}
	
			int z = 0 ;
			for (long i = 0 ; i < m_lHeight ; i++)
			{
				for (long j = 0 ; j < m_lWidth * RGB_IMAGE ; j += 3)
				{
					m_imageData[RGB_IMAGE * i * m_lWidth + j + 0] = static_cast<ImDataType>(color_map[pixels[z]].R) ;
					m_imageData[RGB_IMAGE * i * m_lWidth + j + 1] = static_cast<ImDataType>(color_map[pixels[z]].G) ;
					m_imageData[RGB_IMAGE * i * m_lWidth + j + 2] = static_cast<ImDataType>(color_map[pixels[z]].B) ;
					z++ ;
				}
			}
			m_eColorModel = RGB;
			m_eErrorCode = OK ;
			return ;
		}
		//-----------------------------------------------------------------------------------------
		//	24 and 32 bit Bitmap																 //
		//-----------------------------------------------------------------------------------------
		else if ((bmp_ih.biBitCount == 24) || (bmp_ih.biBitCount == 32))
		{
			int bpp ;
			switch (bmp_ih.biBitCount)
			{
			case 24: bpp = RGB_IMAGE ; break ;
			case 32: bpp = RGBA_IMAGE; break ;
			default: bpp = RGB_IMAGE ; break ;
			}

			if ( bpp == RGB_IMAGE )
				m_eColorModel = RGB;
			else
				m_eColorModel = RGBA;
	
			unsigned char* pixel = new unsigned char[bpp] ;
	
			if (!pixel)
			{
				initNull() ;
				m_eErrorCode = MEMORY_ERROR ;
				return ;
			}
	
			m_imageData = (ImDataType*) malloc(bpp * m_lWidth * m_lHeight * sizeof(ImDataType)) ;
			
			if (!m_imageData)
			{
				initNull() ;
				m_eErrorCode = MEMORY_ERROR ;
				return ;
			}
	
			long padding = m_lWidth % 4 ;
			
			fseek(fp, bmp_fh.bfOffBits, SEEK_SET) ;
			//read data
			for (long i = 0 ; i < m_lHeight ; i++)
			{
				for (long j = 0 ; j < m_lWidth * bpp ; j += bpp)
				{
					fread(pixel, 1, bpp, fp) ;	//reads one BGR (with 24 bit Bitmap))
					//exchange B and R
					if (bpp == RGB_IMAGE) {	//24 bit Bitmap
						unsigned char temp = pixel[0] ;
						pixel[0] = pixel[2] ;
						pixel[2] = temp ;
					}
					else if (bpp == RGBA_IMAGE) {	//??????
						unsigned char temp = pixel[0] ;
						pixel[0] = pixel[1] ;
						pixel[1] = pixel[2] ;
						pixel[2] = pixel[3] ;
						pixel[3] = temp ;
					}
	
					for (int k = 0 ; k < bpp ; k++)
						m_imageData[bpp * i * m_lWidth + j + k] = static_cast<ImDataType>(pixel[k]) ;
				}
				fseek(fp, padding, SEEK_CUR) ;
			}
			m_eErrorCode = OK ;
			return ;
		}
		else
		{
			initNull() ;
			m_eErrorCode = FILE_ERROR ;
			return ;
		}
	}
}

#endif