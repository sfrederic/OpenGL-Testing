#include "CGERenderableDataGenerator.h"

namespace CGEngine
{

	CRenderableDataGenerator::~CRenderableDataGenerator()
	{
		m_vertices.clear();
		m_normals.clear();
		m_tangents.clear();
		m_texcoords.clear();
		m_binormals.clear();
		m_face_indices.clear();
	}

	const CGEngine::Vec3DVec& CRenderableDataGenerator::GetVertices() const
	{
		return m_vertices;
	}

	const CGEngine::Vec3DVec& CRenderableDataGenerator::GetNormalVectors() const
	{
		return m_normals;
	}

	const CGEngine::Vec2DVec& CRenderableDataGenerator::GetTextureCoordinates() const
	{
		return m_texcoords;
	}

	const CGEngine::Vec3DVec& CRenderableDataGenerator::GetBinormalVector() const
	{
		return m_binormals;
	}

	const CGEngine::Vec3DVec& CRenderableDataGenerator::GetTangentVectors() const
	{
		return m_tangents;
	}

	const CGEngine::GLuintVec& CRenderableDataGenerator::GetFaceIndices() const
	{
		return m_face_indices;
	}

	void CRenderableDataGenerator::CopyVertices(Vec3DVec& _vertices) const
	{
		std::copy(m_vertices.begin(), m_vertices.end(), _vertices.begin());
	}

	void CRenderableDataGenerator::CopyNormals(Vec3DVec& _normals) const
	{
		std::copy(m_normals.begin(), m_normals.end(), _normals.begin());
	}

	void CRenderableDataGenerator::CopyTextureCoordinates(Vec2DVec& _texcoords) const
	{
		std::copy(m_texcoords.begin(), m_texcoords.end(), _texcoords.begin());
	}

	void CRenderableDataGenerator::CopyTangentSpace(Vec3DVec& _binormals, Vec3DVec& _tangents) const
	{
		std::copy(m_binormals.begin(), m_binormals.end(), _binormals.begin());
		std::copy(m_tangents.begin(), m_tangents.begin(), _tangents.begin());
	}

	void CRenderableDataGenerator::CopyFaceIndices(GLuintVec& _indices) const
	{
		std::copy(m_face_indices.begin(), m_face_indices.end(), _indices.begin());
	}

	bool CRenderableDataGenerator::AddVertexAttributeStorage(const std::string& _attribName, const Attributes::AttribStoragePtr& _attribStorage)
	{
		if (m_VertexAttributeMap.find(_attribName) != m_VertexAttributeMap.end())
			return false;
		else
		{
			m_VertexAttributeMap[_attribName] = _attribStorage;
			return true;
		}
	}

	CGEngine::Attributes::AttribStoragePtr CRenderableDataGenerator::GetVertexAttributeStorage(const std::string& _attribName) const
	{
		auto m_it = m_VertexAttributeMap.find(_attribName);
		if (m_it != m_VertexAttributeMap.end())
			return m_it->second;
		else
			return Attributes::AttribStoragePtr(nullptr);
	}

	bool CRenderableDataGenerator::RemoveVertexAttributeStorage(const std::string& _attribName)
	{
		auto m_it = m_VertexAttributeMap.find(_attribName);
		if (m_it != m_VertexAttributeMap.end())
		{
			m_VertexAttributeMap.erase(m_it);
			return true;
		}
		else
			return false;
	}

	bool CRenderableDataGenerator::RemoveVertexAttribute(const std::string& _attribName)
	{
		return m_VertexAttributeContainer.Remove(_attribName);
	}

	void CRenderableDataGenerator::GetAttributeNames(std::vector<std::string>& _names) const
	{
		m_VertexAttributeContainer.GetAttributeNames(_names);
	}

	bool CRenderableDataGenerator::HasAttributes() const
	{
		return !m_VertexAttributeContainer.empty();
	}

	std::size_t CRenderableDataGenerator::GetNumAttributes() const
	{
		return m_VertexAttributeContainer.GetNumAttributes();
	}

	const CGEngine::Attributes::CAttributeContainer& CRenderableDataGenerator::GetAttributeContainer() const
	{
		return m_VertexAttributeContainer;
	}

}