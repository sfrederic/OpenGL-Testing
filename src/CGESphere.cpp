#include "CGESphere.h"

namespace CGEngine
{
	CSphere::CSphere()
	{
		m_VBO = m_IBO = 0;
		m_VertexArraySize = m_NormalArraySize = m_TexCoordsArraySize = m_IndexBufferSize = 0;
	}

	bool CSphere::VInitObject(const CSphereGeneratorShPtr& _sphereGenerator)
	{
		try
		{
			populate_buffers(_sphereGenerator->GetVertices(), _sphereGenerator->GetTextureCoordinates(),
				_sphereGenerator->GetNormalVectors(), _sphereGenerator->GetFaceIndices());

			return true;
		}
		catch (...)
		{
			return false;
		}
	}

	CSphere::~CSphere()
	{
		if (m_VBO) glDeleteBuffers(1, &m_VBO);
		if (m_IBO) glDeleteBuffers(1, &m_IBO);
	}

	void CSphere::VOnDraw()
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);

		if (m_NormalArraySize)
			glEnableClientState(GL_NORMAL_ARRAY);
		if (m_TexCoordsArraySize)
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		if (m_VertexArraySize)
			glEnableClientState(GL_VERTEX_ARRAY);

		glVertexPointer(3, GL_FLOAT, 0, (GLvoid*)0);
		glTexCoordPointer(2, GL_FLOAT, 0, (GLvoid*)(m_VertexArraySize));
		glNormalPointer(GL_FLOAT, 0, (GLvoid*)(m_VertexArraySize + m_TexCoordsArraySize));

		GLsizei num_elements = m_IndexBufferSize / sizeof(GLuint);
		glDrawElements(GL_TRIANGLES, num_elements, GL_UNSIGNED_INT, 0);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void CSphere::populate_buffers(const Vec3DVec &_positions, const Vec2DVec &_texcoords, const Vec3DVec &_normals, const std::vector<GLuint> &_indices)
	{
		// create new Vertex Buffer Object
		glGenBuffers(1, &m_VBO);
		glGenBuffers(1, &m_IBO);

		m_VertexArraySize		= 3 * _positions.size() * sizeof(GLfloat);
		m_TexCoordsArraySize	= 2 * _texcoords.size() * sizeof(GLfloat);
		m_NormalArraySize		= 3 * _normals.size() * sizeof(GLfloat);
		m_IndexBufferSize		= _indices.size() * sizeof(GLuint);

		// bind buffer and load data
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

		// reserve buffer size
		glBufferData(GL_ARRAY_BUFFER, m_VertexArraySize + m_TexCoordsArraySize + m_NormalArraySize, 0, GL_DYNAMIC_DRAW);

		// first part: vertex data, second part: texture coordinates, third part: normal coordinates
		if (m_VertexArraySize > 0)
			glBufferSubData(GL_ARRAY_BUFFER, 0, m_VertexArraySize, &_positions[0]);
		if (m_TexCoordsArraySize > 0)
			glBufferSubData(GL_ARRAY_BUFFER, m_VertexArraySize, m_TexCoordsArraySize, &_texcoords[0]);
		if (m_NormalArraySize > 0)
			glBufferSubData(GL_ARRAY_BUFFER, m_VertexArraySize + m_TexCoordsArraySize, m_NormalArraySize, &_normals[0]);

		// index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
		if (m_IndexBufferSize > 0)
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_IndexBufferSize, &_indices[0], GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

}


