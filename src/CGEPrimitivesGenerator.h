#pragma once

#include "CGERenderableDataGenerator.h"
#include <algorithm>
#include <memory>
#include <map>
#include <boost/foreach.hpp>

namespace CGEngine
{
	/*
	 *Base class for generating geometric primitives
	 */
	class CPrimitivesGenerator : public CRenderableDataGenerator
	{
	public:
		CPrimitivesGenerator() : CRenderableDataGenerator() {}
		virtual ~CPrimitivesGenerator();

		virtual bool VMakeObject(bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace) = 0;

	protected:

		virtual void make_normals() = 0;
		virtual void make_texcoords() = 0;
		virtual void make_tangentspace() = 0;
	private:
	};

	//////////////////////////////////////////////////////////////////////////

	class CSphereGenerator;
	typedef std::shared_ptr<CSphereGenerator>	CSphereGeneratorShPtr	;
	typedef std::unique_ptr<CSphereGenerator>	CSphereGeneratorUPtr	;

	class CSphereGenerator : public CPrimitivesGenerator
	{
	public:
		enum GenerationMethod
		{
			POLAR_COORD_SUBDIVISION = 0,
			SUBDIVISION_NORMALIZATION
		} m_GenerationMethod;

		float m_radius;
		int	m_num_subdivisions;
		std::shared_ptr<Vec3> m_center;

		explicit CSphereGenerator(GenerationMethod _generation_method, float _radius, int _number_of_subdivisions, std::shared_ptr<Vec3>& _center);
		~CSphereGenerator();

		virtual bool VMakeObject(bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace) override;	

	protected:
		CSphereGenerator();

		virtual void make_normals() override;

		virtual void make_texcoords() override;

		virtual void make_tangentspace() override;

		void translate_to_center();

	private:
		void make_sphere_polar(float _radius, int _dtheta, int _dphi);

		void make_sphere_subdivision(float _radius, int _num_subdivisions);

	};

} //End Namespace