#include "CGEPrimitivesGenerator.h"

namespace CGEngine
{

	CPrimitivesGenerator::~CPrimitivesGenerator()
	{
		
	}

	//////////////////////////////////////////////////////////////////////////

	CSphereGenerator::CSphereGenerator() : CPrimitivesGenerator()
	{
	}

	CSphereGenerator::CSphereGenerator(GenerationMethod _generation_method, float _radius, int _number_of_subdivisions, 
		std::shared_ptr<Vec3>& _center) : m_GenerationMethod(_generation_method), m_radius(_radius), m_num_subdivisions(_number_of_subdivisions),
		CPrimitivesGenerator()
	{
		m_center = _center;
	}

	void CSphereGenerator::make_normals()
	{
		m_normals.clear();
		BOOST_FOREACH(const Vec3& v, m_vertices)
		{
			m_normals.push_back(glm::normalize(v));
		}
	}

	void CSphereGenerator::make_texcoords()
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void CSphereGenerator::make_tangentspace()
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	CSphereGenerator::~CSphereGenerator()
	{
	}

	bool CSphereGenerator::VMakeObject(bool _compute_normals, bool _compute_texcoords, bool _compute_tangentspace)
	{
		switch (m_GenerationMethod)
		{
		case SUBDIVISION_NORMALIZATION:
			make_sphere_subdivision(m_radius, m_num_subdivisions);
			break;
		case POLAR_COORD_SUBDIVISION:
		default:
			make_sphere_polar(m_radius, 180/m_num_subdivisions, 360/(2*m_num_subdivisions));
			break;
		}

		if (_compute_normals)
			make_normals();
		if (_compute_texcoords)
			make_texcoords();
		if (_compute_tangentspace)
			make_tangentspace();
		if (m_center)
			translate_to_center();

		return true;
	}

	void CSphereGenerator::translate_to_center()
	{
		Matrix4x4 t = glm::translate(Matrix4x4(1.0f), *m_center);
		BOOST_FOREACH(Vec3& v, m_vertices)
		{
			Vec4 nv = t * Vec4(v, 1.0f);
			v = Vec3(nv.x, nv.y, nv.z);
		}
	}

	void CSphereGenerator::make_sphere_polar(float _radius, int _dtheta, int _dphi)
	{
		int n(0); //local vertex index counter (azimuth within loop)
		GLuint g_i(0); //global vertex index counter
		int num_vertices_azimuth = 360 / _dphi;
		Vec3 p;
		m_vertices.clear();
		m_face_indices.clear();

		std::vector<GLuint> prev_indices(num_vertices_azimuth, 0); //previously generated indices in azimuth loop
																   //first point at south pole
		p = Vec3(0.0f, 0.0f, _radius * -1.0f);
		m_vertices.push_back(p);
		g_i++;

		for (int theta = -90 + _dtheta; theta <= 90 - _dtheta; theta += _dtheta) //polar
		{
			n = 0;
			std::vector<GLuint> new_indices; //newly generated indices in azimuth loop
			new_indices.reserve(num_vertices_azimuth);

			for (int phi = 0; phi <= 360 - _dphi; phi += _dphi, n++, g_i++) //azimuth
			{
				//new point coordinates
				p.x = _radius * glm::cos(glm::radians(static_cast<float>(theta))) * glm::cos(glm::radians(static_cast<float>(phi)));
				p.y = _radius * glm::cos(glm::radians(static_cast<float>(theta))) * glm::sin(glm::radians(static_cast<float>(phi)));
				p.z = _radius * glm::sin(glm::radians(static_cast<float>(theta)));

				m_vertices.push_back(p);
				new_indices.push_back(g_i);

				//face 1
				m_face_indices.push_back(g_i);
				m_face_indices.push_back(g_i + 1);
				m_face_indices.push_back(prev_indices[n]);
				//face 2
				if (theta > -90 + _dtheta)
				{
					m_face_indices.push_back(g_i + 1);
					m_face_indices.push_back(prev_indices[(n + 1) % num_vertices_azimuth]);
					m_face_indices.push_back(prev_indices[n]);
				}
			}
			std::swap(prev_indices, new_indices);
		}
		//last point at north pole
		p = Vec3(0.0f, 0.0f, _radius);
		m_vertices.push_back(p);
		g_i;
		n = 0;
		for (; n < num_vertices_azimuth; ++n)
		{
			m_face_indices.push_back(g_i);
			m_face_indices.push_back(prev_indices[(n + 1) % num_vertices_azimuth]);
			m_face_indices.push_back(prev_indices[n]);
		}
	}


	inline size_t get_opposite_face_index(GLuintVec &_face_indices, size_t &_fi, GLuint &_edge_vert_s, GLuint &_edge_vert_e)
	{
		GLuint fi_opp(0);
		for (size_t fi = 0; fi < _face_indices.size() / 3; ++fi) //search opposite face
		{
			if (fi == _fi) continue;
			if ((_face_indices[3 * fi] == _edge_vert_s || _face_indices[3 * fi + 1] == _edge_vert_s || _face_indices[3 * fi + 2] == _edge_vert_s) &&
				(_face_indices[3 * fi] == _edge_vert_e || _face_indices[3 * fi + 1] == _edge_vert_e || _face_indices[3 * fi + 2] == _edge_vert_e)
				)
			{
				fi_opp = fi;
				break;
			}
		}
		return fi_opp;
	}

	void CSphereGenerator::make_sphere_subdivision(float _radius, int _num_subdivisions)
	{
		//start with octahedron
		m_vertices.push_back(_radius * Vec3(0, 1, 0));		m_vertices.push_back(_radius * Vec3(-1, 0, 1));
		m_vertices.push_back(_radius * Vec3(-1, 0, -1));	m_vertices.push_back(_radius * Vec3(1, 0, -1));
		m_vertices.push_back(_radius * Vec3(1, 0, 1));		m_vertices.push_back(_radius * Vec3(0, -1, 0));
		GLuint fi[] = { 0,4,1, 0,1,2, 0,2,3, 0,3,4, 1,4,5, 2,1,5, 3,2,5, 4,3,5 }; //clockwise vertex ordering
		m_face_indices = GLuintVec(fi, fi + sizeof(fi) / sizeof(GLuint));
		const float ONE_THIRD = (1.0f / 3.0f);

		while (_num_subdivisions > 0)
		{
			GLuintVec new_face_indices;
			GLuint new_vert_index = m_vertices.size();
			std::map<size_t, GLuint> vertex_face_map; //map center vertex index to face index
			std::vector<bool> face_processed(m_face_indices.size()/3, false);

			//compute center vertex for each face
			for (size_t f_it = 0; f_it < m_face_indices.size() / 3; ++f_it)
			{
				Vec3 cv = ONE_THIRD * ( m_vertices[m_face_indices[3*f_it]] + m_vertices[m_face_indices[3*f_it+1]] + m_vertices[m_face_indices[3*f_it+2]] );
				m_vertices.push_back(_radius * glm::normalize(cv));

				vertex_face_map.insert(std::pair<size_t, GLuint>(f_it, new_vert_index));
				new_vert_index++;
			}
			//flip edges to get new (regular) faces
			for (size_t f_it = 0; f_it < m_face_indices.size() / 3; ++f_it)
			{
				for (int i = 0; i < 3; ++i) //for each edge in face get opposite face and its center vertex
				{
					GLuint edge_vert_s(m_face_indices[3*f_it + i]), edge_vert_e(m_face_indices[3*f_it + ((i + 1) % 3)]); //start and end vertex of edge
					size_t fi_opp = get_opposite_face_index(m_face_indices, f_it, edge_vert_s, edge_vert_e);

					if (face_processed[fi_opp]) continue;
					else
					{
						//insert new faces
						new_face_indices.push_back(edge_vert_s);
						new_face_indices.push_back(vertex_face_map.at(fi_opp));
						new_face_indices.push_back(vertex_face_map.at(f_it));

						new_face_indices.push_back(vertex_face_map.at(f_it));
						new_face_indices.push_back(vertex_face_map.at(fi_opp));
						new_face_indices.push_back(edge_vert_e);
					}
				}
				face_processed[f_it] = true;
			}
			std::swap(m_face_indices, new_face_indices);
			new_face_indices.clear();
			vertex_face_map.clear();

			_num_subdivisions--;
		}

		BOOST_FOREACH(Vec3& v, m_vertices)
		{
			v = _radius * glm::normalize(v);
		}
	}

} //End Namespace