/************************************************************************/
/* Noise texture generation                                             */
/************************************************************************/

#include "CGEGeometry.h"
#ifndef __GLEW_H__
#define GLEW_STATIC
#include <GL/glew.h>
#endif
#include <glm/gtc/noise.hpp>
#include <stdlib.h>
#include <iostream>
#include <fstream>

class NoiseTexture
{
public:
	NoiseTexture();
	NoiseTexture(int _size);

	void make3DNoiseTexture();
	void init3DNoiseTexture(GLenum _activeTexUnit);
	void outputToFile(const std::string& _fileName);
	bool loadFromFile(const std::string& _fileName);
	void bind(GLenum _activeTexUnit);

private:
	GLuint noise3DTexName;
	int noise3DtexSize;
	GLubyte* noise3DTexPtr;
};
